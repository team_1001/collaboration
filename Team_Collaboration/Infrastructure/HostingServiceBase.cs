﻿using System;
using System.ServiceProcess;
using NLog;

namespace Infrastructure
{
    public abstract class HostingServiceBase : ServiceBase, IServiceBase
    {
        protected readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public event EventHandler ServiceStopped;
        protected readonly string LogTag;

        protected HostingServiceBase(string serviceName, string logTag = "")
        {
            ServiceName = serviceName;
            LogTag = logTag;
        }

        #region private methods

        protected override void OnStart(string[] args)
        {
            Logger.Info("{0} - Service named '{1}' is starting...", LogTag, ServiceName);

            try
            {
                OnStartInternal();
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                throw;
            }

            Logger.Info("{0} - Service named '{1}' started!", LogTag, ServiceName);
        }

        protected override void OnStop()
        {
            OnServiceStopped(EventArgs.Empty);
        }

        private void OnServiceStopped(EventArgs e)
        {
            EventHandler stopped = ServiceStopped;
            if (stopped != null) stopped(this, e);
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    Stop();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        protected abstract void OnStartInternal();

        public void Open()
        {
            OnStart(null);
        }

        public void Close()
        {
            OnStop();
        }
    }
}
