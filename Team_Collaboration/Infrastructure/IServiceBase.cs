﻿namespace Infrastructure
{
    public interface IServiceBase
    {
        void Open();

        void Close();
    }
}
