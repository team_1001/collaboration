﻿using System;
using Microsoft.Owin.Hosting;

namespace Infrastructure
{
    public class WebHostingService<TStartup> : HostingServiceBase
    {
        private readonly string _baseAddress;
        private IDisposable _app;
        
        public WebHostingService(string serviceName, string baseAddress, string logTag = "") 
            : base(serviceName, logTag)
        {
            _baseAddress = baseAddress;
        }

        protected override void OnStartInternal()
        {
            // Start OWIN host 
            _app = WebApp.Start<TStartup>(_baseAddress);
            Logger.Info("Listening to {0}", _baseAddress);
        }

        protected override void OnStop()
        {
            if (_app != null)
                _app.Dispose();

            base.OnStop();
        }
    }
}
