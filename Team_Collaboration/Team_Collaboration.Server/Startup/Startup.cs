﻿using System;
using System.Globalization;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using AuthLayer.Interfaces;
using Autofac;
using Autofac.Extras.NLog;
using Autofac.Integration.WebApi;
using BusinessLayer.Config;
using Common;
using Common.Configuration;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Owin.Security.AesDataProtectorProvider;
using Team_Collaboration.Server.Config;
using Team_Collaboration.Server.Providers.Auth;
using MainModule = Team_Collaboration.Server.AutoFac.MainModule;

[assembly: OwinStartup(typeof(Team_Collaboration.Server.Startup))]

namespace Team_Collaboration.Server
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AutoFacCore.Init(
                new MainModule(app),
                new BusinessLayer.AutoFac.MainModule(),
                new Hub.AutoFac.MainModule(),
                new NLogModule());

            var scope = AutoFacCore.Core.BeginLifetimeScope();

            // todo: improve
            var configurator = scope.Resolve<IConfigurator>();
            var config = scope.Resolve<IConfigurationProvider>();

            configurator.Configure(config.ConnectionStrings["DefaultConnection"]);

            // NOTE: this option should be here on the top, don't move it anywhere

            app.UseCors(CorsOptions.AllowAll);

            app.Use(async (context, next) =>
            {
                CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en-US");
                await next.Invoke();
            });

            
            ConfigureOAuth(app, scope.Resolve<IApplicationUserManagerFactory>());
            HttpConfiguration configuration = new HttpConfiguration();
            WebApiConfig.Register(configuration);
            configuration.MessageHandlers.Insert(0, new CurrentRequestHandler());
            configuration.MessageHandlers.Add(new ResponseWrappingHandler());

            

            configuration.DependencyResolver = new AutofacWebApiDependencyResolver(scope);
            app.UseAutofacMiddleware(scope);
            app.UseMiddlewareFromContainer<LoggingModule>();
            app.UseWebApi(configuration);
        }

        private void ConfigureOAuth(IAppBuilder app, IApplicationUserManagerFactory userManagerFactory)
        {
            //use a cookie to temporarily store information about a user logging in with a third party login provider
            //app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);

            app.UseAesDataProtectorProvider();

            // Token Generation
            //app.Use(typeof(OwinMiddleWareQueryStringExtractor));
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
                AccessTokenProvider = new AuthenticationTokenProvider(),
                Provider = new SimpleAuthorizationServerProvider(userManagerFactory),
                RefreshTokenProvider = new SimpleRefreshTokenProvider(userManagerFactory)
            });

            ////Token Consumption
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}
