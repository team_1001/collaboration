﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;

namespace Team_Collaboration.Server.Controllers
{
    [Authorize(Roles = "user")]
    public class TestController : ApiController
    {
        private readonly IMessagesProvider _repository;

        public TestController(IMessagesProvider repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<string>> Get()
        {
            return Enumerable.Repeat("test", 10);
        } 


    }
}