﻿using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using Models;
using Models.Exceptions;
using Models.Paging;
using Team_Collaboration.Server.Filters;

namespace Team_Collaboration.Server.Controllers
{
    [KnownExceptionsFilter(typeof(UserException), typeof(EventException), typeof(RequestException))]
    public class RequestsController : BaseApiController
    {
        private readonly IRequestsProvider _requestsProvider;

        public RequestsController(IRequestsProvider requestsProvider)
        {
            _requestsProvider = requestsProvider;
        }

        public async Task<CreatedRequestModel> Post([FromUri] int eventId)
        {
            var model = new RequestModel()
            {
                UserId = UserId,
                EventId = eventId
            };

            return await _requestsProvider.Add(model);
        }

        public async Task<RequestModel> Delete([FromUri] int eventId)
        {
            var model = new RequestModel()
            {
                UserId = UserId,
                EventId = eventId
            };

            await _requestsProvider.Cancel(model);
            return model;
        }

        [HttpGet]
        [Route("api/Requests/Outgoing")]
        public async Task<ItemsResult<Request>> Outgoing([FromUri] PageInfo pageInfo)
        {
            return await _requestsProvider.GetOutgoing(UserId, pageInfo);
        }

        [HttpGet]
        [Route("api/Requests/Incoming/{id}")]
        public async Task<ItemsResult<Request>> Incoming(int id, [FromUri] PageInfo pageInfo)
        {
            return await _requestsProvider.GetIncomingByEvent(id, pageInfo);
        }

        [HttpGet]
        [Route("api/Requests/Incoming")]
        public async Task<ItemsResult<Request>> Incoming([FromUri] PageInfo pageInfo)
        {
            return await _requestsProvider.GetIncoming(UserId, pageInfo);
        }

        [HttpPost]
        [Route("api/Requests/Accept")]
        public async Task<Request> Accept(Request model)
        {
            await _requestsProvider.Accept(model);
            return model;
        }

        [HttpPost]
        [Route("api/Requests/Decline")]
        public async Task<Request> Decline(Request model)
        {
            await _requestsProvider.Decline(model);
            return model;
        }
    }
}
