﻿using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using Models.Exceptions;
using Models.Paging;
using Team_Collaboration.Server.Filters;

namespace Team_Collaboration.Server.Controllers
{
    [KnownExceptionsFilter(typeof(UserException), typeof(NotificationException))]
    public class NotificationsController : BaseApiController
    {
        private readonly INotificationProvider _notificationProvider;

        public NotificationsController(INotificationProvider notificationProvider)
        {
            _notificationProvider = notificationProvider;
        }

        public async Task<dynamic> Get([FromUri] PageInfo pageInfo, [FromUri] NotificationType[] types)
        {
            return await _notificationProvider.Get(UserId, pageInfo, notificationTypes: types);
        }

        public async Task<dynamic> Post(string id)
        {
            return new { Success = await _notificationProvider.Update(id, UserId) };
        }

        public async Task<dynamic> Post([FromBody] NotificationType[] types)
        {
            return new { Success = await _notificationProvider.Update(types, UserId) };
        }
    }
}
