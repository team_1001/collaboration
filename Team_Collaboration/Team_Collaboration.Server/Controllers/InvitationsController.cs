﻿using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using Models;
using Models.Exceptions;
using Models.Paging;
using Team_Collaboration.Server.Filters;

namespace Team_Collaboration.Server.Controllers
{
    [KnownExceptionsFilter(typeof(UserException), typeof(EventException), typeof(InvitationException))]
    public class InvitationsController : BaseApiController
    {
        private readonly IInvitationsProvider _invitationsProvider;

        public InvitationsController(IInvitationsProvider invitationsProvider)
        {
            _invitationsProvider = invitationsProvider;
        }

        public async Task<dynamic> Post([FromBody] InvitationModel model)
        {
            var userIds = await _invitationsProvider.Add(model, UserId);
            return new { UserIds = userIds };
        }

        [HttpGet]
        [Route("api/Invitations/Outgoing/{id}")]
        public async Task<ItemsResult<Invite>> Outgoing(int id, [FromUri] PageInfo pageInfo)
        {
            return await _invitationsProvider.GetOutgoing(id, UserId, pageInfo);
        }

        [HttpGet]
        [Route("api/Invitations/Incoming/{id}")]
        public async Task<ItemsResult<Invite>> Incoming(int id, [FromUri] PageInfo pageInfo)
        {
            return await _invitationsProvider.GetIncoming(id, UserId, pageInfo);
        }

        [HttpGet]
        [Route("api/Invitations/Incoming")]
        public async Task<ItemsResult<Invite>> Incoming([FromUri] PageInfo pageInfo)
        {
            return await _invitationsProvider.GetIncoming(UserId, pageInfo);
        }

        [HttpGet]
        [Route("api/Invitations/Outgoing")]
        public async Task<ItemsResult<Invite>> Outgoing([FromUri] PageInfo pageInfo)
        {
            return await _invitationsProvider.GetOutgoing(UserId, pageInfo);
        }

        [HttpPost]
        [Route("api/Invitations/Accept")]
        public async Task<Invite> Accept(Invite model)
        {
            await _invitationsProvider.Accept(model);
            return model;
        }

        [HttpPost]
        [Route("api/Invitations/Decline")]
        public async Task<Invite> Decline(Invite model)
        {
            await _invitationsProvider.Decline(model);
            return model;
        }
    }
}
