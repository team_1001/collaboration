﻿using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using Models.Paging;

namespace Team_Collaboration.Server.Controllers
{
    [Authorize]
    public class MessagesController : BaseApiController
    {
        private readonly IMessagesProvider _messagesProvider;

        public MessagesController(IMessagesProvider messagesProvider)
        {
            _messagesProvider = messagesProvider;
        }

        public async Task<dynamic> Get(string id, int page, int pageSize)
        {
            return new { Messages = await _messagesProvider.GetMessages(UserId, id, new PageInfo(page, pageSize)) };
        }

        [Route("api/Messages/missed")]
        [HttpGet]
        public async Task<dynamic> Missed()
        {
            return new { Count = await _messagesProvider.GetUnreadMessagesCount(UserId) };
        }
    }
}
