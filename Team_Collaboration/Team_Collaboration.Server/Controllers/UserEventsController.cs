﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using Models;
using Models.Exceptions;
using Team_Collaboration.Server.Filters;

namespace Team_Collaboration.Server.Controllers
{
    [KnownExceptionsFilter(typeof(UserException), typeof(EventException))]
    public class UserEventsController : BaseApiController
    {
        private readonly IUserProvider _userProvider;

        public UserEventsController(IUserProvider userProvider)
        {
            _userProvider = userProvider;
        }

        public async Task<EventJoinResult> Post([FromUri] int id)
        {
            return await _userProvider.Join(UserId, id);
        }

        public async Task<IHttpActionResult> Delete([FromUri] int id)
        {
            try
            {
                await _userProvider.Leave(UserId, id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        public async Task<dynamic> Get([FromUri] string[] roles)
        {
            return new { Events = await _userProvider.GetEvents(UserId, roles) };
        }

        [Route("api/UserEvents/Participants/{id}")]
        [HttpGet]
        public async Task<dynamic> Get(int id, [FromUri] string[] roles)
        {
            return new { Events = await _userProvider.GetParticipants(id, UserId, roles) };
        }
    }
}