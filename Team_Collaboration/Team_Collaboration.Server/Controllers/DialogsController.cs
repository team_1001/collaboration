﻿using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;

namespace Team_Collaboration.Server.Controllers
{
    [Authorize]
    public class DialogsController : BaseApiController
    {
        private readonly IMessagesProvider _messagesProvider;

        public DialogsController(IMessagesProvider messagesProvider)
        {
            _messagesProvider = messagesProvider;
        }

        public async Task<dynamic> Get()
        {
            return new { Messages = await _messagesProvider.GetDialogs(UserId) };
        }
    }
}
