﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using Common.Extensions;

namespace Team_Collaboration.Server.Controllers
{
    public class FilesController : BaseApiController
    {
        private readonly IFileService _fileService;

        public FilesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [AllowAnonymous]
        public async Task<HttpResponseMessage> Get(string id)
        {
            string fullPath = _fileService.MapPath(id);
            if (!File.Exists(fullPath))
                throw new Exception($"File {id} doesn't exist");

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            
            var stream = new FileStream(fullPath, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return await Task.FromResult(result);
        }

        [AllowAnonymous]
        public async Task<IHttpActionResult> Post(HttpRequestMessage request)
        {
            try
            {
                string mapPath = _fileService.GetFilesPath();
                _fileService.CreateIfNotExists(mapPath);

                var provider = new MultipartFormDataStreamProvider(mapPath);
                await request.Content.ReadAsMultipartAsync(provider);
                if (provider.FileData.Count == 0)
                    throw new Exception("Request is invalid");

                string fileName = Guid.NewGuid().ToString("n");
                var file = provider.FileData[0];
                string fileNameExtension = Path.GetExtension(file.Headers.ContentDisposition.FileName.Match(@"(?>(?<=\W(?!\W))(?<Name>[\w\.]*(?=\W)))", "Name"));
                string fileNameWithExtension = $"{fileName}{fileNameExtension}";
                string fullNameWithExtension = Path.Combine(mapPath, fileNameWithExtension);

                _fileService.Copy(file.LocalFileName, Path.Combine(mapPath, fullNameWithExtension));

                return Ok(new { FileName = fileNameWithExtension });
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        } 
    }
}
