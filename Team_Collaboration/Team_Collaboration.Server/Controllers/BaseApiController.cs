﻿using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace Team_Collaboration.Server.Controllers
{
    [Authorize]
    public abstract class BaseApiController : ApiController
    {
        protected int UserId => User.Identity.GetUserId<int>();
    }
}