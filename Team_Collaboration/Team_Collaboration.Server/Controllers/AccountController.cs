﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AuthLayer;
using AuthLayer.Interfaces;
using DataLayer.Entity;
using Microsoft.AspNet.Identity;
using Team_Collaboration.Server.Models;

namespace Team_Collaboration.Server.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly IApplicationUserManager _userManager;

        public AccountController(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        //[AllowAnonymous]
        //public async Task<IEnumerable<string>> Get()
        //{
        //    return await Task.FromResult(Enumerable.Repeat("test", 10));
        //}

        public async Task<IEnumerable<string>> Get()
        {
            return await Task.FromResult(Enumerable.Repeat("test", 10));
        }

        // POST api/Account/Register
        [AllowAnonymous]
        public async Task<IHttpActionResult> Post(RegisterViewModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new AppUser
            {
                UserName = userModel.Email,
                Email = userModel.Email,
                User = new User()
                {
                    Email = userModel.Email,
                    UserName = userModel.Email,
                    FirstName = userModel.FirstName,
                    LastName = userModel.LastName
                }
            };

            IdentityResult result = await _userManager.CreateAsync(user, userModel.Password);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}
