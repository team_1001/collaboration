﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using Models;

namespace Team_Collaboration.Server.Controllers
{
    [Authorize]
    public class UsersController : BaseApiController
    {
        private readonly IUserProvider _userProvider;

        public UsersController(IUserProvider userProvider)
        {
            _userProvider = userProvider;
        }

        public async Task<ItemsResult<UserInfo>> Get()
        {
            return await _userProvider.GetAll(UserId);
        }
    }
}
