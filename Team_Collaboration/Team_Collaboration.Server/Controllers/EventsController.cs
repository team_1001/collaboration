﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using Models;
using Models.Exceptions;
using Models.Paging;
using Team_Collaboration.Server.Filters;

namespace Team_Collaboration.Server.Controllers
{
    [KnownExceptionsFilter(typeof(UserException), typeof(EventException))]
    public class EventsController : BaseApiController
    {
        private readonly IEventProvider _eventProvider;

        public EventsController(IEventProvider eventProvider)
        {
            _eventProvider = eventProvider;
        }

        public async Task<ItemsResult<Event>> Get([FromUri] PageInfo pageInfo)
        {
            return await _eventProvider.GetAll(pageInfo);
        }

        [HttpGet]
        [Route("api/Events/Geo")]
        public async Task<ItemsResult<Event>> Get([FromUri] GeoInfo geoInfo)
        {
            return await _eventProvider.GetAllGeo(geoInfo, new PageInfo());
        }

        [HttpGet]
        [Route("api/Events/Categories")]
        public async Task<dynamic> Categories()
        {
            return new
            {
                Categories = await _eventProvider.GetCategories(),
                Types = Enum.GetNames(typeof(EventType))
            };
        }

        public async Task<EventResult> Get(int id)
        {
            return await _eventProvider.Get(id, UserId);
        }

        public async Task<Event> Post([FromBody] Event model)
        {
            await _eventProvider.Add(model, UserId);
            return model;
        }

        public async Task<Event> Put(Event model)
        {
            await _eventProvider.Update(model, UserId);
            return model;
        }

        public async Task<IHttpActionResult> Delete([FromUri] int id)
        {
            await _eventProvider.Delete(id, UserId);
            return Ok();
        }
    }
}