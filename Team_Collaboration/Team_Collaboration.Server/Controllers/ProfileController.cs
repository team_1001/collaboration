﻿using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using Models.Exceptions;
using Team_Collaboration.Server.Filters;

namespace Team_Collaboration.Server.Controllers
{
    [KnownExceptionsFilter(typeof(UserException))]
    public class ProfileController : BaseApiController
    {
        private readonly IUserProvider _userProvider;

        public ProfileController(IUserProvider userProvider)
        {
            _userProvider = userProvider;
        }

        public async Task<User> Get()
        {
            return await _userProvider.Get(UserId);
        }

        public async Task<bool> Put([FromBody] User user)
        {
            return await _userProvider.Update(user);
        }
    }
}
