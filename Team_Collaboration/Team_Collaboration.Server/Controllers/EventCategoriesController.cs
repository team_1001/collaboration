﻿using BusinessLayer.Services.Interfaces;

namespace Team_Collaboration.Server.Controllers
{
    public class EventCategoriesController : BaseApiController
    {
        private readonly IEventCategoriesProvider _eventCategoriesProvider;

        public EventCategoriesController(IEventCategoriesProvider eventCategoriesProvider)
        {
            _eventCategoriesProvider = eventCategoriesProvider;
        }
    }
}
