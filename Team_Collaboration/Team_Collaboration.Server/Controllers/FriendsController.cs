﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using Models.Exceptions;
using Team_Collaboration.Server.Filters;

namespace Team_Collaboration.Server.Controllers
{
    [Authorize]
    [KnownExceptionsFilter(typeof(UserException))]
    public class FriendsController : BaseApiController
    {
        private readonly IUserFriendsProvider _friendsProvider;

        public FriendsController(IUserFriendsProvider friendsProvider)
        {
            _friendsProvider = friendsProvider;
        }

        public async Task<dynamic> Get()
        {
            return new
            {
                Friends = await _friendsProvider.GetFriends(UserId)
            };
        }

        public async Task<IHttpActionResult> Post([FromUri] int id)
        {
            bool result = await _friendsProvider.Add(UserId, id);
            if (!result)
                throw new Exception("Unknown error occured");

            return Ok("Friend was added successfully");
        }

        public async Task<IHttpActionResult> Delete([FromUri] int id)
        {
            bool result = await _friendsProvider.Remove(UserId, id);
            if (!result)
                throw new Exception("Unknown error occured");

            return Ok("Friend was removed successfully");
        }
    }
}