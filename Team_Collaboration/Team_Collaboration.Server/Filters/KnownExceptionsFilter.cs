﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Team_Collaboration.Server.Filters
{
    public class KnownExceptionsFilterAttribute : ExceptionFilterAttribute
    {
        public Type[] ExceptionTypes { get; set; }

        public KnownExceptionsFilterAttribute(params Type[] exceptionTypes)
        {
            ExceptionTypes = exceptionTypes;
        }

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (ExceptionTypes.Any(t => t.IsInstanceOfType(actionExecutedContext.Exception)))
            {
                throw new HttpResponseException(new HttpResponseMessage()
                {
                    Content = new ObjectContent(actionExecutedContext.Exception.GetType(), actionExecutedContext.Exception, new JsonMediaTypeFormatter()),
                    ReasonPhrase = "Error"
                });
            }
        }
    }
}
