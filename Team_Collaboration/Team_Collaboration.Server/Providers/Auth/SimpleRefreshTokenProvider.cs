﻿using System;
using System.Threading.Tasks;
using AuthLayer;
using AuthLayer.Interfaces;
using Common.Utils;
using Microsoft.Owin.Security.Infrastructure;

namespace Team_Collaboration.Server.Providers.Auth
{
    class SimpleRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly IApplicationUserManagerFactory _authManagerFactory;

        public SimpleRefreshTokenProvider(IApplicationUserManagerFactory authManagerFactory)
        {
            _authManagerFactory = authManagerFactory;
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientid = context.Ticket.Properties.Dictionary["as:client_id"];

            if (string.IsNullOrEmpty(clientid))
            {
                return;
            }

            var refreshTokenId = Guid.NewGuid().ToString("n");

            using (var manager = _authManagerFactory.Create())
            {
                var refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");

                var token = new RefreshToken()
                {
                    Id = Hasher.GetHash(refreshTokenId),
                    ClientId = clientid,
                    Subject = context.Ticket.Identity.Name,
                    IssuedUtc = DateTime.UtcNow,
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
                };

                context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
                context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

                token.ProtectedTicket = context.SerializeTicket();

                var result = await manager.AddRefreshTokenAsync(token);

                if (result)
                {
                    context.SetToken(refreshTokenId);
                }
            }
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            try
            {
                if (context.OwinContext.Response.Headers.ContainsKey("Access-Control-Allow-Origin"))
                    context.OwinContext.Response.Headers["Access-Control-Allow-Origin"] = allowedOrigin;
                else
                {
                    context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });
                }

            }
            catch (Exception e)
            {
            }

            string hashedTokenId = Hasher.GetHash(context.Token);

            using (var manager = _authManagerFactory.Create())
            {
                var refreshToken = await manager.FindRefreshTokenAsync(hashedTokenId);

                if (refreshToken != null)
                {
                    //Get protectedTicket from refreshToken class
                    context.DeserializeTicket(refreshToken.ProtectedTicket);
                    await manager.RemoveRefreshTokenAsync(hashedTokenId);
                }
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
    }
}
