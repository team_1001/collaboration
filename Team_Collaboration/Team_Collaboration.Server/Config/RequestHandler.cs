﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Team_Collaboration.Server.Config
{
    public class CurrentRequest
    {
        public HttpRequestMessage Value { get; set; }
    }

    public class CurrentRequestHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            var scope = request.GetDependencyScope();
            var currentRequest = (CurrentRequest)scope.GetService(typeof(CurrentRequest));
            currentRequest.Value = request;
            return await base.SendAsync(request, cancellationToken);
        }
    }
}
