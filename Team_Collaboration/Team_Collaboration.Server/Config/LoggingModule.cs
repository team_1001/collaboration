﻿using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using Microsoft.Owin;

namespace Team_Collaboration.Server.Config
{
    public class LoggingModule : OwinMiddleware
    {
        private readonly ILogger _logger;

        public LoggingModule(OwinMiddleware next, ILogger logger) : base(next)
        {
            _logger = logger;
        }

        public override async Task Invoke(IOwinContext context)
        {
            _logger.Trace($"{context.Request.Method}:{context.Request.Uri}");
            await Next.Invoke(context);
        }
    }
}
