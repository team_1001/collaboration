﻿using System.Net.Http;
using Autofac;
using Autofac.Integration.WebApi;
using BusinessLayer.Services.Interfaces;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using Team_Collaboration.Server.Config;
using Team_Collaboration.Server.Helpers;

namespace Team_Collaboration.Server.AutoFac
{
    public class MainModule : Module
    {
        private readonly IAppBuilder _appBuilder;

        public MainModule(IAppBuilder appBuilder)
        {
            _appBuilder = appBuilder;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CurrentRequest>().AsSelf();

            builder.Register(c => c.Resolve<CurrentRequest>().Value.GetOwinContext()).As<IOwinContext>();

            builder.Register(x => _appBuilder.GetDataProtectionProvider());

            builder.RegisterType<DataStorage>().As<IDataStorage>();
            //builder.RegisterType<EventProvider>().As<IEventProvider>();
            //builder.RegisterType<UserProvider>().As<IUserProvider>();

            builder.RegisterType<LoggingModule>().AsSelf();
            builder.RegisterApiControllers(typeof (MainModule).Assembly);
        }
    }
}