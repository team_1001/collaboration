﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Extensions;
using Newtonsoft.Json;

namespace DataLayer.Entity
{
    public class Request : IEntity
    {
        public virtual Event Event { get; set; }

        public int EventId { get; set; }

        public ProposalState State { get; set; }

        public virtual User User { get; set; }

        public int UserId { get; set; }

        [JsonIgnore]
        public DateTime TimeStamp { get; set; }

        [NotMapped]
        public long TimeStampTicks
        {
            get { return TimeStamp.Convert(); }
            set { TimeStamp = value.Convert(); }
        }
    }
}
