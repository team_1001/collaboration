﻿namespace DataLayer.Entity
{
    public class UnreadMessage : IEntity
    {
        public string MessageId { get; set; }

        public int UserId { get; set; }
    }
}
