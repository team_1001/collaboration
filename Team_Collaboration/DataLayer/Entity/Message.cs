﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Entity
{
    public enum MessageType
    {
        Text = 1,
        File = 2
    }

    public class Message : IEntity
    {
        public Message()
        {
            UnreadMessages = new HashSet<UnreadMessage>();    
        }

        public string Id { get; set; }

        public string Content { get; set; }

        public string DialogId { get; set; }

        public MessageType Type { get; set; }

        public DateTime TimeStamp { get; set; }

        public int SenderId { get; set; }

        public virtual User Sender { get; set; }

        public bool Viewed { get; set; }

        [NotMapped]
        public bool Missed { get; set; }

        [NotMapped]
        public string SenderName { get; set; }

        public virtual ICollection<UnreadMessage> UnreadMessages { get; set; }
    }
}
