﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Extensions;
using DataLayer.Validation;
using FluentValidation.Attributes;
using Newtonsoft.Json;

namespace DataLayer.Entity
{
    public enum EventType
    {
        Public = 1,
        Private = 2
    }

    [Serializable]
    [Validator(typeof(EventValidator))]
    public class Event : IEntity
    {
        public Event()
        {
            AllUsers = new HashSet<UserToEvent>();
            Invites = new HashSet<Invite>();
            Requests = new HashSet<Request>();
            Tags = new HashSet<Tag>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int MaxPeopleCount { get; set; }

        [JsonIgnore]
        public DateTime DateStarts { get; set; }

        [JsonIgnore]
        public DateTime DateEnds { get; set; }

        public virtual Location Location { get; set; }

        public int LocationId { get; set; }

        public virtual ICollection<UserToEvent> AllUsers { get; set; }

        public virtual ICollection<Invite> Invites { get; set; }

        public virtual ICollection<Request> Requests { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

        public string Visibility { get; set; }

        public virtual Dialog Dialog { get; set; }

        public virtual EventCategory Category { get; set; }

        public int CategoryId { get; set; }

        public string DialogId { get; set; }

        public EventType EventType { get; set; }

        [NotMapped]
        public int HostId { get; set; }

        [NotMapped]
        public long DateStartsTicks
        {
            get { return DateStarts.Convert(); }
            set { DateStarts = value.Convert(); }
        }

        [NotMapped]
        public long DateEndsTicks
        {
            get { return DateEnds.Convert(); }
            set { DateEnds = value.Convert(); }
        }
    }
}
