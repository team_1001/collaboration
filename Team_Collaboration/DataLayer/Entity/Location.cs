﻿using System.ComponentModel.DataAnnotations.Schema;
using Common.Extensions;
using Newtonsoft.Json;

namespace DataLayer.Entity
{
    public class Location : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Address { get; set; }

        public string Name { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        [NotMapped]
        [JsonProperty("coordinates")]
        public string Сoordinates
        {
            get { return $"{Latitude};{Longitude}"; }
            set
            {
                if (value.IsNullOrEmpty())
                    return;

                string[] values = value.Split(';');
                if (values.Length < 2)
                    return;

                Latitude = values[0].ToDouble();
                Longitude = values[1].ToDouble();
            }
        }
    }
}
