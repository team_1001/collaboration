﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Entity
{
    public class Dialog : IEntity
    {
        public Dialog()
        {
            Participants = new HashSet<User>();
            Messages = new HashSet<Message>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<User> Participants { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
        
        [NotMapped]
        public string FullName { get; set; }

        [NotMapped]
        public int ParticipantsCount { get; set; }

        [NotMapped]
        public int UnreadCount { get; set; }

        [NotMapped]
        public Message LastMessage { get; set; }

        [NotMapped]
        public bool IsEvent { get; set; }
    }
}
