﻿namespace DataLayer.Entity
{
    public class Tag
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
