﻿namespace DataLayer.Entity
{
   public class EventCategory
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }
    }
}
