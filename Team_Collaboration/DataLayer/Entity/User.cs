﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DataLayer.Validation;
using FluentValidation.Attributes;

namespace DataLayer.Entity
{
    [Validator(typeof(UserValidator))]
    public class User : IEntity
    {
        public User()
        {
            Events = new HashSet<UserToEvent>();
            IncomingInvites = new HashSet<Invite>();
            OutgoingInvites = new HashSet<Invite>();
            Requests = new HashSet<Request>();
            Friends = new HashSet<User>();
            Dialogs = new HashSet<Dialog>();
            Notifications = new HashSet<Notification>();
            UnreadMessages = new HashSet<UnreadMessage>();
            MissedNotifications = new HashSet<MissedNotification>();
        }

        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Birthdate { get; set; }

        public string Contacts { get; set; }

        public string Email { get; set; }

        public string Avatar { get; set; }

        public virtual Location Address { get; set; }

        public int? AddressId { get; set; }

        public virtual ICollection<User> Friends { get; set; }

        public virtual ICollection<UserToEvent> Events { get; set; }

        public virtual ICollection<Invite> IncomingInvites { get; set; }

        public virtual ICollection<Invite> OutgoingInvites { get; set; }

        public virtual ICollection<Request> Requests { get; set; }

        public virtual ICollection<Dialog> Dialogs { get; set; }

        public virtual ICollection<UnreadMessage> UnreadMessages { get; set; }

        public virtual ICollection<MissedNotification> MissedNotifications { get; set; }

        public virtual ICollection<Notification> Notifications { get; set; }

        [NotMapped]
        public string FullName => FirstName + " " + LastName;
    }
}
