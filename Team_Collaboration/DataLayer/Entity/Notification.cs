﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Entity
{
    public enum NotificationType
    {
        EventCreated = 1,
        InvitationCreated = 2
    }

    public class Notification
    {
        public Notification()
        {
            MissedNotifications = new HashSet<MissedNotification>();
            Users = new HashSet<User>();
        }

        public string Id { get; set; }

        public DateTime TimeStamp { get; set; }

        public NotificationType Type { get; set; }

        public string Data { get; set; }

        [NotMapped]
        public bool Missed { get; set; }

        public virtual ICollection<User> Users { get; set; } 

        public virtual ICollection<MissedNotification> MissedNotifications { get; set; }
    }
}
