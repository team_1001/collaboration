﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Extensions;
using Newtonsoft.Json;

namespace DataLayer.Entity
{
    public enum ProposalState
    {
        Pending = 1,
        Accepted = 2,
        Declined = 3,
        Canceled = 4
    }

    public class Invite : IEntity
    {
        public virtual User User { get; set; }

        public int UserId { get; set; }

        public virtual User InvitedUser { get; set; }

        public int InvitedUserId { get; set; }

        public ProposalState State { get; set; }

        public virtual Event Event { get; set; }

        public int EventId { get; set; }

        [JsonIgnore]
        public DateTime TimeStamp { get; set; }

        [NotMapped]
        public long TimeStampTicks
        {
            get { return TimeStamp.Convert(); }
            set { TimeStamp = value.Convert(); }
        }
    }
}
