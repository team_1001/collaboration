﻿
namespace DataLayer.Entity
{
    public class EventRoles
    {
        public const string Host = "host";
        public const string User = "user";
        public const string Admin = "admin";
    }

    public class UserToEvent : IEntity
    {
        public virtual User User { get; set; }

        public virtual Event Event { get; set; }

        public int UserId { get; set; }

        public int EventId { get; set; }

        public string Role { get; set; }
    }
}
