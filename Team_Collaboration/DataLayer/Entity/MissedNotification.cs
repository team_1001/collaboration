﻿namespace DataLayer.Entity
{
    public class MissedNotification
    {
        public string NotificationId { get; set; }

        public int UserId { get; set; }
    }
}
