﻿using DataLayer.Entity;
using FluentValidation;

namespace DataLayer.Validation
{
    public class EventValidator : AbstractValidator<Event>
    {
        public EventValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("The Name cannot be blank.")
                                        .Length(0, 100).WithMessage("The Name cannot be more than 100 characters.");

            RuleFor(x => x.Description).NotEmpty().WithMessage("The Description cannot be blank.");

            RuleFor(x => x.CategoryId).GreaterThan(0).WithMessage("Category must have value");

            RuleFor(x => x.EventType)
                .Must(x => (int) x > 0)
                .WithMessage("Invalid event type");

            RuleFor(x => x.DateEnds).GreaterThanOrEqualTo(x => x.DateStarts)
                .WithMessage("The EndTime must be greater than or equal to the start time");
        }
    }
}
