﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Entity;
using FluentValidation;

namespace DataLayer.Validation
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("The First Name cannot be blank.")
                .Length(0, 100).WithMessage("The First Name cannot be more than 100 characters.");

            RuleFor(x => x.LastName).NotEmpty().WithMessage("The Last Name cannot be blank.")
                .Length(0, 100).WithMessage("The Last Name cannot be more than 100 characters.");

            RuleFor(x => x.UserName).NotEmpty().WithMessage("The User Name cannot be blank.")
                .Length(0, 100).WithMessage("The User Name cannot be more than 100 characters.");

            RuleFor(x => x.Email).EmailAddress().WithMessage("The Email address is invalid");
        }
    }
}
