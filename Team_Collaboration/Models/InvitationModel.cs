﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models
{
    [Serializable]
    public class InvitationModel
    {
        [JsonProperty("userIds")]
        public IEnumerable<int> UserIds { get; set; }

        public int EventId { get; set; }
    }
}
