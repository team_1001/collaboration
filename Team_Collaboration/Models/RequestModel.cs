﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models
{
    public class RequestModel
    {
        public int UserId { get; set; }

        public int EventId { get; set; }
    }

    public class CreatedRequestModel : RequestModel
    {
        [JsonProperty("userIds")]
        public IEnumerable<int> UserIds { get; set; }
    }
}
