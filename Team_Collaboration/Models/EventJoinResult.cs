﻿namespace Models
{
    public class EventJoinResult
    {
        public bool Success { get; set; }

        public CreatedRequestModel Request { get; set; }
    }
}
