﻿using DataLayer.Entity;

namespace Models
{
    public class EventResult
    {
        public Event Event { get; set; }

        public bool IsHost { get; set; }

        public bool IsJoined { get; set; }

        public bool IsRequested { get; set; }

        public bool IsInvited { get; set; }

        public bool IsPublic { get; set; }

        public int UserId { get; set; }
    }
}
