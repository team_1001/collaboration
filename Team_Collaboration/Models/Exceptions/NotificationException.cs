﻿using System;

namespace Models.Exceptions
{
    public class NotificationException : Exception
    {
        public NotificationException(string message) : base(message)
        {
            
        }
    }
}
