﻿using System;

namespace Models.Exceptions
{
    public class UserException : Exception
    {
        public UserException(string message) : base(message)
        {
            
        }
    }
}
