﻿using System;

namespace Models.Exceptions
{
    public class EventException : Exception
    {
        public EventException(string message) : base(message)
        {
            
        }
    }
}
