﻿using System;

namespace Models.Exceptions
{
    public class RequestException : Exception
    {
        public RequestException(string message) : base(message)
        {

        }
    }
}
