﻿using System;

namespace Models.Hub
{
    [Serializable]
    public class HubExistingMessage
    {
        public string Id { get; set; }

        public string DialogId { get; set; }

        public int ReceiverId { get; set; }
    }
}
