using System;
using DataLayer.Entity;

namespace Models.Hub
{
    [Serializable]
    public class HubNewMessage
    {
        public string Id { get; set; }

        public string DialogId { get; set; }

        public string Content { get; set; }

        public int SenderId { get; set; }

        public string SenderName { get; set; }

        public long TimeStamp { get; set; }

        public MessageType Type { get; set; }
    }
}