﻿using System;

namespace Models.Hub
{
    [Serializable]
    public class HubNotificationMessage
    {
        public string NotificationId { get; set; }

        public int FromUserId { get; set; }

        public bool ForAll { get; set; }
    }
}
