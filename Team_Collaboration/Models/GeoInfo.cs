﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class GeoInfo
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Radius { get; set; }
    }
}
