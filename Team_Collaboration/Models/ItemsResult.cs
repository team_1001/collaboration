﻿using System.Collections.Generic;
using System.Linq;
using DataLayer.Entity;

namespace Models
{
    public class UserInfo
    {
        public User User { get; set; }

        public bool IsFriend { get; set; }

        public bool IsSelf { get; set; }
    }

    public class ItemsResult<TItem>
    {
        public IEnumerable<TItem> Items { get; private set; }

        public int Total { get; private set; }

        public ItemsResult(IEnumerable<TItem> items, int total)
        {
            Items = items;
            Total = total;
        }

        public ItemsResult(IEnumerable<TItem> items)
        {
            Items = items;
            Total = items.Count();
        }
    }
}
