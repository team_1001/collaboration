﻿using System;
using System.IO;
using System.Threading.Tasks;
using Autofac.Extras.NLog;

namespace BusinessLayer.Services.Interfaces
{
    public interface IFileService
    {
        Task<bool> WriteToFileAsync(string filePath, byte[] data);

        Task<byte[]> ReadFromFileAsync(string filePath, int bufferSize = 4096);

        string MapPath(string path);

        string GetFilesPath();

        bool Copy(string from, string to, bool deleteOld = true);

        void CreateIfNotExists(string path);
    }

    class FileService : IFileService
    {
        private readonly ILogger _logger;

        public FileService(ILogger logger)
        {
            _logger = logger;
        }

        public async Task<bool> WriteToFileAsync(string filePath, byte[] data)
        {
            byte[] buffer = data;

            Int32 offset = 0;

            Int32 sizeOfBuffer = 4096;

            bool result = true;
            try
            {
                using (FileStream fileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write,

                FileShare.None, bufferSize: sizeOfBuffer, useAsync: true))
                {
                    await fileStream.WriteAsync(buffer, offset, buffer.Length);
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        public async Task<byte[]> ReadFromFileAsync(string filePath, int bufferSize = 4096)
        {

            if (bufferSize < 1024)
                throw new ArgumentNullException("bufferSize");

            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentNullException("filePath");

            byte[] result = null;
            try
            {
                byte[] buffer = new byte[bufferSize];

                using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read,
                FileShare.Read, bufferSize: bufferSize, useAsync: true))
                using (MemoryStream ms = new MemoryStream())
                {
                    Int32 bytesRead = 0;

                    while ((bytesRead = await fileStream.ReadAsync(buffer, 0, buffer.Length)) > 0)
                    {
                        await ms.WriteAsync(buffer, 0, bytesRead);
                    }

                    result = ms.ToArray();
                }
            }
            catch (Exception)
            {
            }

            return result;
        }

        public string MapPath(string path)
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            return Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory), "Files", path);
        }

        public string GetFilesPath()
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            return Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory), "Files");
        }

        public bool Copy(string @from, string to, bool deleteOld = true)
        {
            if (!File.Exists(@from))
                throw new Exception($"File {@from} doesn't exist");

            try
            {
                File.Copy(@from, to);
                if (deleteOld)
                    File.Delete(@from);

                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public void CreateIfNotExists(string path)
        {
            if (Path.HasExtension(path))
                throw new Exception("Specified path is not a directory");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }
    }
}
