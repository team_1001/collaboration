﻿namespace BusinessLayer.Services.Interfaces
{
   public interface IGeoLocator
   {
       string GetCountryCode(string userIp);
   }
}
