﻿using System.Threading.Tasks;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models;
using Models.Paging;

namespace BusinessLayer.Services.Interfaces
{
    public interface INotificationProvider
    {
        Task<ItemsResult<Notification>> Get(int userId, PageInfo pageInfo, bool onlyMissed = true, params NotificationType[] notificationTypes);

        Task<bool> Update(string notificationId, int userId);

        Task<bool> Update(NotificationType[] types, int userId);
    }

    class NotificationProvider : INotificationProvider
    {
        private readonly NotificationsRepository _notificationsRepository;

        public NotificationProvider(NotificationsRepository notificationsRepository)
        {
            _notificationsRepository = notificationsRepository;
        }

        public async Task<ItemsResult<Notification>> Get(int userId, PageInfo pageInfo, bool onlyMissed = true, params NotificationType[] notificationTypes)
        {
            return await _notificationsRepository.GetNotifications(userId, pageInfo, onlyMissed, notificationTypes ?? new NotificationType[0]);
        }

        public async Task<bool> Update(string notificationId, int userId)
        {
            return await _notificationsRepository.UpdateNotification(notificationId, userId);
        }

        public async Task<bool> Update(NotificationType[] types, int userId)
        {
            return await _notificationsRepository.UpdateNotification(types, userId);
        }
    }
}
