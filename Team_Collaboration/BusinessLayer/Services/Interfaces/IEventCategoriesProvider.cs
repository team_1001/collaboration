﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLayer.Repositories;
using DataLayer.Entity;

namespace BusinessLayer.Services.Interfaces
{
    public interface IEventCategoriesProvider
    {
        Task<IEnumerable<EventCategory>> GetAll();
    }

    class EventCategoriesProvider : IEventCategoriesProvider
    {
        private readonly EventsRepository _eventsRepository;

        public EventCategoriesProvider(EventsRepository eventsRepository)
        {
            _eventsRepository = eventsRepository;
        }

        public async Task<IEnumerable<EventCategory>> GetAll()
        {
            return await _eventsRepository.GetCategories();
        }
    }
}
