﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models;
using Models.Paging;

namespace BusinessLayer.Services.Interfaces
{
    public interface IRequestsProvider
    {
        Task<CreatedRequestModel> Add(RequestModel model);

        Task<bool> Cancel(RequestModel model);

        Task<bool> Accept(Request model);

        Task<bool> Decline(Request model);

        Task<ItemsResult<Request>> GetOutgoing(int userId, PageInfo pageInfo);

        Task<ItemsResult<Request>> GetIncoming(int userId, PageInfo pageInfo);

        Task<ItemsResult<Request>> GetIncomingByEvent(int eventId, PageInfo pageInfo);
    }

    class RequestsProvider : IRequestsProvider
    {
        private readonly RequestsRepository _repository;

        public RequestsProvider(RequestsRepository repository)
        {
            _repository = repository;
        }

        public async Task<CreatedRequestModel> Add(RequestModel model)
        {
            return await _repository.Create(model.EventId, model.UserId);
        }

        public async Task<bool> Cancel(RequestModel model)
        {
            return await _repository.Update(new Request()
                    {
                        EventId = model.EventId,
                        UserId = model.UserId,
                        TimeStamp = DateTime.UtcNow,
                        State = ProposalState.Canceled
                    });
        }

        public async Task<bool> Accept(Request model)
        {
            return await _repository.Accept(model);
        }

        public async Task<bool> Decline(Request model)
        {
            return await _repository.Decline(model);
        }

        public async Task<ItemsResult<Request>> GetOutgoing(int userId, PageInfo pageInfo)
        {
            return await _repository.GetOutgoing(userId, pageInfo);
        }

        public async Task<ItemsResult<Request>> GetIncoming(int userId, PageInfo pageInfo)
        {
            return await _repository.GetIncomingByUser(userId, pageInfo);
        }

        public async Task<ItemsResult<Request>> GetIncomingByEvent(int eventId, PageInfo pageInfo)
        {
            return await _repository.GetIncoming(eventId, pageInfo);
        }
    }
}
