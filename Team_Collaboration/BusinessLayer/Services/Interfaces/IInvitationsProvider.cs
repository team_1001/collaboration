﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.Messaging;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models;
using Models.Hub;
using Models.Paging;
using Newtonsoft.Json;

namespace BusinessLayer.Services.Interfaces
{
    public interface IInvitationsProvider
    {
        Task<IList<int>> Add(InvitationModel model, int hostId);

        Task<bool> Accept(Invite model);

        Task<bool> Decline(Invite model);

        Task<ItemsResult<Invite>> GetOutgoing(int hostId, PageInfo pageInfo);

        Task<ItemsResult<Invite>> GetIncoming(int userId, PageInfo pageInfo);

        Task<ItemsResult<Invite>> GetOutgoing(int eventId, int hostId, PageInfo pageInfo);

        Task<ItemsResult<Invite>> GetIncoming(int eventId, int userId, PageInfo pageInfo);
    }

    class InvitationsProvider : IInvitationsProvider
    {
        private readonly InvitationsRepository _repository;
        private readonly NotificationsRepository _notificationsRepository;
        private readonly HubNotificationService _notificationService;

        public InvitationsProvider(
            InvitationsRepository repository, 
            NotificationsRepository notificationsRepository, 
            HubNotificationService notificationService)
        {
            _repository = repository;
            _notificationsRepository = notificationsRepository;
            _notificationService = notificationService;
        }

        public async Task<IList<int>> Add(InvitationModel model, int hostId)
        {
            IList<int> invitedUsersIds = await _repository.Create(hostId, model.UserIds, model.EventId);
            NotificationCreationResult result =
                await
                    _notificationsRepository.AddNotifications(model.UserIds.ToArray(), NotificationType.InvitationCreated,
                        JsonConvert.SerializeObject(model));

            if (result.Success)
                await _notificationService.SendNotificationAsync(new HubNotificationMessage()
                {
                    FromUserId = hostId,
                    NotificationId = result.NotificationId
                });

            return invitedUsersIds;
        }

        public async Task<bool> Accept(Invite model)
        {
            return await _repository.Accept(model);
        }

        public async Task<bool> Decline(Invite model)
        {
            return await _repository.Decline(model);
        }

        public async Task<ItemsResult<Invite>> GetOutgoing(int hostId, PageInfo pageInfo)
        {
            return await _repository.GetOutgoing(hostId, pageInfo);
        }

        public async Task<ItemsResult<Invite>> GetIncoming(int userId, PageInfo pageInfo)
        {
            return await _repository.GetIncoming(userId, pageInfo);
        }

        public async Task<ItemsResult<Invite>> GetOutgoing(int eventId, int hostId, PageInfo pageInfo)
        {
            return await _repository.GetOutgoing(eventId, hostId, pageInfo);
        }

        public async Task<ItemsResult<Invite>> GetIncoming(int eventId, int userId, PageInfo pageInfo)
        {
            return await _repository.GetIncoming(eventId, userId, pageInfo);
        }
    }
}
