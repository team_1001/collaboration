﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using DataAccessLayer.Repositories;
using DataLayer.Entity;

namespace BusinessLayer.Services.Interfaces
{
    public interface IUserFriendsProvider
    {
        Task<IEnumerable<User>> GetFriends(int userId);

        Task<bool> Add(int userId, int friendId);

        Task<bool> Remove(int userId, int friendId);
    }

    class UserFriendsProvider : IUserFriendsProvider
    {
        private readonly ILogger _logger;
        private readonly UsersRepository _usersRepository;

        public UserFriendsProvider(
            ILogger logger,
            UsersRepository usersRepository)
        {
            _logger = logger;
            _usersRepository = usersRepository;
        }

        public async Task<IEnumerable<User>> GetFriends(int userId)
        {
            return await _usersRepository.GetFriends(userId);
        }

        public async Task<bool> Add(int userId, int friendId)
        {
            return await _usersRepository.AddFriend(userId, friendId);
        }

        public async Task<bool> Remove(int userId, int friendId)
        {
            return await _usersRepository.RemoveFriend(userId, friendId);
        }
    }
}
