﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLayer.Messaging;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models;
using Models.Hub;
using Models.Paging;
using Newtonsoft.Json;

namespace BusinessLayer.Services.Interfaces
{
    public interface IEventProvider
    {
        Task<ItemsResult<Event>> GetAll(PageInfo pageInfo);

        Task<ItemsResult<Event>> GetAllGeo(GeoInfo geoInfo, PageInfo pageInfo);

        Task<EventResult> Get(int eventId, int userId);

        Task<IEnumerable<EventCategory>> GetCategories();

        Task<bool> Add(Event @event, int userId);

        Task<bool> Update(Event @event, int userId);

        Task<bool> Delete(int eventId, int userId);
    }

    class EventProvider : IEventProvider
    {
        private readonly EventsRepository _eventsRepository;
        private readonly NotificationsRepository _notificationsRepository;
        private readonly HubNotificationService _notificationService;

        public EventProvider(
            EventsRepository eventsRepository, 
            NotificationsRepository notificationsRepository, 
            HubNotificationService notificationService)
        {
            _eventsRepository = eventsRepository;
            _notificationsRepository = notificationsRepository;
            _notificationService = notificationService;
        }

        public async Task<ItemsResult<Event>> GetAll(PageInfo pageInfo)
        {
            return await _eventsRepository.GetEvents(pageInfo);
        }

        public async Task<ItemsResult<Event>> GetAllGeo(GeoInfo geoInfo, PageInfo pageInfo)
        {
            return await _eventsRepository.GetGeoEvents(geoInfo, new PageInfo()); //todo
        }

        public async Task<EventResult> Get(int eventId, int userId)
        {
            return await _eventsRepository.Get(eventId, userId);
        }

        public async Task<IEnumerable<EventCategory>> GetCategories()
        {
            return await _eventsRepository.GetCategories();
        }

        public async Task<bool> Add(Event @event, int userId)
        {
            bool saveResult = await _eventsRepository.Create(@event, userId);
            
            NotificationCreationResult result =
                await
                    _notificationsRepository.AddNotificationsToAllExcept(userId, NotificationType.EventCreated,
                        JsonConvert.SerializeObject(new { @event.Id }));

            if (result.Success)
                await _notificationService.SendNotificationAsync(new HubNotificationMessage()
                {
                    FromUserId = userId,
                    ForAll = true,
                    NotificationId = result.NotificationId
                });

            return saveResult;
        }

        public async Task<bool> Update(Event @event, int userId)
        {
            return await _eventsRepository.Update(@event, userId);
        }

        public async Task<bool> Delete(int eventId, int userId)
        {
            return await _eventsRepository.Delete(eventId, userId);
        }
    }
}
