﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models;
using Models.Paging;

namespace BusinessLayer.Services.Interfaces
{
    public interface IMessagesProvider
    {
        Task<IEnumerable<Dialog>> GetDialogs(int userId);

        Task<IEnumerable<Message>> GetMessages(int userId);

        Task<int> GetUnreadMessagesCount(int userId);

        Task<ItemsResult<Message>> GetMessages(int userId, string dialogId, PageInfo pageInfo);

        Task<IEnumerable<int>> GetRelatedUsers(int userId);
    }

    class MessagesProvider : IMessagesProvider
    {
        private readonly MessagesRepository _messagesRepository;

        public MessagesProvider(MessagesRepository messagesRepository)
        {
            _messagesRepository = messagesRepository;
        }

        public async Task<IEnumerable<Dialog>> GetDialogs(int userId)
        {
            return await _messagesRepository.GetDialogs(userId);
        }

        public Task<IEnumerable<Message>> GetMessages(int userId)
        {
            throw new NotImplementedException();
        }

        public async Task<int> GetUnreadMessagesCount(int userId)
        {
            return await _messagesRepository.GetUnreadMessagesCount(userId);
        }

        public async Task<ItemsResult<Message>> GetMessages(int userId, string dialogId, PageInfo pageInfo)
        {
            return await _messagesRepository.GetMessages(userId, dialogId, pageInfo);
        }

        public async Task<IEnumerable<int>> GetRelatedUsers(int userId)
        {
            return await _messagesRepository.GetRelatedUsers(userId);
        }
    }
}
