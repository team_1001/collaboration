﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models;
using Models.Exceptions;
using Models.Paging;

namespace BusinessLayer.Services.Interfaces
{
    public interface IUserProvider
    {
        Task<ItemsResult<UserInfo>> GetAll(int userId);

        Task<User> Get(int userId);

        Task<bool> Update(User user);

        Task<IEnumerable<Event>> GetEvents(int userId, string[] roles);

        Task<IEnumerable<UserToEvent>> GetParticipants(int eventId, int userId, string[] roles);

        IEnumerable<Event> GetEvents();

        Task<EventJoinResult> Join(int userId, int eventId);

        Task<bool> Leave(int userId, int eventId);
    }

    class UserProvider : IUserProvider
    {
        private readonly UsersRepository _usersRepository;
        private readonly EventsRepository _eventsRepository;
        private readonly RequestsRepository _requestsRepository;

        public UserProvider(UsersRepository usersRepository, EventsRepository eventsRepository, RequestsRepository requestsRepository)
        {
            _usersRepository = usersRepository;
            _eventsRepository = eventsRepository;
            _requestsRepository = requestsRepository;
        }

        public async Task<ItemsResult<UserInfo>> GetAll(int userId)
        {
            return await _usersRepository.GetAll(userId, new PageInfo());
        }

        public async Task<User> Get(int userId)
        {
            return await _usersRepository.Get(userId);
        }

        public async Task<bool> Update(User user)
        {
            return await _usersRepository.Update(user);
        }

        public async Task<IEnumerable<Event>> GetEvents(int userId, string[] roles)
        {
            return await _eventsRepository.GetEvents(userId, roles ?? new string[0]);
        }

        public async Task<IEnumerable<UserToEvent>> GetParticipants(int eventId, int userId, string[] roles)
        {
            return await _eventsRepository.GetParticipants(eventId, userId, roles ?? new string[0]);
        }

        public IEnumerable<Event> GetEvents()
        {
            throw new System.NotImplementedException();
        }

        public async Task<EventJoinResult> Join(int userId, int eventId)
        {
            var @event = await _eventsRepository.Get(eventId);
            if (@event == null)
                throw new EventException($"Event {eventId} doesn't exist");

            if (@event.EventType == EventType.Public)
                return new EventJoinResult()
                {
                    Success = await _eventsRepository.Join(eventId, userId)
                };

            var request = await _requestsRepository.Create(eventId, userId);
            return new EventJoinResult()
            {
                Success = request != null,
                Request = request
            };
        }

        public async Task<bool> Leave(int userId, int eventId)
        {
            return await _eventsRepository.Leave(eventId, userId);
        }
    }
}
