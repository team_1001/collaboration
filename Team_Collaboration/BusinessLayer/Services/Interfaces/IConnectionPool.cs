﻿using System.Collections.Generic;

namespace BusinessLayer.Services.Interfaces
{
    public interface IConnectionPool<in TKey>
    {
        IEnumerable<string> GetConnections(TKey userId);
    }
}
