﻿using System.Net;
using BusinessLayer.Services.Interfaces;
using Newtonsoft.Json;

namespace BusinessLayer.Services.Implementations
{
   public class GeoLocator : IGeoLocator
    {
       public string GetCountryCode(string userIp)
       {
            string url = "http://freegeoip.net/json/" + userIp;
            WebClient client = new WebClient();
            string jsonstring = client.DownloadString(url);
            dynamic dynObj = JsonConvert.DeserializeObject(jsonstring);
            return dynObj.country_code;
        }
    }
}
