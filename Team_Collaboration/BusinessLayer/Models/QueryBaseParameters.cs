﻿namespace BusinessLayer.Models
{
  public  class QueryBaseParameters
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public string Query { get; set; }
    }
}
