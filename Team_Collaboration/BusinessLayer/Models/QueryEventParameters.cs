﻿namespace BusinessLayer.Models
{
   public class QueryEventParameters : QueryBaseParameters
    {
        public int UserId { get; set; }

        public EventFilterType FilterType { get; set; }
    }

    public enum EventFilterType
    {
        Suggested,
        Popular,
        All
    }
}
