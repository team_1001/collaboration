﻿using Autofac;
using BusinessLayer.Config;
using BusinessLayer.Messaging;
using BusinessLayer.Messaging.Notifications;
using BusinessLayer.Services.Interfaces;
using Common.Configuration;
using Models.Hub;
using Queue.Core;

namespace BusinessLayer.AutoFac
{
    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FileService>().As<IFileService>();
            builder.RegisterType<Configurator>().As<IConfigurator>();
            builder.RegisterType<AppConfigurationProvider>().As<IConfigurationProvider>();

            builder.RegisterType<MessageProducer>().As<IMessageProducer>();

            builder.RegisterType<HubMessagePersister>()
                .As<MessageProcessorBase<HubNewMessage>>()
                .As<IMessageProcessor>()
                .As<IMessageProcessor<HubNewMessage>>();

            builder.RegisterType<HubNewMessageNotifier>()
                .As<MessageProcessorBase<HubNewMessage>>()
                .As<IMessageProcessor>()
                .As<IMessageProcessor<HubNewMessage>>();

            builder.RegisterType<HubMessageUpdater>()
                .As<MessageProcessorBase<HubExistingMessage>>()
                .As<IMessageProcessor>()
                .As<IMessageProcessor<HubExistingMessage>>();

            builder.RegisterType<HubExistingMessageNotifier>()
                .As<MessageProcessorBase<HubExistingMessage>>()
                .As<IMessageProcessor>()
                .As<IMessageProcessor<HubExistingMessage>>();

            builder.RegisterType<HubNotificationMessageNotifier>()
               .As<MessageProcessorBase<HubNotificationMessage>>()
               .As<IMessageProcessor>()
               .As<IMessageProcessor<HubNotificationMessage>>();

            builder.RegisterType<CreatedEventNotifier>()
               .As<INotificationNotifier>();

            builder.RegisterType<CreatedInvitationNotifier>()
               .As<INotificationNotifier>();

            builder.RegisterType<MessageProcessorManager>().AsSelf();
            builder.RegisterType<HubProxy>().AsSelf();
            builder.RegisterType<HubNotificationService>().AsSelf();
            
            builder.RegisterType<EventCategoriesProvider>().As<IEventCategoriesProvider>();
            builder.RegisterType<EventProvider>().As<IEventProvider>();
            builder.RegisterType<UserProvider>().As<IUserProvider>();
            builder.RegisterType<UserFriendsProvider>().As<IUserFriendsProvider>();
            builder.RegisterType<MessagesProvider>().As<IMessagesProvider>();
            builder.RegisterType<InvitationsProvider>().As<IInvitationsProvider>();
            builder.RegisterType<RequestsProvider>().As<IRequestsProvider>();
            builder.RegisterType<NotificationProvider>().As<INotificationProvider>();

            builder.RegisterModule(new DataAccessLayer.AutoFac.MainModule());
        }
    }
}
