﻿using System.Threading.Tasks;
using DataLayer.Entity;
using Microsoft.AspNet.SignalR;

namespace BusinessLayer.Messaging.Notifications
{
    public interface INotificationNotifier
    {
        bool CanNotify(NotificationType notificationType);

        Task Notify(int userId, Notification notification, IHubContext hubContext);
    }
}
