﻿using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BusinessLayer.Messaging.Notifications
{
    class CreatedEventNotifier : INotificationNotifier
    {
        private readonly IConnectionPool<string> _connectionPool;

        public CreatedEventNotifier(IConnectionPool<string> connectionPool)
        {
            _connectionPool = connectionPool;
        }

        public bool CanNotify(NotificationType notificationType)
        {
            return notificationType == NotificationType.EventCreated;
        }

        public async Task Notify(int userId, Notification notification, IHubContext hubContext)
        {
            var userConnections = _connectionPool.GetConnections(userId.ToString()).ToArray();
            string data = notification.Data;
            var jObj = JObject.Parse(data);
            jObj.Merge(JObject.FromObject(new { NotificationId = notification.Id }));
            var @dynamic = JsonConvert.DeserializeAnonymousType(jObj.ToString(),
                new {Id = default(string), NotificationId = default(string)});

            await hubContext.Clients.AllExcept(userConnections).onEventCreated(@dynamic);
        }
    }
}