using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Entity;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BusinessLayer.Messaging.Notifications
{
    class CreatedInvitationNotifier : INotificationNotifier
    {
        public bool CanNotify(NotificationType notificationType)
        {
            return notificationType == NotificationType.InvitationCreated;
        }

        public async Task Notify(int userId, Notification notification, IHubContext hubContext)
        {
            string data = notification.Data;
            var jObj = JObject.Parse(data);
            jObj.Merge(JObject.FromObject(new { NotificationId = notification.Id }));

            var @dynamic = JsonConvert.DeserializeAnonymousType(jObj.ToString(),
                new
                {
                    EventId = default(string),
                    UserIds = default(IEnumerable<string>),
                    NotificationId = default(string)
                });

            await hubContext.Clients.Users(notification.MissedNotifications.Select(x => x.UserId.ToString()).ToList())
                .onInvitationCreated(@dynamic);
        }
    }
}