﻿using System;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using Common.Configuration;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models.Hub;
using Queue.Core;

namespace BusinessLayer.Messaging
{
    class HubNotificationMessageNotifier : MessageProcessorBase<HubNotificationMessage>
    {
        private readonly HubProxy _hubProxy;
        private readonly NotificationsRepository _notificationsRepository;

        public HubNotificationMessageNotifier(
            ILogger logger, 
            IMessageProducer messageProducer, 
            IConfigurationProvider configuration,
            HubProxy hubProxy,
            NotificationsRepository notificationsRepository) 
            : base(logger, messageProducer, configuration)
        {
            _hubProxy = hubProxy;
            _notificationsRepository = notificationsRepository;
        }

        public override MessageProcessorType ProcessorType => MessageProcessorType.NotificationNotifier;

        protected override string Topic => Configuration.AppSettings["NotificationNotifyTopic"];

        protected override string QueueName => Configuration.AppSettings["NotificationQueue"];


        protected override async Task<bool> ProcessInternal(MessageInfo<HubNotificationMessage> messageInfo)
        {
            Notification notification = await _notificationsRepository.Get(messageInfo.Message.NotificationId, !messageInfo.Message.ForAll);
            if (notification == null)
            {
                Logger.Error("Can't read notification {0}", messageInfo.Message.NotificationId);
                return false;
            }

            try
            {
                await _hubProxy.Notify(messageInfo.Message.FromUserId, notification);

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("Error notifying clients: {0}", e);
            }

            return false;
        }

        protected override async Task<bool> Next(MessageInfo<HubNotificationMessage> message)
        {
            return await Task.FromResult(true);
        }
    }
}
