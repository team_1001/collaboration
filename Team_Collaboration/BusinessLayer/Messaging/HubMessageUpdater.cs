﻿using System.Threading.Tasks;
using Autofac.Extras.NLog;
using Common.Configuration;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models.Hub;
using Queue.Core;

namespace BusinessLayer.Messaging
{
    class HubMessageUpdater : MessageProcessorBase<HubExistingMessage>
    {
        private readonly IConfigurationProvider _configuration;
        private readonly MessagesRepository _messagesRepository;

        public HubMessageUpdater(
            ILogger logger, 
            IConfigurationProvider configuration,
            MessagesRepository messagesRepository,
            IMessageProducer messageProducer) 
            : base(logger, messageProducer, configuration)
        {
            _configuration = configuration;
            _messagesRepository = messagesRepository;
        }

        public override MessageProcessorType ProcessorType => MessageProcessorType.Updater;

        protected override string Topic => Configuration.AppSettings["UpdateTopic"];

        protected override string QueueName => Configuration.AppSettings["UpdaterQueue"];


        protected override async Task<bool> ProcessInternal(MessageInfo<HubExistingMessage> message)
        {
            return await _messagesRepository.ReadMessage(message.Message);
        }

        protected override async Task<bool> Next(MessageInfo<HubExistingMessage> messageInfo)
        {
            Message message = await _messagesRepository.GetMessage(messageInfo.Message.Id);
            if (message == null)
            {
                Logger.Error("Can't read message {0}", messageInfo.Message.Id);
                return false;
            }

            if (!message.Viewed)
            {
                Logger.Trace("Message {0} hasn't been viewed by all the clients yet. Skipping...", message.Id);
                return false;
            }

            string notifyTopic = _configuration.AppSettings["NotifyExistingTopic"];
            if (!await MessageProducer.Produce(messageInfo.Message, notifyTopic))
            {
                Logger.Fatal("Can't push message {0} to the notify existing queue. Deliery tag: {1}", 
                    messageInfo.Message, 
                    messageInfo.DeliveryTag);

                return false;
            }

            return true;
        }
    }
}
