﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using Common.Configuration;
using Queue.Core;

namespace BusinessLayer.Messaging
{
    public enum MessageProcessorType
    {
        Persister = 1,
        Updater = 2,
        NewMessageNotifier = 3,
        ExistingMesageNotifier = 4,
        NotificationNotifier = 5
    }

    public interface IMessageProcessor
    {
        MessageProcessorType ProcessorType { get; }

        IMessageConsumer CreateConsumer();
    }

    public abstract class MessageProcessorBase<TMessage> : IMessageProcessor, IMessageProcessor<TMessage> where TMessage : class
    {
        protected readonly ILogger Logger;
        protected readonly IMessageProducer MessageProducer;
        protected readonly IConfigurationProvider Configuration;

        protected MessageProcessorBase(
            ILogger logger,
            IMessageProducer messageProducer, 
            IConfigurationProvider configuration)
        {
            Logger = logger;
            MessageProducer = messageProducer;
            Configuration = configuration;
        }

        public async Task<bool> Process(MessageInfo<TMessage> messageInfo)
        {
            TMessage message = messageInfo.Message;

            Logger.Trace("Processing message {0}", message);

            bool result = await ProcessInternal(messageInfo);

            if (result)
            {
                Logger.Trace("Message {0} was processed successully");

                await Next(messageInfo);
                
                return true;
            }

            var headers = new Dictionary<string, object> { { "retry", messageInfo.Retry + 1 } };
            Logger.Info("Message {0} wasn't processed properly. Requeueing message. Retry: {1}", message, messageInfo.Retry + 1);

            // pushing message back to the same queue, increasing retries count
            if (await MessageProducer.Produce(message, Topic, headers))
                // acking the old message if new message was pushed successfully
                return true;

            Logger.Fatal("Can't requeue message {0}", message);
            return false;
        }

        public abstract MessageProcessorType ProcessorType { get; }

        protected abstract string Topic { get; }

        protected abstract string QueueName { get; }

        public IMessageConsumer CreateConsumer()
        {
            return new MessageConsumer<TMessage>(Configuration, this, QueueName, new List<string> { Topic });
        }

        protected abstract Task<bool> ProcessInternal(MessageInfo<TMessage> message);

        protected abstract Task<bool> Next(MessageInfo<TMessage> message);
    }
}
