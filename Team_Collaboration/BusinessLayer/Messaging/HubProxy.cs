﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.Messaging.Notifications;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using Microsoft.AspNet.SignalR;
using Models.Hub;
using Newtonsoft.Json;

namespace BusinessLayer.Messaging
{
    public class HubProxy
    {
        private readonly IHubContext _hubContext;
        private readonly IEnumerable<INotificationNotifier> _notificationNotifiers;

        public HubProxy(IHubContext hubContext, IEnumerable<INotificationNotifier> notificationNotifiers)
        {
            _hubContext = hubContext;
            _notificationNotifiers = notificationNotifiers;
        }

        public async Task Notify(HubNewMessage message, IEnumerable<int> receivers)
        {
            await _hubContext.Clients
                    .Users(receivers.Select(x => x.ToString()).ToList())
                    .messageReceived(message);
        }

        public async Task Notify(Message message)
        {
            await _hubContext
                    .Clients.User(message.SenderId.ToString())
                    .messageDelivered(new { message.Id, message.DialogId });
        }

        public async Task Notify(int userId, Notification notification)
        {
            var notifier = _notificationNotifiers.FirstOrDefault(x => x.CanNotify(notification.Type));
            if (notifier == null)
                throw new Exception($"Notifier for {notification.Type} wasn't registered");

            await notifier.Notify(userId, notification, _hubContext);
        }
    }
}
