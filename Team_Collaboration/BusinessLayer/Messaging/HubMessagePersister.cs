﻿using System.Threading.Tasks;
using Autofac.Extras.NLog;
using Common.Configuration;
using DataAccessLayer.Repositories;
using Models.Hub;
using Queue.Core;

namespace BusinessLayer.Messaging
{
    class HubMessagePersister : MessageProcessorBase<HubNewMessage>
    {
        private readonly IConfigurationProvider _configuration;
        private readonly MessagesRepository _messagesRepository;

        public HubMessagePersister(
            ILogger logger, 
            IConfigurationProvider configuration, 
            MessagesRepository messagesRepository,
            IMessageProducer messageProducer) 
            : base(logger, messageProducer, configuration)
        {
            _configuration = configuration;
            _messagesRepository = messagesRepository;
        }

        public override MessageProcessorType ProcessorType => MessageProcessorType.Persister;

        protected override string Topic => Configuration.AppSettings["PersistTopic"];

        protected override string QueueName => Configuration.AppSettings["PersisterQueue"];

        protected override async Task<bool> ProcessInternal(MessageInfo<HubNewMessage> messageInfo)
        {
            return await _messagesRepository.AddMessage(messageInfo.Message);
        }

        protected override async Task<bool> Next(MessageInfo<HubNewMessage> messageInfo)
        {
            string notifyTopic = _configuration.AppSettings["NotifyNewTopic"];
            if (!await MessageProducer.Produce(messageInfo.Message, notifyTopic))
            {
                Logger.Fatal("Can't push message {0} to the update queue. Deliery tag: {1}", messageInfo.Message, messageInfo.DeliveryTag);
                return false;
            }

            return true;
        }
    }
}
