﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using Common.Configuration;
using DataAccessLayer.Repositories;
using Models.Hub;
using Queue.Core;

namespace BusinessLayer.Messaging
{
    class HubNewMessageNotifier : MessageProcessorBase<HubNewMessage>
    {
        private readonly HubProxy _proxy;
        private readonly MessagesRepository _messagesRepository;

        public HubNewMessageNotifier(
            ILogger logger, 
            IConfigurationProvider configuration, 
            IMessageProducer messageProducer, 
            HubProxy proxy,
            MessagesRepository messagesRepository) 
            : base(logger, messageProducer, configuration)
        {
            _proxy = proxy;
            _messagesRepository = messagesRepository;
        }

        public override MessageProcessorType ProcessorType => MessageProcessorType.NewMessageNotifier;

        protected override string Topic => Configuration.AppSettings["NotifyNewTopic"];

        protected override string QueueName => Configuration.AppSettings["NotifyNewQueue"];


        protected override async Task<bool> ProcessInternal(MessageInfo<HubNewMessage> messageInfo)
        {
            IEnumerable<int> receivers = await _messagesRepository.GetReceivers(messageInfo.Message.DialogId, messageInfo.Message.SenderId);
            if (receivers == null)
                return false;

            try
            {
                await _proxy.Notify(messageInfo.Message, receivers);

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("Error notifying clients: {0}", e);
            }

            return false;
        }

        protected override async Task<bool> Next(MessageInfo<HubNewMessage> message)
        {
            return await Task.FromResult(true);
        }
    }
}
