﻿using System.Threading.Tasks;
using Common.Configuration;
using Models.Hub;
using Queue.Core;

namespace BusinessLayer.Messaging
{
    public class HubNotificationService
    {
        private readonly IConfigurationProvider _configuration;
        private readonly IMessageProducer _messageProducer;

        public HubNotificationService(
            IConfigurationProvider configuration,
            IMessageProducer messageProducer)
        {
            _configuration = configuration;
            _messageProducer = messageProducer;
        }

        public async Task<bool> SendMessageAsync(HubNewMessage message)
        {
            return await _messageProducer.Produce(message, _configuration.AppSettings["PersistTopic"]);
        }

        public async Task<bool> SendNotificationAsync(HubNotificationMessage message)
        {
            return await _messageProducer.Produce(message, _configuration.AppSettings["NotificationNotifyTopic"]);
        }

        public async Task<bool> ReceiveMessageAsync(HubExistingMessage message)
        {
            return await _messageProducer.Produce(message, _configuration.AppSettings["UpdateTopic"]);
        }
    }
}
