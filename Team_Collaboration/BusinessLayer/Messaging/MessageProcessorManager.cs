﻿using System.Collections.Generic;
using System.Linq;
using Queue.Core;

namespace BusinessLayer.Messaging
{
    public class MessageProcessorManager
    {
        private List<IMessageConsumer> _consumers;
         
        private readonly IEnumerable<IMessageProcessor> _messageProcessors;

        public MessageProcessorManager(
            IEnumerable<IMessageProcessor> messageProcessors)
        {
            _messageProcessors = messageProcessors;
        }

        private List<IMessageConsumer> GetConsumers()
        {
            return _messageProcessors.Select(x => x.CreateConsumer()).ToList();
        }

        public void Start()
        {
            _consumers = GetConsumers();
            _consumers.ForEach(x => x.StartConsuming());
        }

        public void Stop()
        {
            _consumers.ForEach(x => x.StopConsuming());
        }
    }
}
