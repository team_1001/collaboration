﻿using System;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using Common.Configuration;
using DataAccessLayer.Repositories;
using DataLayer.Entity;
using Models.Hub;
using Queue.Core;

namespace BusinessLayer.Messaging
{
    class HubExistingMessageNotifier : MessageProcessorBase<HubExistingMessage>
    {
        private readonly MessagesRepository _messagesRepository;
        private readonly HubProxy _proxy;

        public HubExistingMessageNotifier(
            ILogger logger, 
            IConfigurationProvider configuration,
            MessagesRepository messagesRepository,
            HubProxy proxy,
            IMessageProducer messageProducer) 
            : base(logger, messageProducer, configuration)
        {
            _messagesRepository = messagesRepository;
            _proxy = proxy;
        }

        public override MessageProcessorType ProcessorType => MessageProcessorType.ExistingMesageNotifier;

        protected override string Topic => Configuration.AppSettings["NotifyExistingTopic"];

        protected override string QueueName => Configuration.AppSettings["NotifyExistingQueue"];

        protected override async Task<bool> ProcessInternal(MessageInfo<HubExistingMessage> messageInfo)
        {
            Message message = await _messagesRepository.GetMessage(messageInfo.Message.Id);
            if (message == null)
            {
                Logger.Error("Can't read message {0}", messageInfo.Message.Id);
                return false;
            }

            try
            {
                await _proxy.Notify(message);

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("Error notifying client {0}: {1}", message.SenderId, e);
            }

            return false;
        }

        protected override async Task<bool> Next(MessageInfo<HubExistingMessage> message)
        {
            return await Task.FromResult(true);
        }
    }
}
