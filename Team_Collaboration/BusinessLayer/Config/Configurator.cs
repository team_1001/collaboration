﻿using DataAccessLayer.Configuration;

namespace BusinessLayer.Config
{
    class Configurator : IConfigurator
    {
        private readonly Migrator _migrator;

        public Configurator(Migrator migrator)
        {
            _migrator = migrator;
        }

        public void Configure(string connectionString)
        {
            _migrator.Migrate(connectionString);
        }
    }

    public interface IConfigurator
    {
        void Configure(string connectionString);
    }
}
