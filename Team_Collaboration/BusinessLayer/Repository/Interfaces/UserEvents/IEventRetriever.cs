﻿using System.Collections.Generic;
using BusinessLayer.Models;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Interfaces.UserEvents
{
   public interface IEventRetriever
   {
       bool CanHandle(QueryEventParameters parameters);

       IEnumerable<Event> GetEvents(QueryEventParameters parameters);
   }
}
