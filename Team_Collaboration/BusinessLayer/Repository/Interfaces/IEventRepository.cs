﻿using System.Collections.Generic;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Interfaces
{
   public interface IEventRepository
   {
       Event GetEvent(int eventId);

       IEnumerable<Request> GetAllRequests(int eventId);

       IEnumerable<Invite> GetAllInvites(int eventId);

       IEnumerable<UserToEvent> GetAllUsers(int eventId);

        bool Create(int userId, Event @event);

        bool Edit(int userId,Event @event);

        bool Remove(int userId, int eventId);
    }
}
