﻿using System.Collections.Generic;
using BusinessLayer.Models;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Interfaces
{
   public interface IUserEventsRepository
   {
       IEnumerable<UserToEvent> GetEvents(int userId);

       bool AddUserToEvent(int userId, int eventId,string role);

       bool RemoveUserFromEvent(int userId, int eventId);

        //todo
       IEnumerable<Event> GetEvents(QueryEventParameters queryEventParameters);

       bool InviteFriendToEvent(int userId,int friendId, int eventId);

       bool CreateRequestToEvent(int userId, int eventId);
    }
}
