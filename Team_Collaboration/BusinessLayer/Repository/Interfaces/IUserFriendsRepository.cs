﻿using System.Collections.Generic;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Interfaces
{
   public interface IUserFriendsRepository
   {
       IEnumerable<User> GetFriendsOfUser(int userId);

       bool AddFriend(int userFriendId,int userId);

       bool RemoveFriend(int userFriendId, int userId);
   }
}
