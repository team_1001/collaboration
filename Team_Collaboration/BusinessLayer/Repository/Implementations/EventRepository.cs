﻿using System;
using System.Collections.Generic;
using BusinessLayer.Repository.Interfaces;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Implementations
{
   public class EventRepository: IEventRepository
    {
        private readonly IQueryBuilderFactory _queryFactory;

        public EventRepository(IQueryBuilderFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }

       public Event GetEvent(int eventId)
       {
           return _queryFactory.Set<Event>().Where(e=>e.Id == eventId).ExecOne();
       }

       public IEnumerable<Request> GetAllRequests(int eventId)
       {
           return _queryFactory.Set<Event>().Where(e=>e.Id == eventId).SelectMany(e => e.Requests).Exec();
       }

       public IEnumerable<Invite> GetAllInvites(int eventId)
       {
            return _queryFactory.Set<Event>().Where(e => e.Id == eventId).SelectMany(e => e.Invites).Exec();
        }

       public IEnumerable<UserToEvent> GetAllUsers(int eventId)
       {
            return _queryFactory.Set<Event>().Where(e => e.Id == eventId).SelectMany(e => e.AllUsers).Exec();
        }

       public bool Create(int userId, Event @event)
       {
            return _queryFactory.Set<Event>().Add(@event).SaveChanges();
       }

       public bool Edit(int userId, Event @event)
       {
            var existingEvent = _queryFactory.Set<Event>().Where(e => e.Id == @event.Id).ExecOne();
            if (existingEvent == null) throw new Exception("event not found");
            return _queryFactory.Set<Event>().Attach(@event).SaveChanges();
       }

       public bool Remove(int userId, int eventId)
       {
           var @event = _queryFactory.Set<Event>().Where(e => e.Id == eventId).ExecOne();
           if(@event == null)  throw new Exception("event not found");

          return _queryFactory.Set<Event>().Remove(@event).SaveChanges();
       }
    }
}
