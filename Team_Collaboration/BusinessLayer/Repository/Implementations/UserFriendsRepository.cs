﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Repository.Interfaces;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Implementations
{
   public class UserFriendsRepository:IUserFriendsRepository
    {
        private readonly IQueryBuilderFactory _queryFactory;
        public UserFriendsRepository(IQueryBuilderFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }

       public IEnumerable<User> GetFriendsOfUser(int userId)
       {
            return _queryFactory.Set<User>().Where(u=>u.Id == userId).SelectMany(u => u.Friends).Exec();
        }

       public bool AddFriend(int userFriendId, int userId)
       {
           var user = _queryFactory.Set<User>().Where(u => u.Id == userId).ExecOne();
           var friend = _queryFactory.Set<User>().Where(u => u.Id == userFriendId).ExecOne();

           if (user == null) throw new Exception("user is null");
           if (friend == null) throw new Exception("friend is null");

           if (user.Friends.Contains(friend)) throw new Exception("user already has this friend");

           user.Friends.Add(friend);
           return _queryFactory.Set<User>().Attach(user).SaveChanges();
       }

       public bool RemoveFriend(int userFriendId, int userId)
       {
            var user = _queryFactory.Set<User>().Where(u => u.Id == userId).Include(u=>u.Friends).ExecOne();
            if (user == null) throw new Exception("user is null");

            var friend = user.Friends.FirstOrDefault(f => f.Id == userFriendId);
            if (friend == null) throw new Exception("friend is null");

            user.Friends.Remove(friend);
           return _queryFactory.Set<User>().Attach(user).SaveChanges();
       }
    }
}
