﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Models;
using BusinessLayer.Repository.Interfaces;
using BusinessLayer.Repository.Interfaces.UserEvents;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;
using User = DataLayer.Entity.User;

namespace BusinessLayer.Repository.Implementations
{
    public class UserEventsRepository: IUserEventsRepository
    {
        private readonly IQueryBuilderFactory _queryFactory;
        private readonly IEnumerable<IEventRetriever> _handlers; 

        public UserEventsRepository(IQueryBuilderFactory queryFactory,IEnumerable<IEventRetriever> handlers)
        {
            _queryFactory = queryFactory;
            _handlers = handlers;
        }

        public bool AddUserToEvent(int userId, int eventId, string role)
        {
            var user = _queryFactory.Set<User>().Where(u => u.Id == userId).ExecOne();
            var @event = _queryFactory.Set<Event>().Where(e => e.Id == eventId).ExecOne();

            if (user == null) throw new Exception("user is null");
            if (@event == null) throw new Exception("event is null");

            var userToEvent = new UserToEvent() {EventId = eventId, UserId = userId, Role = role};

            return _queryFactory.Set<UserToEvent>().Add(userToEvent).SaveChanges();
        }

        public bool RemoveUserFromEvent(int userId, int eventId)
        {
            var user = _queryFactory.Set<User>().Where(u => u.Id == userId).Include(u=>u.Events).ExecOne();
            if (user == null) throw new Exception("user is null");

            var @event = user.Events.FirstOrDefault(e => e.EventId == eventId);
            if (@event == null) throw new Exception("event is null");

            //todo change this role later
            if (@event.Role == "host") throw new Exception("this user is owner of event!");

            user.Events.Remove(@event);
            return _queryFactory.Set<User>().Attach(user).SaveChanges();
        }


        public IEnumerable<UserToEvent> GetEvents(int userId)
        {
            return _queryFactory.Set<User>().Where(u => u.Id == userId).SelectMany(u => u.Events).Exec();
        }

        public IEnumerable<Event> GetEvents(QueryEventParameters queryEventParameters)
        {
           var handler = _handlers.FirstOrDefault(h => h.CanHandle(queryEventParameters));

            if(handler == null) throw  new Exception("not implemented handler");

            return handler.GetEvents(queryEventParameters);
        }

        public bool InviteFriendToEvent(int userId, int friendId, int eventId)
        {
            var user = _queryFactory.Set<User>().Where(u => u.Id == userId).ExecOne();
            var friend = _queryFactory.Set<User>().Where(u => u.Id == friendId).ExecOne();

            if (user == null) throw new Exception("user is null");
            if (friend == null) throw new Exception("friend is null");

            var invite = new Invite() {EventId = eventId,HostUserId = userId,InvitedUserId = friendId};
            return _queryFactory.Set<Invite>().Add(invite).SaveChanges();
        }

        public bool CreateRequestToEvent(int userId, int eventId)
        {
            var user = _queryFactory.Set<User>().Where(u => u.Id == userId).ExecOne();
            if (user == null) throw new Exception("user not found");

            var @event = _queryFactory.Set<Event>().Where(e => e.Id == eventId).ExecOne();
            if (@event == null) throw new Exception("event not found");

            var request = new Request() { EventId = @event.Id,  UserId = userId ,State = "Pending"};
            return _queryFactory.Set<Request>().Add(request).SaveChanges();
        }
    }
}
