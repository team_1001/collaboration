﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Models;
using BusinessLayer.Repository.Interfaces.UserEvents;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Implementations.UserEvents
{
    public class SuggestedEventsRetriever: IEventRetriever
    {
        private readonly IQueryBuilderFactory _queryFactory;
        public SuggestedEventsRetriever(IQueryBuilderFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }


        public bool CanHandle(QueryEventParameters parameters)
        {
            return parameters.FilterType == EventFilterType.Suggested;
        }

        public IEnumerable<Event> GetEvents(QueryEventParameters parameters)
        {
            var user = _queryFactory.Set<User>()
                .Where(u => u.Id == parameters.UserId)
                .Include(u => u.Adress)
                .Include(u => u.Events.Select(e => e.Event.Category))
                .Include(u => u.Events.Select(e => e.Event.Location))
                .ExecOne();

            if (user == null) throw new Exception("user not found");

            var filterParameters = new
            {
                Tags = user.Events.SelectMany(e=>e.Event.Tags),
                Categories =user.Events.Select(e=>e.Event.CategoryId)
            };

           return _queryFactory.Set<Event>()
                .Where(
                    @event =>
                        filterParameters.Tags.Any(tag => @event.Tags.Contains(tag)) ||
                        filterParameters.Categories.Contains(@event.CategoryId))
                        .Skip((parameters.Page - 1) * parameters.PageSize)
                        .Take(parameters.PageSize)
                        .Exec();
        }
    }
}
