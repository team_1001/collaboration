﻿using System.Collections.Generic;
using BusinessLayer.Models;
using BusinessLayer.Repository.Interfaces.UserEvents;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Implementations.UserEvents
{
   public class AllEventsRetriever : IEventRetriever
    {
        private readonly IQueryBuilderFactory _queryFactory;
        public AllEventsRetriever(IQueryBuilderFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }

        public bool CanHandle(QueryEventParameters parameters)
       {
           return parameters.FilterType == EventFilterType.All;
       }

       public IEnumerable<Event> GetEvents(QueryEventParameters parameters)
       {
           return _queryFactory.Set<Event>().Exec();
       }
    }
}
