﻿using System.Collections.Generic;
using BusinessLayer.Models;
using BusinessLayer.Repository.Interfaces.UserEvents;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;

namespace BusinessLayer.Repository.Implementations.UserEvents
{
    public class PopularEventsRetriever : IEventRetriever
    {
        private readonly IQueryBuilderFactory _queryFactory;

        public PopularEventsRetriever(IQueryBuilderFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }

        public bool CanHandle(QueryEventParameters parameters)
        {
            return parameters.FilterType == EventFilterType.Popular;
        }

        public IEnumerable<Event> GetEvents(QueryEventParameters parameters)
        {
            return _queryFactory.Set<Event>()
                .OrderByDesc(e=>e.AllUsers.Count)
                .Skip((parameters.Page - 1)*parameters.PageSize)
                .Take(parameters.PageSize)
                .Exec();
        }
    }
}
