﻿using Autofac;
using BusinessLayer.Messaging;
using Common;
using Infrastructure;

namespace Team_Collaboration.HubServer.Service
{
    class HubHostingService : WebHostingService<Startup.Startup>
    {
        private MessageProcessorManager _messageProcessorManager;

        public HubHostingService(
            string serviceName, 
            string baseAddress, 
            string logTag = "") 
            : base(serviceName, baseAddress, logTag)
        {
        }

        protected override void OnStartInternal()
        {
            base.OnStartInternal();
            _messageProcessorManager = AutoFacCore.Core.BeginLifetimeScope().Resolve<MessageProcessorManager>();
            _messageProcessorManager.Start();
        }

        protected override void OnStop()
        {
            _messageProcessorManager.Stop();
            base.OnStop();
        }
    }
}
