﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using Autofac;
using Autofac.Extras.NLog;
using BusinessLayer.Messaging;
using Common;
using Common.Extensions;
using Infrastructure;
using Team_Collaboration.Hub.AutoFac;
using Team_Collaboration.HubServer.Service;

namespace Team_Collaboration.HubServer
{
    class Program
    {
        [STAThread]

        private static void Main()
        {
            bool debug = IsDebugMode();
            if (debug)
            {
                Debugger.Launch();
            }
            
            using (HubHostingService service = GetService())
            {
                if (IsConsoleMode())
                {
                    service.Open();

                    ConsoleKeyInfo info = Console.ReadKey(true);
                    while (info.Key != ConsoleKey.Enter)
                    {
                        info = Console.ReadKey(true);
                    }

                    service.Close();
                }
                else
                {
                    ServiceBase.Run(service);
                }
            }
        }

        private static bool IsDebugMode()
        {
            var args = Environment.GetCommandLineArgs();
            return (args.Length > 0 && args.Any(x => x.ToLower() == "debug"));
        }

        private static bool IsConsoleMode()
        {
            var args = Environment.GetCommandLineArgs();
            if (args.Length > 0 && args.Any(x => x.ToLower() == "service"))
                return false;

            return ConfigurationManager.AppSettings["IsConsole"].ToBoolean();
        }

        private static HubHostingService GetService()
        {
            var serviceName = GetServiceNameFromConfiguration();
            string address = ConfigurationManager.AppSettings["Address"];

            return new HubHostingService(serviceName, address, "Server");
        }

        private static string GetServiceNameFromConfiguration()
        {
            // READ THIS: Get the service name from App.Config
            var serviceName = ConfigurationManager.AppSettings["ServiceName"];

            if (string.IsNullOrEmpty(serviceName))
                serviceName = "NotSpecified";

            return serviceName;
        }
    }
}
