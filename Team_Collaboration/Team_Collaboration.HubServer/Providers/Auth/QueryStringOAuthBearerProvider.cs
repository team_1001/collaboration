﻿using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;

namespace Team_Collaboration.HubServer.Providers.Auth
{
    public class QueryStringOAuthBearerProvider : OAuthBearerAuthenticationProvider
    {
        public override Task RequestToken(OAuthRequestTokenContext context)
        {
            var value = context.Request.Headers.Get("access_token") ?? context.Request.Cookies["access_token"];

            if (string.IsNullOrEmpty(value))
                return base.RequestToken(context);

            context.Token = value;
            return Task.FromResult<object>(null);
        }
    }
}
