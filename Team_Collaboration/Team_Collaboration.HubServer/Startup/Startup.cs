﻿using Autofac;
using Autofac.Extras.NLog;
using Autofac.Integration.SignalR;
using BusinessLayer.Config;
using Common;
using Common.Configuration;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Owin.Security.AesDataProtectorProvider;
using Team_Collaboration.Hub.Hubs;
using Team_Collaboration.HubServer.Logging;
using Team_Collaboration.HubServer.Providers.Auth;
using MainModule = Team_Collaboration.Hub.AutoFac.MainModule;

[assembly: OwinStartup(typeof(Team_Collaboration.HubServer.Startup.Startup))]
namespace Team_Collaboration.HubServer.Startup
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AutoFacCore.Init(
                new MainModule(),
                new BusinessLayer.AutoFac.MainModule(),
                new AutoFac.MainModule(),
                new NLogModule());

            HubConfiguration hubConfiguration = new HubConfiguration();
            
            AutoFacCore.Extend(c => c.Register(x => hubConfiguration.Resolver.Resolve<IConnectionManager>().GetHubContext<ChatHub>()).ExternallyOwned());

            var scope = AutoFacCore.Core.BeginLifetimeScope();

            // todo: improve
            var configurator = scope.Resolve<IConfigurator>();
            var config = scope.Resolve<IConfigurationProvider>();

            configurator.Configure(config.ConnectionStrings["DefaultConnection"]);

            var resolver = new AutofacDependencyResolver(scope);

            app.UseAutofacMiddleware(scope);

            app.Map("/signalr", map =>
            {
                // Setup the CORS middleware to run before SignalR.
                // By default this will allow all origins. You can 
                // configure the set of origins and/or http verbs by
                // providing a cors options with a different policy.
                map.UseCors(CorsOptions.AllowAll);

                map.UseAesDataProtectorProvider();

                map.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
                {
                    Provider = new QueryStringOAuthBearerProvider()
                });

                hubConfiguration.EnableDetailedErrors = true;
                hubConfiguration.EnableJavaScriptProxies = true;
                hubConfiguration.Resolver = resolver;
                //var hubConfiguration = new HubConfiguration
                //{
                //    EnableDetailedErrors = true,
                //    EnableJavaScriptProxies = true,
                //    Resolver = resolver
                //};

                var pipeline = resolver.Resolve<IHubPipeline>();
                pipeline.AddModule(resolver.Resolve<LoggingModule>());

                // Run the SignalR pipeline. We're not using MapSignalR
                // since this branch already runs under the "/signalr"
                // path.
                map.RunSignalR(hubConfiguration);
            });
        }
    }
}
