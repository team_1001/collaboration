﻿using Autofac;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Team_Collaboration.Hub.Services;
using Team_Collaboration.Hub.Utils;
using Team_Collaboration.HubServer.Logging;
using Team_Collaboration.HubServer.Providers;

namespace Team_Collaboration.HubServer.AutoFac
{
    class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(
                c =>
                    JsonSerializer.Create(new JsonSerializerSettings()
                    {
                        ContractResolver = new SignalRContractResolver()
                    })).As<JsonSerializer>();

            
            builder.RegisterType<LoggingModule>().AsSelf();
        }
    }
}
