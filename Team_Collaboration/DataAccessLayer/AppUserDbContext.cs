﻿using System;
using System.Data.Entity;
using AuthLayer;
using DataAccessLayer.Configuration;
using DataLayer.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAccessLayer
{
    [DbConfigurationType(typeof(AppUserContextConfiguration))]
    public class AppUserDbContext
        : IdentityDbContext<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>
    {
        public AppUserDbContext() : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public AppUserDbContext(string connectionString) : base(connectionString)
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        //public virtual IDbSet<User> AppUsers { get; set; } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            modelBuilder.Entity<User>()
                .Ignore(x => x.IncomingInvites)
                .Ignore(x => x.OutgoingInvites)
                .Ignore(x => x.Events)
                .Ignore(x => x.Requests)
                .Ignore(x => x.Address)
                .Ignore(x => x.Dialogs)
                .Ignore(x => x.UnreadMessages)
                .Ignore(x => x.Notifications)
                .Ignore(x => x.MissedNotifications)
                .Ignore(x => x.Friends);

            modelBuilder.Entity<AppUser>()
                .HasKey(x => x.Id)
                .HasRequired(x => x.User)
                .WithRequiredPrincipal();

            modelBuilder.Entity<AppClient>().HasKey(x => x.Id).ToTable("Clients");
            modelBuilder.Entity<RefreshToken>()
                .HasKey(x => x.Id)
                .ToTable("RefreshTokens")
                .HasRequired(x => x.Client)
                .WithMany()
                .HasForeignKey(x => x.ClientId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
