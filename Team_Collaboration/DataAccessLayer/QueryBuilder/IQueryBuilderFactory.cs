namespace DataAccessLayer.QueryBuilder
{
    public interface IQueryBuilderFactory
    {
        IQueryBuilder<T> Set<T>() where T : class;
    }
}