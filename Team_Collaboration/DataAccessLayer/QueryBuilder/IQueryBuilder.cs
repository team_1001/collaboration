using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataLayer.Entity;

namespace DataAccessLayer.QueryBuilder
{
    public interface IQueryBuilder<TEntity> where TEntity : class 
    {
        Expression Expression { get; }

        IQueryBuilder<TResult> Join<TTo, TKey, TResult>(IQueryBuilder<TTo> to, Expression<Func<TEntity, TKey>> left,
            Expression<Func<TTo, TKey>> right,
            Expression<Func<TEntity, TTo, TResult>> result)
            where TTo : class where TResult : class;

        IQueryBuilder<TResult> Join<TTo, TKey, TResult>(Expression<Func<TEntity, TKey>> left,
            Expression<Func<TTo, TKey>> right,
            Expression<Func<TEntity, TTo, TResult>> result) where TTo : class where TResult : class;

        IQueryBuilder<IGrouping<TKey, TEntity>> GroupBy<TKey>(
            Expression<Func<TEntity, TKey>> keySelector);

        IQueryBuilder<TResult> GroupJoin<TTo, TKey, TResult>(IQueryBuilder<TTo> to, Expression<Func<TEntity, TKey>> left,
            Expression<Func<TTo, TKey>> right,
            Expression<Func<TEntity, IEnumerable<TTo>, TResult>> result)
            where TTo : class where TResult : class;

        IQueryBuilder<TResult> GroupJoin<TTo, TKey, TResult>(Expression<Func<TEntity, TKey>> left,
            Expression<Func<TTo, TKey>> right,
            Expression<Func<TEntity, IEnumerable<TTo>, TResult>> result) where TTo : class where TResult : class;

        IQueryBuilder<TEntity> Where(Expression<Func<TEntity, bool>> predicate);

        IQueryBuilder<TResult> Select<TResult>(Expression<Func<TEntity, TResult>> selector) where TResult : class;

        IQueryBuilder<TResult> SelectMany<TResult>(Expression<Func<TEntity, IEnumerable<TResult>>> selector) where TResult : class;

        IQueryBuilder<TEntity> OrderBy<TResult>(Expression<Func<TEntity, TResult>> selector);

        IQueryBuilder<TEntity> OrderByDesc<TResult>(Expression<Func<TEntity, TResult>> selector);

        IQueryBuilder<TEntity> Skip(int count);

        IQueryBuilder<TEntity> Take(int count);

        IEnumerable<TEntity> Exec();

        Task<IEnumerable<TEntity>> ExecAsync();

        TEntity ExecOne();

        Task<TEntity> ExecOneAsync();

        IQueryBuilder<TEntity> Include<TResult>(Expression<Func<TEntity, TResult>> predicate);

        bool SaveChanges();

        Task<bool> SaveChangesAsync();

        IQueryBuilder<TEntity> Add<T>(T item) where T : TEntity, IEntity;

        IQueryBuilder<TEntity> Attach<T>(T item) where T : TEntity, IEntity;

        IQueryBuilder<TEntity> AddMany<T>(IEnumerable<T> items) where T : class, TEntity, IEntity;

        IQueryBuilder<TEntity> Remove<T>(T item) where T : TEntity, IEntity;

        IQueryBuilder<TEntity> RemoveMany<T>(IEnumerable<T> items) where T : class, TEntity, IEntity;
    }
}