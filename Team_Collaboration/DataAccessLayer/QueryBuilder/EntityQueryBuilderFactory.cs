using System.Data.Entity;

namespace DataAccessLayer.QueryBuilder
{
    public class EntityQueryBuilderFactory : IQueryBuilderFactory
    {
        private readonly DbContext _context;

        public EntityQueryBuilderFactory(DbContext context)
        {
            _context = context;
        }

        public IQueryBuilder<T> Set<T>() where T : class 
        {
            return new EntityQueryBuilder<T>(_context);
        }
    }
}