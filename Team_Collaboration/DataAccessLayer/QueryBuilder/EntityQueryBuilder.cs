﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataLayer.Entity;

namespace DataAccessLayer.QueryBuilder
{
    public class EntityQueryBuilder<TEntity> : IQueryBuilder<TEntity> where TEntity : class
    {
        private readonly DbContext _context;

        public EntityQueryBuilder(DbContext context)
        {
            _context = context;
            Expression = Expression.Constant(_context.Set<TEntity>());
        }

        public EntityQueryBuilder(Expression expression, DbContext context) : this(context)
        {
            Expression = expression;
            _context = context;
        }

        public Expression Expression { get; private set; }

        public IQueryBuilder<TResult> Join<TTo, TKey, TResult>(
            IQueryBuilder<TTo> to,
            Expression<Func<TEntity, TKey>> left,
            Expression<Func<TTo, TKey>> right,
            Expression<Func<TEntity, TTo, TResult>> result
            ) where TTo : class where TResult : class
        {
            Expression = Expression.Call(typeof(Queryable), "Join",
                    new Type[] { typeof(TEntity), typeof(TTo), typeof(TKey), typeof(TResult) },
                    Expression, to.Expression, left,
                    right, result);

            return new EntityQueryBuilder<TResult>(Expression, _context);
        }

        public IQueryBuilder<TResult> Join<TTo, TKey, TResult>(
            Expression<Func<TEntity, TKey>> left,
            Expression<Func<TTo, TKey>> right,
            Expression<Func<TEntity, TTo, TResult>> result) where TTo : class where TResult : class
        {
            Expression = Expression.Call(typeof(Queryable), "Join",
                    new Type[] { typeof(TEntity), typeof(TTo), typeof(TKey), typeof(TResult) },
                    Expression, Expression.Constant(_context.Set<TTo>()), left,
                    right, result);

            return new EntityQueryBuilder<TResult>(Expression, _context);
        }

        public IQueryBuilder<IGrouping<TKey, TEntity>> GroupBy<TKey>(
            Expression<Func<TEntity, TKey>> keySelector)
        {
            Expression = Expression.Call(typeof(Queryable), "GroupBy",
                    new Type[] { typeof(TEntity), typeof(TKey) },
                    Expression, keySelector);

            return new EntityQueryBuilder<IGrouping<TKey, TEntity>>(Expression, _context);
        }

        public IQueryBuilder<TResult> GroupJoin<TTo, TKey, TResult>(
            IQueryBuilder<TTo> to,
            Expression<Func<TEntity, TKey>> left,
            Expression<Func<TTo, TKey>> right,
            Expression<Func<TEntity, IEnumerable<TTo>, TResult>> result) where TTo : class where TResult : class
        {
            Expression = Expression.Call(typeof (Queryable), "GroupJoin",
                new Type[] {typeof (TEntity), typeof (TTo), typeof (TKey), typeof (TResult)},
                Expression, to.Expression, left,
                right, result);

            return new EntityQueryBuilder<TResult>(Expression, _context);
        }

        public IQueryBuilder<TResult> GroupJoin<TTo, TKey, TResult>(
            Expression<Func<TEntity, TKey>> left, 
            Expression<Func<TTo, TKey>> right, 
            Expression<Func<TEntity, IEnumerable<TTo>, TResult>> result) where TTo : class where TResult : class
        {
            Expression = Expression.Call(typeof(Queryable), "GroupJoin",
                    new Type[] { typeof(TEntity), typeof(TTo), typeof(TKey), typeof(TResult) },
                    Expression, Expression.Constant(_context.Set<TTo>()), left,
                    right, result);

            return new EntityQueryBuilder<TResult>(Expression, _context);
        }

        public IQueryBuilder<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            Expression = Expression.Call(typeof(Queryable), "Where",
                       new Type[] { typeof(TEntity) },
                       Expression, predicate);

            return this;
        }

        public IQueryBuilder<TResult> Select<TResult>(Expression<Func<TEntity, TResult>> selector) where TResult : class
        {
            Expression = Expression.Call(typeof(Queryable), "Select",
                       new Type[] { typeof(TEntity), typeof(TResult) },
                       Expression, selector);

            return new EntityQueryBuilder<TResult>(Expression, _context);
        }

        public IQueryBuilder<TResult> SelectMany<TResult>(Expression<Func<TEntity, IEnumerable<TResult>>> selector) where TResult : class
        {
            Expression = Expression.Call(typeof(Queryable), "SelectMany",
                      new Type[] { typeof(TEntity), typeof(TResult) },
                      Expression, selector);

            return new EntityQueryBuilder<TResult>(Expression, _context);
        }

        public IQueryBuilder<TEntity> OrderBy<TResult>(Expression<Func<TEntity, TResult>> selector)
        {
            Expression = Expression.Call(typeof(Queryable), "OrderBy",
                       new Type[] { typeof(TEntity), typeof(TResult) },
                       Expression, selector);

            return this;
        }

        public IQueryBuilder<TEntity> OrderByDesc<TResult>(Expression<Func<TEntity, TResult>> selector)
        {
            Expression = Expression.Call(typeof(Queryable), "OrderByDescending",
                       new Type[] { typeof(TEntity), typeof(TResult) },
                       Expression, selector);

            return this;
        }

        public IQueryBuilder<TEntity> Skip(int count)
        {
            Expression = Expression.Call(typeof(Queryable), "Skip",
                       new Type[] { typeof(TEntity), typeof(int) },
                       Expression, Expression.Constant(count));

            return this;
        }

        public IQueryBuilder<TEntity> Take(int count)
        {
            Expression = Expression.Call(typeof(Queryable), "Take",
                       new Type[] { typeof(TEntity), typeof(int) },
                       Expression, Expression.Constant(count));

            return this;
        }

        public IEnumerable<TEntity> Exec()
        {
            Expression<Func<IEnumerable<TEntity>>> result =
                Expression.Lambda<Func<IEnumerable<TEntity>>>(
                    Expression.Call(typeof(Enumerable), "ToList",
                        new Type[] { typeof(TEntity) },
                        Expression));

            return result.Compile()();
        }

        public async Task<IEnumerable<TEntity>> ExecAsync()
        {
            Expression<Func<Task<IEnumerable<TEntity>>>>result =
                Expression.Lambda<Func<Task<IEnumerable<TEntity>>>>(
                    Expression.Call(typeof(Queryable), "ToListAsync",
                        new Type[] { typeof(TEntity) },
                        Expression));

            return await result.Compile()();
        }

        public TEntity ExecOne()
        {
            Expression<Func<TEntity>> result =
                Expression.Lambda<Func<TEntity>>(
                    Expression.Call(typeof(Enumerable), "FirstOrDefault",
                        new Type[] { typeof(TEntity) },
                        Expression));

            return result.Compile()();
        }

        public async Task<TEntity> ExecOneAsync()
        {
            Expression<Func<Task<TEntity>>> result =
                Expression.Lambda<Func<Task<TEntity>>>(
                    Expression.Call(typeof(Queryable), "FirstOrDefaultAsync",
                        new Type[] { typeof(TEntity) },
                        Expression));

            return await result.Compile()();
        }

        public IQueryBuilder<TEntity> Include<TResult>(Expression<Func<TEntity, TResult>> predicate)
        {
            Expression = Expression.Call(typeof(Queryable), "Include",
                       new Type[] { typeof(TEntity) },
                       Expression, predicate);

            return this;
        }

        public bool SaveChanges()
        {
            _context.ChangeTracker.DetectChanges();
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            _context.ChangeTracker.DetectChanges();
            return await _context.SaveChangesAsync() > 0;
        }

        public IQueryBuilder<TEntity> Add<T>(T item) where T : TEntity, IEntity
        {
            _context.Set<TEntity>().Add(item);
            return this;
        }

        public IQueryBuilder<TEntity> Attach<T>(T item) where T : TEntity, IEntity
        {
            _context.Set<TEntity>().Attach(item);
            return this;
        }

        public IQueryBuilder<TEntity> AddMany<T>(IEnumerable<T> items) where T : class, TEntity, IEntity
        {
            _context.Set<T>().AddRange(items);
            return this;
        }

        public IQueryBuilder<TEntity> Remove<T>(T item) where T : TEntity, IEntity
        {
            _context.Set<TEntity>().Remove(item);
            return this;
        }

        public IQueryBuilder<TEntity> RemoveMany<T>(IEnumerable<T> items) where T : class, TEntity, IEntity
        {
            _context.Set<T>().RemoveRange(items);
            return this;
        }
    }
}
