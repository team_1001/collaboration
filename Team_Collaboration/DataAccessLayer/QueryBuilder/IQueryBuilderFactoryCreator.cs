﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.QueryBuilder
{
    public interface IQueryBuilderFactoryCreator
    {
        IQueryBuilderFactory Create(DbContext context);
    }

    class EntityQueryBuilderFactoryCreator : IQueryBuilderFactoryCreator
    {
        public IQueryBuilderFactory Create(DbContext context)
        {
            return new EntityQueryBuilderFactory(context);
        }
    }
}
