﻿using System.Data.Entity.Infrastructure;
using Common.Configuration;

namespace DataAccessLayer
{
    public class ActivityContextFactory : IDbContextFactory<ActivityContext>
    {
        private readonly IConfigurationProvider _configuration;

        public ActivityContextFactory(IConfigurationProvider configuration)
        {
            _configuration = configuration;
        }

        public ActivityContext Create()
        {
            return new ActivityContext(_configuration.ConnectionStrings["DefaultConnection"]);
        }

        public ActivityContext Create(string connectionString)
        {
            return new ActivityContext(connectionString);
        }

        public ActivityContext CreateByName(string connectionStringName)
        {
            return new ActivityContext(_configuration.ConnectionStrings[connectionStringName]);
        }
    }
}
