﻿using System.Data.Entity.Infrastructure;
using Common.Configuration;

namespace DataAccessLayer
{
    public class AppUserDbContextFactory : IDbContextFactory<AppUserDbContext>
    {
        private readonly IConfigurationProvider _configuration;

        public AppUserDbContextFactory(IConfigurationProvider configuration)
        {
            _configuration = configuration;
        }

        public AppUserDbContext Create()
        {
            return new AppUserDbContext(_configuration.ConnectionStrings["DefaultConnection"]);
        }

        public AppUserDbContext Create(string connectionString)
        {
            return new AppUserDbContext(connectionString);
        }

        public AppUserDbContext CreateByName(string connectionStringName)
        {
            return  new AppUserDbContext(_configuration.ConnectionStrings[connectionStringName]);
        }
    }
}