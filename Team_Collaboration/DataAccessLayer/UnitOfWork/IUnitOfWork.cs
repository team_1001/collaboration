﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IUnitOfWork Create();

        Task<TResult> Execute<TContext, TResult>(Func<TContext, Task<TResult>> execute) where TContext : class;

        Task Execute<TContext>(Func<TContext, Task> execute) where TContext : class;

        IQueryable<TItem> Set<TItem>() where TItem : class;

        Task<TItem> FindAsync<TItem>(params object[] keys) where TItem : class;

        IUnitOfWork Add<TItem>(TItem item) where TItem : class;

        IUnitOfWork AddRange<TItem>(IEnumerable<TItem> items) where TItem : class;

        IUnitOfWork Remove<TItem>(TItem item) where TItem : class;

        IUnitOfWork RemoveRange<TItem>(IEnumerable<TItem> items) where TItem : class;

        IUnitOfWork Attach<TItem>(TItem item) where TItem : class;

        IUnitOfWork AttachRange<TItem>(IEnumerable<TItem> items) where TItem : class;

        IUnitOfWork Detach<TItem>(TItem item) where TItem : class;

        IUnitOfWork DetachRange<TItem>(IEnumerable<TItem> items) where TItem : class;

        IUnitOfWork Update<TItem>(TItem item) where TItem : class;

        IUnitOfWork UpdateRange<TItem>(IEnumerable<TItem> items) where TItem : class;

        Task<bool> CommitAsync();
    }

    class EntityUnitOfWork<TContext> : IUnitOfWork where TContext : DbContext
    {
        private readonly IDbContextFactory<TContext> _contextFactory;
        private TContext _context;
        private bool _disposed;

        public EntityUnitOfWork(IDbContextFactory<TContext> contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public IUnitOfWork Create()
        {
            _context = _contextFactory.Create();
            return this;
        }

        public async Task Execute<TCurrentContext>(Func<TCurrentContext, Task> execute) where TCurrentContext : class
        {
            TCurrentContext currentContext = _context as TCurrentContext;
            if (currentContext == null)
                throw new Exception("Wrong context type");

            await execute(currentContext);
        }

        public IQueryable<TItem> Set<TItem>() where TItem : class
        {
            return _context.Set<TItem>();
        }

        public async Task<TItem> FindAsync<TItem>(params object[] keys) where TItem : class
        {
            return await _context.Set<TItem>().FindAsync(keys);
        }

        public IUnitOfWork Add<TItem>(TItem item) where TItem : class
        {
            _context.Set<TItem>().Add(item);
            return this;
        }

        public IUnitOfWork Remove<TItem>(TItem item) where TItem : class
        {
            _context.Set<TItem>().Remove(item);
            return this;
        }

        public IUnitOfWork RemoveRange<TItem>(IEnumerable<TItem> items) where TItem : class
        {
            _context.Set<TItem>().RemoveRange(items);
            return this;
        }

        public IUnitOfWork Attach<TItem>(TItem item) where TItem : class
        {
            _context.Set<TItem>().Attach(item);
            return this;
        }

        public IUnitOfWork AttachRange<TItem>(IEnumerable<TItem> items) where TItem : class
        {
            var set = _context.Set<TItem>();
            foreach (var item in items)
            {
                set.Attach(item);
            }

            return this;
        }

        public IUnitOfWork Detach<TItem>(TItem item) where TItem : class
        {
            _context.Entry(item).State = EntityState.Detached;
            return this;
        }

        public IUnitOfWork DetachRange<TItem>(IEnumerable<TItem> items) where TItem : class
        {
            foreach (var item in items)
            {
                _context.Entry(item).State = EntityState.Detached;
            }

            return this;
        }

        public IUnitOfWork Update<TItem>(TItem item) where TItem : class
        {
            Attach(item);
            _context.Entry(item).State = EntityState.Modified;
            return this;
        }

        public IUnitOfWork UpdateRange<TItem>(IEnumerable<TItem> items) where TItem : class
        {
            foreach (var item in items)
            {
                Attach(item);
                _context.Entry(item).State = EntityState.Modified;
            }
            
            return this;
        }

        public IUnitOfWork AddRange<TItem>(IEnumerable<TItem> items) where TItem : class
        {
            _context.Set<TItem>().AddRange(items);
            return this;
        }

        public async Task<TResult> Execute<TCurrentContext, TResult>(Func<TCurrentContext, Task<TResult>> execute) where TCurrentContext : class
        {
            TCurrentContext currentContext = _context as TCurrentContext;
            if (currentContext == null)
                throw new Exception("Wrong context type");

            return await execute(currentContext);
        }

        public async Task<bool> CommitAsync()
        {
            _context.ChangeTracker.DetectChanges();
            return await _context.SaveChangesAsync() > 0;
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;
            if (_context != null)
                _context.Dispose();
        }
    }
}
