namespace DataAccessLayer.AuthMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "AddressId", c => c.Int());
            DropColumn("dbo.Users", "AdressId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "AdressId", c => c.Int());
            DropColumn("dbo.Users", "AddressId");
        }
    }
}
