using System.Collections.Generic;
using AuthLayer;
using Common.Utils;

namespace DataAccessLayer.AuthMigrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<DataAccessLayer.AppUserDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"AuthMigrations";
        }

        protected override void Seed(AppUserDbContext context)
        {
            context.Set<AppClient>().AddOrUpdate(ClientsList().ToArray());
            base.Seed(context);
        }

        private List<AppClient> ClientsList()
        {
            return new List<AppClient>
            {
                new AppClient()
                {
                    Id = "WebApp",
                    Secret = Hasher.GetHash("WebApp"),
                    Name = "Web Application",
                    ApplicationType = ApplicationTypes.JavaScript,
                    Active = true,
                    RefreshTokenLifeTime = 14400,
                    AllowedOrigin = "*"
                },

                new AppClient
                {
                    Id = "MobileApp",
                    Secret = Hasher.GetHash("MobileApp"),
                    Name = "Mobile Application",
                    ApplicationType = ApplicationTypes.NativeConfidential,
                    Active = true,
                    RefreshTokenLifeTime = 14400,
                    AllowedOrigin = "*"
                }
            };
        }
    }
}
