namespace DataAccessLayer.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        MaxPeopleCount = c.Int(nullable: false),
                        DateStarts = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DateEnds = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LocationId = c.Int(nullable: false),
                        Visibility = c.String(),
                        CategoryId = c.Int(nullable: false),
                        DialogId = c.String(nullable: false, maxLength: 128),
                        EventType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EventCategories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Dialogs", t => t.DialogId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId)
                .Index(t => t.CategoryId)
                .Index(t => t.DialogId);
            
            CreateTable(
                "dbo.UserToEvents",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        EventId = c.Int(nullable: false),
                        Role = c.String(),
                    })
                .PrimaryKey(t => new { t.UserId, t.EventId })
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.EventId);
            
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                        Name = c.String(),
                        Latitude = c.Double(),
                        Longitude = c.Double(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dialogs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Content = c.String(),
                        DialogId = c.String(nullable: false, maxLength: 128),
                        Type = c.Int(nullable: false),
                        TimeStamp = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        SenderId = c.Int(nullable: false),
                        Viewed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.SenderId)
                .ForeignKey("dbo.Dialogs", t => t.DialogId, cascadeDelete: true)
                .Index(t => t.DialogId)
                .Index(t => t.SenderId);
            
            CreateTable(
                "dbo.UnreadMessages",
                c => new
                    {
                        MessageId = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MessageId, t.UserId })
                .ForeignKey("dbo.Messages", t => t.MessageId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.MessageId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Invites",
                c => new
                    {
                        EventId = c.Int(nullable: false),
                        InvitedUserId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EventId, t.InvitedUserId })
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.InvitedUserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.EventId)
                .Index(t => t.InvitedUserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        EventId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EventId, t.UserId })
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.EventId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.EventCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DialogsUsers",
                c => new
                    {
                        DialogId = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DialogId, t.UserId })
                .ForeignKey("dbo.Dialogs", t => t.DialogId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.DialogId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Friends",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        FriendId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.FriendId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Users", t => t.FriendId)
                .Index(t => t.UserId)
                .Index(t => t.FriendId);
            
            CreateTable(
                "dbo.EventTags",
                c => new
                    {
                        EventId = c.Int(nullable: false),
                        TagId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.EventId, t.TagId })
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.TagId);
            
            CreateStoredProcedure(
                "dbo.Message_Insert",
                p => new
                    {
                        Id = p.String(maxLength: 128),
                        Content = p.String(),
                        DialogId = p.String(maxLength: 128),
                        Type = p.Int(),
                        TimeStamp = p.DateTime(storeType: "datetime2"),
                        SenderId = p.Int(),
                        Viewed = p.Boolean(),
                    },
                body:
                    @"INSERT [dbo].[Messages]([Id], [Content], [DialogId], [Type], [TimeStamp], [SenderId], [Viewed])
                      VALUES (@Id, @Content, @DialogId, @Type, @TimeStamp, @SenderId, @Viewed)"
            );
            
            CreateStoredProcedure(
                "dbo.Message_Update",
                p => new
                    {
                        Id = p.String(maxLength: 128),
                        Content = p.String(),
                        DialogId = p.String(maxLength: 128),
                        Type = p.Int(),
                        TimeStamp = p.DateTime(storeType: "datetime2"),
                        SenderId = p.Int(),
                        Viewed = p.Boolean(),
                    },
                body:
                    @"UPDATE [dbo].[Messages]
                      SET [Content] = @Content, [DialogId] = @DialogId, [Type] = @Type, [TimeStamp] = @TimeStamp, [SenderId] = @SenderId, [Viewed] = @Viewed
                      WHERE ([Id] = @Id)"
            );
            
            CreateStoredProcedure(
                "dbo.Message_Delete",
                p => new
                    {
                        Id = p.String(maxLength: 128),
                    },
                body:
                    @"DELETE [dbo].[Messages]
                      WHERE ([Id] = @Id)"
            );


            string sql = @"
                ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.Locations_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Locations] ([Id]) ON DELETE CASCADE";

            Sql(sql);

            AlterStoredProcedure(
                            "dbo.Message_Insert",
                            p => new
                            {
                                Id = p.String(maxLength: 128),
                                Content = p.String(),
                                DialogId = p.String(maxLength: 128),
                                Type = p.Int(),
                                TimeStamp = p.DateTime(storeType: "datetime2"),
                                SenderId = p.Int(),
                                Viewed = p.Boolean(),
                            },
                            body:
                                @"INSERT [dbo].[Messages]([Id], [Content], [DialogId], [Type], [TimeStamp], [SenderId], [Viewed])
                      VALUES (@Id, @Content, @DialogId, @Type, @TimeStamp, @SenderId, @Viewed)
                      INSERT INTO [dbo].[UnreadMessages] ([MessageId], [UserId])
                      (SELECT @Id as MessageId, DU.UserId as UserId FROM [dbo].[Dialogs] D
                      INNER JOIN [dbo].[DialogsUsers] DU on D.Id = DU.DialogId
                      WHERE NOT DU.UserId = @SenderId AND D.Id = @DialogId)"
                        );

            CreateStoredProcedure(
                "dbo.Message_Read",
                p => new
                {
                    UserId = p.Int(),
                    MessageId = p.String(maxLength: 128)
                },
                body:
                    @"DELETE [dbo].[UnreadMessages]
                      WHERE ([MessageId] = @MessageId) AND ([UserId] = @UserId)
                      IF NOT EXISTS(SELECT * FROM [dbo].[UnreadMessages] 
                          WHERE [MessageId] = @MessageId AND [UserId] = @UserId)
                          UPDATE [dbo].[Messages]
                          SET Viewed = 1"
            );

            // prevents from having request and invite for the same event at the same time
            sql = @"BEGIN TRAN

            CREATE TABLE dbo.TwoNums
              (
                 Num INT PRIMARY KEY
              )

            INSERT INTO dbo.TwoNums
            VALUES      (1),
                        (2) 

            GO

            CREATE VIEW dbo.PreventInviteRequestOnSameEvent
            WITH SCHEMABINDING
            AS
              SELECT R.EventId,
                     R.UserId
              FROM   dbo.Requests R
                     JOIN dbo.Invites I
                       ON R.EventId = I.EventId AND R.UserId = I.InvitedUserId
                     CROSS JOIN dbo.TwoNums 
                     WHERE R.State = 1 AND I.State = 1
            GO

            CREATE UNIQUE CLUSTERED INDEX [UIX_PreventInviteRequestOnSameEvent (EventId, UserId)]
              ON dbo.PreventInviteRequestOnSameEvent(EventId, UserId) 

            GO

            COMMIT";

            Sql(sql);
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Message_Delete");
            DropStoredProcedure("dbo.Message_Update");
            DropStoredProcedure("dbo.Message_Insert");
            DropForeignKey("dbo.EventTags", "TagId", "dbo.Tags");
            DropForeignKey("dbo.EventTags", "EventId", "dbo.Events");
            DropForeignKey("dbo.Events", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Events", "DialogId", "dbo.Dialogs");
            DropForeignKey("dbo.Events", "CategoryId", "dbo.EventCategories");
            DropForeignKey("dbo.UserToEvents", "UserId", "dbo.Users");
            DropForeignKey("dbo.UnreadMessages", "UserId", "dbo.Users");
            DropForeignKey("dbo.Requests", "UserId", "dbo.Users");
            DropForeignKey("dbo.Requests", "EventId", "dbo.Events");
            DropForeignKey("dbo.Invites", "UserId", "dbo.Users");
            DropForeignKey("dbo.Invites", "InvitedUserId", "dbo.Users");
            DropForeignKey("dbo.Invites", "EventId", "dbo.Events");
            DropForeignKey("dbo.Friends", "FriendId", "dbo.Users");
            DropForeignKey("dbo.Friends", "UserId", "dbo.Users");
            DropForeignKey("dbo.DialogsUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.DialogsUsers", "DialogId", "dbo.Dialogs");
            DropForeignKey("dbo.Messages", "DialogId", "dbo.Dialogs");
            DropForeignKey("dbo.UnreadMessages", "MessageId", "dbo.Messages");
            DropForeignKey("dbo.Messages", "SenderId", "dbo.Users");
            DropForeignKey("dbo.Users", "AddressId", "dbo.Locations");
            DropForeignKey("dbo.UserToEvents", "EventId", "dbo.Events");
            DropIndex("dbo.EventTags", new[] { "TagId" });
            DropIndex("dbo.EventTags", new[] { "EventId" });
            DropIndex("dbo.Friends", new[] { "FriendId" });
            DropIndex("dbo.Friends", new[] { "UserId" });
            DropIndex("dbo.DialogsUsers", new[] { "UserId" });
            DropIndex("dbo.DialogsUsers", new[] { "DialogId" });
            DropIndex("dbo.Requests", new[] { "UserId" });
            DropIndex("dbo.Requests", new[] { "EventId" });
            DropIndex("dbo.Invites", new[] { "UserId" });
            DropIndex("dbo.Invites", new[] { "InvitedUserId" });
            DropIndex("dbo.Invites", new[] { "EventId" });
            DropIndex("dbo.UnreadMessages", new[] { "UserId" });
            DropIndex("dbo.UnreadMessages", new[] { "MessageId" });
            DropIndex("dbo.Messages", new[] { "SenderId" });
            DropIndex("dbo.Messages", new[] { "DialogId" });
            DropIndex("dbo.Users", new[] { "AddressId" });
            DropIndex("dbo.UserToEvents", new[] { "EventId" });
            DropIndex("dbo.UserToEvents", new[] { "UserId" });
            DropIndex("dbo.Events", new[] { "DialogId" });
            DropIndex("dbo.Events", new[] { "CategoryId" });
            DropIndex("dbo.Events", new[] { "LocationId" });
            DropTable("dbo.EventTags");
            DropTable("dbo.Friends");
            DropTable("dbo.DialogsUsers");
            DropTable("dbo.Tags");
            DropTable("dbo.EventCategories");
            DropTable("dbo.Requests");
            DropTable("dbo.Invites");
            DropTable("dbo.UnreadMessages");
            DropTable("dbo.Messages");
            DropTable("dbo.Dialogs");
            DropTable("dbo.Locations");
            DropTable("dbo.Users");
            DropTable("dbo.UserToEvents");
            DropTable("dbo.Events");
        }
    }
}
