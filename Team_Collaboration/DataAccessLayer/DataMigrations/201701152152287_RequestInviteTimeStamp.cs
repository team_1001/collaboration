namespace DataAccessLayer.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequestInviteTimeStamp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invites", "TimeStamp", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Requests", "TimeStamp", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Requests", "TimeStamp");
            DropColumn("dbo.Invites", "TimeStamp");
        }
    }
}
