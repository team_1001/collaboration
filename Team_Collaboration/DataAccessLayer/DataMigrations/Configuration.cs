using DataLayer.Entity;

namespace DataAccessLayer.DataMigrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<DataAccessLayer.ActivityContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"DataMigrations";
        }

        protected override void Seed(DataAccessLayer.ActivityContext context)
        {
            context.Set<EventCategory>()
                .AddOrUpdate(
                    x => x.Name,
                    new EventCategory()
                    {
                        Name = "Sports",
                        Description = "Sports"
                    },
                    new EventCategory()
                    {
                        Name = "Games",
                        Description = "Games"
                    },
                    new EventCategory()
                    {
                        Name = "Other",
                        Description = "Other"
                    }
                );

            base.Seed(context);
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
