namespace DataAccessLayer.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Notifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MissedNotifications",
                c => new
                    {
                        NotificationId = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.NotificationId, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Notifications", t => t.NotificationId, cascadeDelete: true)
                .Index(t => t.NotificationId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TimeStamp = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Type = c.Int(nullable: false),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserNotifications",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        NotificationId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.NotificationId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Notifications", t => t.NotificationId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.NotificationId);

            CreateStoredProcedure(
                "dbo.Notifications_Insert_ForAll",
                p => new
                {
                    Id = p.String(maxLength: 128),
                    TimeStamp = p.DateTime(storeType: "datetime2"),
                    Type = p.Int(),
                    Data = p.String(),
                    UserId = p.Int()
                },
                body:
                    @"INSERT [dbo].[Notifications]([Id], [TimeStamp], [Type], [Data])
                      VALUES (@Id, @TimeStamp, @Type, @Data)
                      INSERT [dbo].[UserNotifications]([UserId], [NotificationId])
                      SELECT UserId = Id, NotificationId = @Id FROM [dbo].[Users] u WHERE NOT u.Id = @UserId
                      INSERT [dbo].[MissedNotifications]([UserId], [NotificationId])
                      SELECT UserId = Id, NotificationId = @Id FROM [dbo].[Users] u WHERE NOT u.Id = @UserId"
            );
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserNotifications", "NotificationId", "dbo.Notifications");
            DropForeignKey("dbo.UserNotifications", "UserId", "dbo.Users");
            DropForeignKey("dbo.MissedNotifications", "NotificationId", "dbo.Notifications");
            DropForeignKey("dbo.MissedNotifications", "UserId", "dbo.Users");
            DropIndex("dbo.UserNotifications", new[] { "NotificationId" });
            DropIndex("dbo.UserNotifications", new[] { "UserId" });
            DropIndex("dbo.MissedNotifications", new[] { "UserId" });
            DropIndex("dbo.MissedNotifications", new[] { "NotificationId" });
            DropTable("dbo.UserNotifications");
            DropTable("dbo.Notifications");
            DropTable("dbo.MissedNotifications");
        }
    }
}
