﻿using System;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Threading.Tasks;
using DataAccessLayer.Configuration;
using DataLayer.Entity;
using User = DataLayer.Entity.User;

namespace DataAccessLayer
{
    [DbConfigurationType(typeof(ActivityContextConfiguration))]
    public class ActivityContext : DbContext
    {
        public IDbSet<User> Users { get; set; }

        public IDbSet<Event> Events { get; set; }

        public IDbSet<Invite> Invites { get; set; }

        public IDbSet<Location> Locations { get; set; }

        public IDbSet<Request> Requests { get; set; }

        public IDbSet<UserToEvent> UserToEvents { get; set; }

        public ActivityContext() : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public ActivityContext(string cstring) : base(cstring)
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public async Task ReadMessage(int userId, string messageId)
        {
            try
            {
                var userIdParam = new SqlParameter("@UserId", userId);
                var messageIdParam = new SqlParameter("@MessageId", messageId);

                await this.Database.ExecuteSqlCommandAsync(TransactionalBehavior.EnsureTransaction, 
                    "exec [dbo].[Message_Read] @UserId, @MessageId", 
                    userIdParam,
                    messageIdParam);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task CreateNotificationForAll(int userId, Notification notification)
        {
            try
            {
                var userIdParam = new SqlParameter("@UserId", userId);
                var idParam = new SqlParameter("@Id", notification.Id);
                var timeStampParam = new SqlParameter("@TimeStamp", notification.TimeStamp);
                var typeParam = new SqlParameter("@Type", (int) notification.Type);
                var dataParam = new SqlParameter("@Data", notification.Data);

                await this.Database.ExecuteSqlCommandAsync(TransactionalBehavior.EnsureTransaction,
                    "exec [dbo].[Notifications_Insert_ForAll] @Id, @TimeStamp, @Type, @Data, @UserId",
                    idParam,
                    timeStampParam,
                    typeParam,
                    dataParam,
                    userIdParam);
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            modelBuilder.Entity<MissedNotification>()
                .HasKey(x => new { x.NotificationId, x.UserId });

            modelBuilder.Entity<UnreadMessage>()
                .HasKey(x => new { x.MessageId, x.UserId });

            modelBuilder.Entity<Event>()
                .HasRequired(e => e.Location)
                .WithMany().HasForeignKey(e=>e.LocationId);

            modelBuilder.Entity<Event>()
                .HasMany(x => x.Tags)
                .WithMany()
                .Map(x => x.ToTable("EventTags").MapLeftKey("EventId").MapRightKey("TagId"));
                
            modelBuilder.Entity<Event>()
                .HasRequired(x => x.Dialog)
                .WithMany()
                .HasForeignKey(x => x.DialogId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Event>()
               .HasRequired(e => e.Category)
               .WithMany().HasForeignKey(e => e.CategoryId);

            modelBuilder.Entity<User>()
                .HasKey(x => x.Id)
                .HasMany(x => x.Friends).WithMany().Map(x => x.ToTable("Friends").MapLeftKey("UserId").MapRightKey("FriendId"));

            modelBuilder.Entity<User>()
                .HasMany(x => x.UnreadMessages)
                .WithRequired()
                .HasForeignKey(x => x.UserId);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Notifications)
                .WithMany(x => x.Users)
                .Map(x => x.ToTable("UserNotifications").MapLeftKey("UserId").MapRightKey("NotificationId"));

            modelBuilder.Entity<User>()
                .HasMany(x => x.MissedNotifications)
                .WithRequired()
                .HasForeignKey(x => x.UserId);

            modelBuilder.Entity<Notification>()
                .HasMany(x => x.MissedNotifications)
                .WithRequired()
                .HasForeignKey(x => x.NotificationId);

            modelBuilder.Entity<UserToEvent>().HasKey(k => new
            {
                k.UserId,
                k.EventId
            });

            modelBuilder.Entity<Invite>()
                .HasKey(x => new {x.EventId, x.InvitedUserId});

            modelBuilder.Entity<Invite>()
                .HasRequired(iu => iu.InvitedUser)
                .WithMany(u => u.IncomingInvites)
                .HasForeignKey(i => i.InvitedUserId).WillCascadeOnDelete(false);

            modelBuilder.Entity<Invite>()
                .HasRequired(iu => iu.User)
                .WithMany(u => u.OutgoingInvites)
                .HasForeignKey(i => i.UserId).WillCascadeOnDelete(false);

            modelBuilder.Entity<Invite>()
                .HasRequired(p => p.Event)
                .WithMany(i => i.Invites)
                .HasForeignKey(fk => fk.EventId);

            modelBuilder.Entity<UserToEvent>()
                .HasRequired(e => e.Event)
                .WithMany(e => e.AllUsers)
                .HasForeignKey(fk => fk.EventId);

            modelBuilder.Entity<UserToEvent>()
                .HasRequired(u => u.User)
                .WithMany(u => u.Events)
                .HasForeignKey(fk => fk.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Request>()
                .HasKey(x => new {x.EventId, x.UserId });

            modelBuilder.Entity<Request>()
                .HasRequired(e => e.Event)
                .WithMany(r => r.Requests)
                .HasForeignKey(fk => fk.EventId);

            modelBuilder.Entity<Request>()
                .HasRequired(u => u.User)
                .WithMany(u => u.Requests)
                .HasForeignKey(r => r.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasOptional(u => u.Address)
                .WithMany().HasForeignKey(u => u.AddressId).WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(friends => friends.Friends)
                .WithMany().Map(x => x.ToTable("Friends").MapLeftKey("UserId").MapRightKey("FriendId"));

            modelBuilder.Entity<Dialog>()
                .HasMany(x => x.Participants)
                .WithMany(x => x.Dialogs)
                .Map(x => x.ToTable("DialogsUsers").MapLeftKey("DialogId").MapRightKey("UserId"));

            modelBuilder.Entity<Dialog>()
                .HasKey(x => x.Id)
                .HasMany(x => x.Messages)
                .WithRequired()
                .HasForeignKey(x => x.DialogId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Message>()
                .HasKey(x => x.Id)
                .MapToStoredProcedures();

            modelBuilder.Entity<Message>()
                .HasRequired(x => x.Sender)
                .WithMany()
                .HasForeignKey(x => x.SenderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Message>()
                .HasMany(x => x.UnreadMessages)
                .WithRequired()
                .HasForeignKey(x => x.MessageId);
                
            base.OnModelCreating(modelBuilder);
        }
    }
}
