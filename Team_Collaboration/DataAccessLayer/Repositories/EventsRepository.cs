﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using DataAccessLayer.UnitOfWork;
using DataLayer.Entity;
using Models;
using Models.Exceptions;
using Models.Paging;

namespace DataAccessLayer.Repositories
{
    public class EventsRepository
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;

        public EventsRepository(ILogger logger, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<EventCategory>> GetCategories()
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    return await context.Set<EventCategory>().ToListAsync();
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }
            }

            return null;
        }
        
        public async Task<ItemsResult<Event>> GetGeoEvents(GeoInfo geo, PageInfo pageInfo)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    // calculate HaversineDistance here: https://en.wikipedia.org/wiki/Haversine_formula
                    var eventsSet = context.Set<Event>();
                    if (pageInfo.NoPaging)
                    {
                        var events = await eventsSet
                            .Select(e => new
                            {
                                Event = e,
                                H1 = SqlFunctions.SquareRoot(
                                    SqlFunctions.Square(SqlFunctions.Sin(SqlFunctions.Radians(geo.Latitude - e.Location.Latitude) / 2))
                                    + (SqlFunctions.Cos(SqlFunctions.Radians(geo.Latitude)) 
                                    * SqlFunctions.Cos(SqlFunctions.Radians(e.Location.Latitude))
                                    * SqlFunctions.Square(SqlFunctions.Sin(SqlFunctions.Radians(geo.Longitude - e.Location.Longitude) / 2))))
                            }).Select(e => new
                            {
                                e.Event,
                                D = 6371 * 2 * SqlFunctions.Asin(e.H1 < 1 ? e.H1 : 1) // 6371 is for kilometers
                            })
                            .Where(e => e.D < geo.Radius)
                            .Select(e => new
                            {
                                Event = e.Event,
                                Location = e.Event.Location,
                                HostId = e.Event.AllUsers.FirstOrDefault(u => u.Role == EventRoles.Host).UserId
                            }).ToListAsync();

                        events.ForEach(e =>
                        {
                            e.Event.HostId = e.HostId;
                            e.Event.Location = e.Location;
                        });

                        return new ItemsResult<Event>(events.Select(e => e.Event), events.Count);
                    }

                    var eventsPaged = await
                        eventsSet
                            .Where(x => pageInfo.LastId == null || x.Id > pageInfo.LastId)
                            .Select(e =>
                                new
                                {
                                    EventInfo = new
                                    {
                                        Event = e,
                                        HostId = e.AllUsers.FirstOrDefault(u => u.Role == EventRoles.Host).UserId
                                    },

                                    Total = eventsSet.Count()
                                }).Skip((pageInfo.Page - 1) * pageInfo.PageSize)
                            .Take(pageInfo.PageSize)
                            .ToListAsync();

                    eventsPaged.ForEach(e => e.EventInfo.Event.HostId = e.EventInfo.HostId);
                    return new ItemsResult<Event>(eventsPaged.Select(e => e.EventInfo.Event),
                        eventsPaged.FirstOrDefault()?.Total ?? 0);
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<ItemsResult<Event>> GetEvents(PageInfo pageInfo)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var eventsSet = context.Set<Event>();
                    if (pageInfo.NoPaging)
                    {
                        var events = await eventsSet
                            .Select(e => new
                            {
                                Event = e,
                                HostId = e.AllUsers.FirstOrDefault(u => u.Role == EventRoles.Host).UserId
                            })
                            .OrderByDescending(x => x.Event.Id)
                            .ToListAsync();

                        events.ForEach(e => e.Event.HostId = e.HostId);
                        return new ItemsResult<Event>(events.Select(e => e.Event), events.Count);
                    }

                    var eventsPaged = await
                        eventsSet
                            .Where(x => pageInfo.LastId == null || x.Id > pageInfo.LastId)
                            .Select(e =>
                                new
                                {
                                    EventInfo = new
                                    {
                                        Event = e,
                                        HostId = e.AllUsers.FirstOrDefault(u => u.Role == EventRoles.Host).UserId
                                    },

                                    Total = eventsSet.Count()
                                })
                            .OrderByDescending(e => e.EventInfo.Event.Id)
                            .Skip((pageInfo.Page - 1)*pageInfo.PageSize)
                            .Take(pageInfo.PageSize)
                            .ToListAsync();

                    eventsPaged.ForEach(e => e.EventInfo.Event.HostId = e.EventInfo.HostId);
                    return new ItemsResult<Event>(eventsPaged.Select(e => e.EventInfo.Event),
                        eventsPaged.FirstOrDefault()?.Total ?? 0);
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<IEnumerable<Event>> GetEvents(int userId, params string[] roles)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var events = await context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(
                            x =>
                                x.Events.Where(e => (!roles.Any() || roles.Contains(e.Role)) && e.UserId == userId)
                                    .Select(e =>
                                        new
                                        {
                                            e.Event,
                                            HostId = e.Event.AllUsers.FirstOrDefault(u => u.Role == EventRoles.Host).UserId
                                        }))
                        .ToListAsync();

                    events.ForEach(e => e.Event.HostId = e.HostId);
                    return events.Select(x => x.Event);
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<IEnumerable<UserToEvent>> GetParticipants(int eventId, int userId, params string[] roles)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    return await context.Set<UserToEvent>()
                        .Where(x => x.EventId == eventId && x.UserId != userId && (!roles.Any() || roles.Contains(x.Role)))
                        .ToListAsync();
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<bool> Create(Event @event, int hostId)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var user = await context.FindAsync<User>(hostId);
                    if (user == null)
                        throw new UserException($"User {hostId} wasn't found");

                    var category = await context.FindAsync<EventCategory>(@event.CategoryId);
                    if (category == null)
                        throw new EventException($"Invalid category {@event.CategoryId}");

                    var dialog = new Dialog() { Name = @event.Name, Id = Guid.NewGuid().ToString("n") };
                    dialog.Participants.Add(user);

                    @event.Dialog = dialog;

                    UserToEvent userToEvent = new UserToEvent()
                    {
                        Event = @event,
                        UserId = hostId,
                        Role = EventRoles.Host
                    };

                    context.Add(userToEvent);
                    result = await context.CommitAsync();
                }
                catch (UserException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding event to db", e);
                    _logger.Error(e);
                }
            }

            return result;
        }

        public async Task<bool> Update(Event @event, int hostId)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var eventSet = context.Set<Event>();
                    var existing =
                        await eventSet
                            .Where(x => x.Id == @event.Id)
                            .Select(x => new
                            {
                                Event = x,
                                HostId = x.AllUsers.Where(u => u.Role == EventRoles.Host).Select(u => u.UserId).FirstOrDefault()
                            }).FirstOrDefaultAsync();

                    if (existing == null)
                        throw new EventException($"Event {@event.Id} doesn't exist");

                    if (existing.HostId != hostId)
                        throw new EventException(
                            $"User {hostId} is not a host of the event {@event.Id}. Can't apply changes");

                    result = await context.Detach(existing.Event).Update(@event).CommitAsync();
                }
                catch (EventException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error updating event {0}", @event.Id);
                    _logger.Error(e);
                }
            }

            return result;
        }

        public async Task<bool> Delete(int @eventId, int hostId)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var existing =
                        await context.Set<Event>()
                            .Where(x => x.Id == @eventId)
                            .Select(x => new
                            {
                                Event = x,
                                HostId = x.AllUsers.Where(u => u.Role == EventRoles.Host).Select(u => u.UserId).FirstOrDefault()
                            }).FirstOrDefaultAsync();

                    if (existing == null)
                        throw new EventException($"Event {@eventId} doesn't exist");

                    if (existing.HostId != hostId)
                        throw new EventException(
                            $"User {hostId} is not a host of the event {@eventId}. Can't apply changes");

                    result = await context.Remove(existing.Event).CommitAsync();
                }
                catch (EventException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error removing event {0} from db", @eventId);
                    _logger.Error(e);
                }
            }

            return result;
        }

        public async Task<Event> Get(int eventId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var @event = await context.Set<Event>()
                        .Where(e => e.Id == eventId)
                        .Select(e =>
                            new
                            {
                                Event = e,
                                Location = e.Location,
                                HostId = e.AllUsers.FirstOrDefault(u => u.Role == EventRoles.Host).UserId
                            }).FirstOrDefaultAsync();

                    if (@event != null)
                    {
                        @event.Event.HostId = @event.HostId;
                        @event.Event.Location = @event.Location;
                        return @event.Event;
                    }

                    return null;
                }
                catch (Exception e)
                {
                    _logger.Error("Error getting event {0}", eventId);
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<EventResult> Get(int eventId, int userId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var @event = await context.Set<Event>()
                        .Where(e => e.Id == eventId)
                        .Select(e =>
                            new
                            {
                                Event = e,
                                Location = e.Location,
                                IsJoined = e.AllUsers.Any(u => u.Role != EventRoles.Host && u.UserId == userId),
                                IsHost = e.AllUsers.Any(u => u.UserId == userId && u.Role == EventRoles.Host),
                                IsRequested = e.Requests.Any(r => r.UserId == userId && r.State == ProposalState.Pending),
                                IsInvited = e.Invites.Any(r => r.InvitedUserId == userId && r.State == ProposalState.Pending),
                                HostId = e.AllUsers.FirstOrDefault(u => u.Role == EventRoles.Host).UserId
                            }).FirstOrDefaultAsync();

                    if (@event != null)
                    {
                        @event.Event.HostId = @event.HostId;
                        return new EventResult()
                        {
                            Event = @event.Event,
                            UserId = userId,
                            IsHost = @event.IsHost,
                            IsInvited = @event.IsInvited,
                            IsJoined = @event.IsJoined,
                            IsRequested = @event.IsRequested,
                            IsPublic = @event.Event.EventType == EventType.Public
                        };
                    }

                    return null;
                }
                catch (Exception e)
                {
                    _logger.Error("Error getting event {0}", eventId);
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<bool> Join(int @eventId, int userId)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var existing =
                        await context.Set<Event>()
                            .Where(x => x.Id == @eventId)
                            .Select(x => new
                            {
                                EventId = x.Id,
                                Dialog = x.Dialog,
                                UserIds = x.AllUsers.Select(u => u.UserId)
                            }).FirstOrDefaultAsync();

                    if (existing == null)
                        throw new EventException($"Event {@eventId} doesn't exist");

                    if (existing.UserIds.Contains(userId))
                        throw new EventException($"User {userId} already participates in event {@eventId}");

                    var user =
                        await
                            context.Set<User>()
                                .Where(u => u.Id == userId)
                                .Select(u => new {User = u, DialogIds = u.Dialogs.Select(d => d.Id)})
                                .FirstOrDefaultAsync();

                    if (user == null)
                        throw new UserException($"User {userId} doesn't exist");

                    if (!user.DialogIds.Contains(existing.Dialog.Id))
                        user.User.Dialogs.Add(existing.Dialog);

                    var invites =
                        await context.Set<Invite>().Where(i => i.EventId == eventId && i.InvitedUserId == userId).ToListAsync();

                    invites.ForEach(i =>
                    {
                        i.State = ProposalState.Accepted;
                    });

                    UserToEvent userToEvent = new UserToEvent()
                    {
                        EventId = existing.EventId,
                        User = user.User,
                        Role = EventRoles.User
                    };

                    result = await context.AttachRange(invites).Add(userToEvent).CommitAsync();
                }
                catch (UserException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (EventException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error removing event {0} from db", @eventId);
                    _logger.Error(e);
                }
            }

            return result;
        }

        public async Task<bool> Leave(int @eventId, int userId)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var existing = await context.FindAsync<UserToEvent>(userId, eventId);
                    if (existing == null)
                        throw new EventException($"User {userId} doesn't participate in event {@eventId}");

                    result = await context.Remove(existing).CommitAsync();
                }
                catch (UserException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (EventException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error removing event {0} from db", @eventId);
                    _logger.Error(e);
                }
            }

            return result;
        }
    }
}
