﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using Common.Extensions;
using DataAccessLayer.UnitOfWork;
using DataLayer.Entity;
using Models;
using Models.Hub;
using Models.Paging;

namespace DataAccessLayer.Repositories
{
    public class MessagesRepository
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;

        public MessagesRepository(
            ILogger logger, 
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> AddMessage(HubNewMessage message)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    Message entity = new Message()
                    {
                        Id = message.Id,
                        Content =  message.Content,
                        DialogId = message.DialogId,
                        SenderId = message.SenderId,
                        Type = message.Type,
                        TimeStamp = message.TimeStamp.Convert()
                    };

                    context.Add(entity);
                    result = await context.CommitAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding message to db: {0}", e);
                }
            }

            return result;
        }

        public async Task<Message> GetMessage(string id)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    return await context.FindAsync<Message>(id);
                }
                catch (Exception e)
                {
                    _logger.Error("Error getting message {0}", id);
                    _logger.Error(e);
                }
            }

            return null;
        } 

        public async Task<IEnumerable<int>> GetReceivers(string dialogId, int senderId)
        {
            IEnumerable<int> receivers = null;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    receivers = await context.Set<Dialog>()
                        .Where(x => x.Id == dialogId)
                        .SelectMany(x => x.Participants.Where(p => p.Id != senderId).Select(p => p.Id))
                        .ToListAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving users from db: {0}", e);
                }
            }

            return receivers;
        }

        public async Task<IEnumerable<int>> GetRelatedUsers(int userId)
        {
            IEnumerable<int> receivers = null;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    receivers = await context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(
                            x => x.Dialogs.SelectMany(d => d.Participants).Where(p => p.Id != userId).Select(u => u.Id))
                        .ToListAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving users from db: {0}", e);
                }
            }

            return receivers;
        }

        public async Task<bool> ReadMessage(HubExistingMessage message)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    await context.Execute<ActivityContext>(async c => await c.ReadMessage(message.ReceiverId, message.Id));
                    return true;
                }
                catch (Exception e)
                {
                    _logger.Error("Error reading message {0} for user {1}", message.Id, message.ReceiverId);
                    _logger.Error(e);
                }
            }

            return false;
        }

        public async Task<IEnumerable<Dialog>> GetDialogs(int userId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var eventsSet = context.Set<Event>();
                    var dialogs = await context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(x => x.Dialogs)
                        .Select(d => new
                        {
                            Dialog = d,
                            LastMessage = d.Messages.OrderByDescending(m => m.TimeStamp).FirstOrDefault(),
                            Participants = d.Participants.Where(p => p.Id != userId).Select(p => p.FirstName + " " + p.LastName),
                            EventName = eventsSet.FirstOrDefault(e => e.DialogId == d.Id).Name,
                            UnreadMessages = d.Messages.Count(m => m.UnreadMessages.Any(u => u.UserId == userId))
                        })
                        .ToListAsync();

                    dialogs.ForEach(d =>
                    {
                        d.Dialog.FullName = d.EventName ??
                                            (d.Participants.Any()
                                                ? string.Join(", ", d.Participants)
                                                : d.Dialog.Name);

                        d.Dialog.IsEvent = !d.EventName.IsNullOrEmpty();
                        d.Dialog.ParticipantsCount = d.Participants.Count() + 1;
                        d.Dialog.LastMessage = d.LastMessage;
                        d.Dialog.UnreadCount = d.UnreadMessages;
                    });

                    return dialogs.Select(d => d.Dialog);
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving dialogs for user {0}", e);
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<IEnumerable<Message>> GetMessages(int userId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    return await context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(x => x.Dialogs.SelectMany(d => d.Messages))
                        .ToListAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving messages for user {0}", e);
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<int> GetUnreadMessagesCount(int userId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    return await context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(x => x.Dialogs.SelectMany(d => d.Messages.Where(m => m.UnreadMessages.Any(u => u.UserId == userId))))
                        .CountAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving messages for user {0}", e);
                    _logger.Error(e);
                }
            }

            return -1;
        }

        public async Task<int> GetUnreadMessagesCount(int userId, string dialogId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    return await context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(x => x.Dialogs.Where(d => d.Id == dialogId)
                            .SelectMany(d => d.Messages.Where(m => m.UnreadMessages.Any(u => u.UserId == userId))))
                        .CountAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving messages for user {0}", e);
                    _logger.Error(e);
                }
            }

            return -1;
        }

        public async Task<IEnumerable<Message>> GetUnreadMessages(int userId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    return await context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(x => x.Dialogs.SelectMany(d => d.Messages.Where(m => m.UnreadMessages.Any(u => u.UserId == userId))))
                        .ToListAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving messages for user {0}", e);
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<IEnumerable<Message>> GetUnreadMessages(int userId, string dialogId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    return await context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(x => x.Dialogs.Where(d => d.Id == dialogId)
                            .SelectMany(d => d.Messages.Where(m => m.UnreadMessages.Any(u => u.UserId == userId))))
                        .ToListAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving messages for user {0}", e);
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<ItemsResult<Message>> GetMessages(int userId, string dialogId, PageInfo pageInfo)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var query = context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(
                            x =>
                                x.Dialogs.Where(d => d.Id == dialogId)
                                    .SelectMany(d => d.Messages)
                                    .Select(m => new
                                    {
                                        Message = m,
                                        SenderName = m.Sender.FirstName + " " + m.Sender.LastName,
                                        Missed = m.UnreadMessages.Any(u => u.UserId == userId)
                                    }));

                    if (pageInfo.NoPaging)
                    {
                        var messages = await query.OrderBy(x => x.Message.TimeStamp).ToListAsync();
                        messages.ForEach(x =>
                        {
                            x.Message.Missed = x.Missed;
                            x.Message.SenderName = x.SenderName;
                        });

                        return new ItemsResult<Message>(messages.Select(x => x.Message), messages.Count);
                    }

                    var messagesPaged = await query
                        .OrderByDescending(x => x.Message.TimeStamp)
                        .Skip((pageInfo.Page - 1) * pageInfo.PageSize).Take(pageInfo.PageSize).Select(x => new
                        {
                            Item = x,
                            Total = query.Count()
                        }).OrderBy(x => x.Item.Message.TimeStamp).ToListAsync();

                    messagesPaged.ForEach(x =>
                    {
                        x.Item.Message.Missed = x.Item.Missed;
                        x.Item.Message.SenderName = x.Item.SenderName;
                    });

                    return new ItemsResult<Message>(messagesPaged.Select(x => x.Item.Message), messagesPaged.FirstOrDefault()?.Total ?? 0);
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving messages for user {0}", e);
                    _logger.Error(e);
                }
            }

            return null;
        }
    }
}
