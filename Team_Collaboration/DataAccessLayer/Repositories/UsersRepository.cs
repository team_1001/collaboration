﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using DataAccessLayer.UnitOfWork;
using DataLayer.Entity;
using Models;
using Models.Exceptions;
using Models.Paging;

namespace DataAccessLayer.Repositories
{
    public class UsersRepository
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;

        public UsersRepository(
            ILogger logger,
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<User> Get(int userId)
        {
            try
            {
                using (var context = _unitOfWork.Create())
                {
                    return await context.Set<User>().FirstOrDefaultAsync(x => x.Id == userId);
                }
            }
            catch (Exception e)
            {
                _logger.Error("Failed to get user {0}.", userId);
                _logger.Error(e);
            }

            return null;
        }

        public async Task<bool> Update(User user)
        {
            try
            {
                using (var context = _unitOfWork.Create())
                {
                    var fromDb = await context.FindAsync<User>(user.Id);
                    if (fromDb == null)
                        throw new UserException($"User {user.Id} doesn't exist");

                    return await context.Detach(fromDb).Update(user).CommitAsync();
                }
            }
            catch (UserException e)
            {
                _logger.Error(e);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to get user {0}.", user.Id);
                _logger.Error(e);
            }

            return false;
        }

        public async Task<ItemsResult<UserInfo>> GetAll(int userId, PageInfo pageInfo)
        {
            try
            {
                using (var context = _unitOfWork.Create())
                {
                    var usersSet = context.Set<User>();
                    if (pageInfo.NoPaging)
                    {
                        var users = await usersSet.Select(x => new UserInfo
                        {
                            User = x,
                            IsFriend = x.Friends.Select(f => f.Id).Contains(userId),
                            IsSelf = x.Id == userId
                        }).ToListAsync();

                        return new ItemsResult<UserInfo>(users, users.Count);
                    }

                    var usersPaged = await usersSet
                        .OrderBy(x => x.Id)
                        .Skip((pageInfo.Page - 1)*pageInfo.PageSize)
                        .Take(pageInfo.PageSize).Select(x => new
                        {
                            User = x,
                            IsFriend = x.Friends.Select(f => f.Id).Contains(userId),
                            IsSelf = x.Id == userId,
                            Total = usersSet.Count()
                        }).ToListAsync();

                    return new ItemsResult<UserInfo>(usersPaged.Select(u => new UserInfo()
                    {
                        User = u.User,
                        IsFriend = u.IsFriend,
                        IsSelf = u.IsSelf
                    }), usersPaged.FirstOrDefault()?.Total ?? 0);
                }
            }
            catch (Exception e)
            {
                _logger.Error("Failed to get users");
                _logger.Error(e);
            }

            return null;
        } 

        public async Task<IEnumerable<User>> GetFriends(int userId)
        {
            try
            {
                using (var context = _unitOfWork.Create())
                {
                    return await context.Set<User>().Where(x => x.Id == userId).SelectMany(x => x.Friends).ToListAsync();
                }
            }
            catch (Exception e)
            {
                _logger.Error("Failed to get friends of user {0}.", userId);
                _logger.Error(e);
            }

            return null;
        }

        public async Task<bool> AddFriend(int userId, int friendId)
        {
            try
            {
                if (userId == friendId)
                    throw new UserException("You can't friend yourself");

                using (var context = _unitOfWork.Create())
                {
                    var usersSet = context.Set<User>();

                    var users = await usersSet.Where(x => x.Id == userId || x.Id == friendId).ToListAsync();

                    var user = users.FirstOrDefault(x => x.Id == userId);
                    var friend = users.FirstOrDefault(x => x.Id == friendId);

                    if (user == null)
                        throw new UserException($"User {userId} wasn't found");

                    if (friend == null)
                        throw new UserException($"User {friendId} wasn't found");

                    if (user.Friends.Any(x => x.Id == friendId))
                        throw new UserException($"User {friendId} has already friended {userId}");

                    user.Friends.Add(friend);
                    friend.Friends.Add(user);

                    var dialogsSet = context.Set<Dialog>();
                    var eventSet = context.Set<Event>();

                    var dialog = await dialogsSet.GroupJoin(eventSet, x => x.Id, x => x.DialogId, (x, y) =>
                        new
                        {
                            Dialog = x,
                            Event = y.FirstOrDefault(e => e.DialogId == x.Id)
                        })
                        .Where(x =>
                            x.Event == null
                            && x.Dialog.Participants.Count() == 2
                            && x.Dialog.Participants.All(p => p.Id == friendId || p.Id == userId))
                        .Select(x => x.Dialog)
                        .FirstOrDefaultAsync();

                    if (dialog == null)
                    {
                        dialog = new Dialog
                        {
                            Id = Guid.NewGuid().ToString("n"),
                            Name = $"{user.UserName}, {friend.UserName}"
                        };

                        dialog.Participants.Add(friend);
                        dialog.Participants.Add(user);

                        context.Add(dialog);
                    }
                    else
                    {
                        context.Attach(user);
                        context.Attach(friend);
                    }

                    return await context.CommitAsync();
                }
            }
            catch (UserException e)
            {
                _logger.Error(e);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to get friends of user {0}.", userId);
                _logger.Error(e);
                throw new Exception("Internal error occured");
            }
        }

        public async Task<bool> RemoveFriend(int userId, int friendId)
        {
            try
            {
                using (var context = _unitOfWork.Create())
                {
                    var usersSet = context.Set<User>();

                    var users = await usersSet.Where(x => x.Id == userId || x.Id == friendId).Include(x => x.Friends).ToListAsync();

                    var user = users.FirstOrDefault(x => x.Id == userId);
                    var friend = users.FirstOrDefault(x => x.Id == friendId);

                    if (user == null)
                        throw new UserException($"User {userId} wasn't found");

                    if (friend == null)
                        throw new UserException($"User {friendId} wasn't found");

                    var userFriend = user.Friends.FirstOrDefault(x => x.Id == friendId);
                    var friendUser = friend.Friends.FirstOrDefault(x => x.Id == userId);

                    if (userFriend == null || friendUser == null)
                        throw new UserException($"User {friendId} isn't a friend of {userId}");
                    
                    user.Friends.Remove(friend);
                    friend.Friends.Remove(user);
                    context.Attach(user);
                    context.Attach(friend);

                    return await context.CommitAsync();
                }
            }
            catch (UserException e)
            {
                _logger.Error(e);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error("Failed to get friends of user {0}.", userId);
                _logger.Error(e);
                throw new Exception("Internal error occured");
            }
        }
    }
}
