﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using DataAccessLayer.UnitOfWork;
using DataLayer.Entity;
using LinqKit;
using Models;
using Models.Exceptions;
using Models.Paging;

namespace DataAccessLayer.Repositories
{
    public class NotificationsRepository
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;

        public NotificationsRepository(ILogger logger,
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> UpdateNotification(string id, int userId)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var notification = await context.FindAsync<MissedNotification>(id, userId);
                    if (notification == null)
                        throw new NotificationException($"Notification {id} for user {userId} doesn't exist");

                    result = await context.Remove(notification).CommitAsync();
                }
                catch (NotificationException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error updating notification {0}: {1}", id, e);
                }
            }

            return result;
        }

        public async Task<bool> UpdateNotification(NotificationType[] types, int userId)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var missedNotifications =
                        await
                            context.Set<User>()
                                .Where(x => x.Id == userId)
                                .SelectMany(
                                    x =>
                                        x.Notifications.Where(n => types.Contains(n.Type))
                                            .Join(x.MissedNotifications, n => n.Id, n => n.NotificationId, (a, b) => b))
                                .ToListAsync();
                    
                    result = await context.RemoveRange(missedNotifications).CommitAsync();
                }
                catch (NotificationException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error updating notification of type {0}: {1}", string.Join(", ", types), e);
                }
            }

            return result;
        }

        public async Task<NotificationCreationResult> AddNotification(int userId, NotificationType type, string data)
        {
            bool result = false;
            string newNotificationId = Guid.NewGuid().ToString("n");

            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var user = await context.FindAsync<User>(userId);
                    if (user == null)
                        throw new UserException($"User {userId} doesn't exist");

                    Notification notification = new Notification()
                    {
                        Id = newNotificationId,
                        Data = data,
                        TimeStamp = DateTime.UtcNow,
                        Type = type
                    };

                    notification.Users.Add(user);
                    user.Notifications.Add(notification);
                    user.MissedNotifications.Add(new MissedNotification()
                    {
                        UserId = userId,
                        NotificationId = notification.Id
                    });

                    result = await context.Add(notification).CommitAsync();
                }
                catch (UserException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding notification to db: {0}", e);
                }
            }

            return new NotificationCreationResult() { Success = result, NotificationId = newNotificationId };
        }

        public async Task<NotificationCreationResult> AddNotifications(int[] userIds, NotificationType type, string data)
        {
            bool result = false;
            string newNotificationId = Guid.NewGuid().ToString("n");

            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var predicate = userIds.Aggregate(PredicateBuilder.New<User>(),
                        (p, userId) => p.Or(u => u.Id == userId));

                    var users = await context.Set<User>().AsExpandable().Where(predicate).ToListAsync();
                    var notFound =
                        userIds.GroupJoin(users, x => x, x => x.Id, (x, y) => new {Id = x, Item = y.FirstOrDefault()})
                            .Where(x => x.Item == null)
                            .ToList();

                    if (notFound.Count > 0)
                        throw new UserException($"Users {string.Join(", ", notFound.Select(u => u.Id))} don't exist");
                    
                    Notification notification = new Notification()
                    {
                        Id = newNotificationId,
                        Data = data,
                        TimeStamp = DateTime.UtcNow,
                        Type = type
                    };

                    users.ForEach(
                        u =>
                        {
                            notification.Users.Add(u);
                            u.Notifications.Add(notification);
                            u.MissedNotifications.Add(new MissedNotification()
                            {
                                UserId = u.Id,
                                NotificationId = notification.Id
                            });
                        });

                    result = await context.Add(notification).CommitAsync();
                }
                catch (UserException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding notifications to db: {0}", e);
                }
            }

            return new NotificationCreationResult() { Success = result, NotificationId = newNotificationId };
        }

        public async Task<NotificationCreationResult> AddNotificationsToAllExcept(int userId, NotificationType type, string data)
        {
            bool result = false;
            string newNotificationId = Guid.NewGuid().ToString("n");

            using (var context = _unitOfWork.Create())
            {
                try
                {
                    Notification notification = new Notification()
                    {
                        Id = newNotificationId,
                        Data = data,
                        TimeStamp = DateTime.UtcNow,
                        Type = type
                    };

                    await context.Execute<ActivityContext>(async c => await c.CreateNotificationForAll(userId, notification));
                    result = true;
                }
                catch (UserException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding notifications to db: {0}", e);
                }
            }

            return new NotificationCreationResult() { Success = result, NotificationId = newNotificationId };
        }

        public async Task<ItemsResult<Notification>> GetNotifications(int userId, PageInfo pageInfo, bool onlyMissed = true, params NotificationType[] types)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var query = context.Set<User>()
                        .Where(x => x.Id == userId)
                        .SelectMany(u => u.Notifications)
                        .Where(n => !types.Any() || types.Contains(n.Type))
                        .Select(x => new { Item = x, Missed = x.MissedNotifications.Any(n => n.UserId == userId )})
                        .Where(x => !onlyMissed || x.Missed);

                    if (pageInfo.NoPaging)
                    {
                        var notifications = await query.OrderBy(x => x.Item.TimeStamp).ToListAsync();

                        notifications.ForEach(x => x.Item.Missed = x.Missed);

                        return new ItemsResult<Notification>(notifications.Select(n => n.Item), notifications.Count);
                    }

                    var notificationsPaged = await query
                        .OrderByDescending(x => x.Item.TimeStamp)
                        .Skip((pageInfo.Page - 1) * pageInfo.PageSize).Take(pageInfo.PageSize).Select(x => new
                        {
                            Notification = x,
                            Total = query.Count()
                        }).OrderBy(x => x.Notification.Item.TimeStamp).ToListAsync();

                    notificationsPaged.ForEach(x => x.Notification.Item.Missed = x.Notification.Missed);

                    return new ItemsResult<Notification>(notificationsPaged.Select(x => x.Notification.Item), notificationsPaged.FirstOrDefault()?.Total ?? 0);
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving notifications for user {0}", e);
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<Notification> Get(string id, bool includeUserData = false)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var query = context.Set<Notification>()
                        .Where(x => x.Id == id);

                    if (includeUserData)
                        query = query.Include(x => x.MissedNotifications);

                    return await query.FirstOrDefaultAsync();
                }
                catch (Exception e)
                {
                    _logger.Error("Error retrieving notification {0} {1}", id, e);
                    _logger.Error(e);
                }
            }

            return null;
        }
    }
}
