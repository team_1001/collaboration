﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using DataAccessLayer.UnitOfWork;
using DataLayer.Entity;
using Models;
using Models.Exceptions;
using Models.Paging;

namespace DataAccessLayer.Repositories
{
    public class InvitationsRepository
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;

        public InvitationsRepository(ILogger logger, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<ItemsResult<Invite>> GetIncoming(
            int userId, 
            PageInfo pageInfo, 
            ProposalState state = ProposalState.Pending)
        {
            return await GetInvitations(x => x.InvitedUserId == userId && x.State == state, pageInfo);
        }

        public async Task<ItemsResult<Invite>> GetOutgoing(int userId, PageInfo pageInfo, ProposalState state = ProposalState.Pending)
        {
            return await GetInvitations(x => x.UserId == userId && x.State == state, pageInfo);
        }

        public async Task<ItemsResult<Invite>> GetIncoming(int eventId, int userId, PageInfo pageInfo, ProposalState state = ProposalState.Pending)
        {
            return await GetInvitations(x => x.EventId == eventId && x.InvitedUserId == userId && x.State == state, pageInfo);
        }

        public async Task<ItemsResult<Invite>> GetOutgoing(int eventId, int userId, PageInfo pageInfo, ProposalState state = ProposalState.Pending)
        {
            return await GetInvitations(x => x.EventId == eventId && x.UserId == userId && x.State == state, pageInfo);
        }

        public async Task<IList<int>> Create(int userId, IEnumerable<int> userIds, int eventId)
        {
            List<int> invitedUsersIds = new List<int>();

            using (var context = _unitOfWork.Create())
            {
                try
                {
                    if (userIds.Any(x => x == userId))
                        throw new EventException("Host can't be invited");

                    var eventSet = context.Set<Event>();
                    var existing =
                        await eventSet
                            .Where(x => x.Id == eventId)
                            .Select(x => new
                            {
                                Event = x,
                                Dialog = x.Dialog,
                                UserIds = x.AllUsers.Where(u => u.Role != EventRoles.Host).Select(u => u.UserId),
                                HostId = x.AllUsers.Where(u => u.Role == EventRoles.Host).Select(u => u.UserId).FirstOrDefault()
                            }).FirstOrDefaultAsync();

                    if (existing == null)
                        throw new EventException($"Event {eventId} doesn't exist");

                    if (existing.Event.EventType == EventType.Private && existing.HostId != userId)
                        throw new EventException(
                            $"User {userId} is not a host of the event {eventId}. Can't apply changes");

                    var intersection = userIds.Intersect(existing.UserIds).ToList();
                    if (intersection.Count > 0)
                        throw new UserException($"Users { string.Join(", ", intersection) } already participate in event {eventId}");

                    var users = await context.Set<User>().Join(userIds, x => x.Id, x => x, (x, y) => x).ToListAsync();
                    if (users.Count < userIds.Count())
                        throw new UserException($"Users { string.Join(", ", userIds.Except(users.Select(u => u.Id))) } don't exist");

                    var invitations =
                        await
                            context.Set<Invite>()
                            .Where(i => i.UserId == userId && i.EventId == eventId)
                            .Join(userIds, x => x.InvitedUserId, x => x, (x, y) => x)
                            .ToListAsync();

                    var pendingInvitations = invitations.Where(i => i.State == ProposalState.Pending).ToList();
                    if (pendingInvitations.Count > 0)
                        throw new InvitationException($"Users { string.Join(", ", pendingInvitations.Select(i => i.InvitedUserId)) } already invited to event {eventId}");

                    var oldInvitations = invitations.Except(pendingInvitations).ToList();

                    var requests =
                        await
                            context.Set<Request>()
                                .Where(i => i.EventId == eventId && i.State == ProposalState.Pending)
                                .Join(userIds, x => x.UserId, x => x, (x, y) => x)
                                .Select(r => new
                                {
                                    Request = r,
                                    User = r.User,
                                    UserDialogs = r.User.Dialogs.Select(d => d.Id)
                                })
                                .ToListAsync();

                    foreach (var request in requests)
                    {
                        request.Request.State = ProposalState.Accepted;
                        context.Attach(request.Request);

                        if (!request.UserDialogs.Contains(existing.Event.DialogId))
                            request.User.Dialogs.Add(existing.Dialog);

                        UserToEvent userToEvent = new UserToEvent()
                        {
                            Event = existing.Event,
                            User = request.User,
                            Role = EventRoles.User
                        };

                        context.Add(userToEvent);
                    }

                    var newInvitationsIds = userIds.Except(requests.Select(r => r.Request.UserId))
                        .Except(oldInvitations.Select(i => i.InvitedUserId));

                    context.AddRange(newInvitationsIds.Select(invitedUser => new Invite()
                    {
                        UserId = userId,
                        InvitedUserId = invitedUser,
                        EventId = eventId,
                        State = ProposalState.Pending,
                        TimeStamp = DateTime.UtcNow
                    }));

                    foreach (var oldInvitation in oldInvitations)
                    {
                        oldInvitation.State = ProposalState.Pending;
                        oldInvitation.TimeStamp = DateTime.UtcNow;
                        context.Attach(oldInvitation);
                    }

                    invitedUsersIds.AddRange(newInvitationsIds.Union(oldInvitations.Select(i => i.InvitedUserId)));
                    await context.CommitAsync();
                }
                catch (InvitationException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (EventException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (UserException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding event to db", e);
                    _logger.Error(e);
                }
            }

            return invitedUsersIds;
        }

        public async Task<bool> Accept(Invite invite)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var fromDb = await context.Set<Invite>()
                        .Where(i =>
                            i.InvitedUserId == invite.InvitedUserId
                            && i.EventId == invite.EventId)
                        .Select(i => new
                        {
                            Invite = i,
                            Dialog = i.Event.Dialog,
                            InvitedUser = i.InvitedUser,
                            UserDialogs = i.InvitedUser.Dialogs.Select(d => d.Id)
                        })
                        .FirstOrDefaultAsync();

                    if (fromDb == null)
                        throw new InvitationException($"Invitation to event {invite.EventId} for user {invite.InvitedUserId} doesn't exist");

                    if (fromDb.Invite.State != ProposalState.Pending)
                        return true;

                    fromDb.Invite.State = ProposalState.Accepted;
                    if (!fromDb.UserDialogs.Contains(fromDb.Dialog.Id))
                        fromDb.InvitedUser.Dialogs.Add(fromDb.Dialog);

                    UserToEvent userToEvent = new UserToEvent()
                    {
                        EventId = fromDb.Invite.EventId,
                        User = fromDb.InvitedUser,
                        Role = EventRoles.User
                    };

                    context.Add(userToEvent);

                    result = await context.CommitAsync();
                }
                catch (InvitationException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding event to db", e);
                    _logger.Error(e);
                }
            }

            return result;
        }

        public async Task<bool> Decline(Invite invite)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var fromDb = await context.FindAsync<Invite>(invite.EventId, invite.InvitedUserId);
                    if (fromDb == null)
                        throw new InvitationException($"Invitation to event {invite.EventId} for user {invite.InvitedUserId} doesn't exist");

                    if (fromDb.State != ProposalState.Pending)
                        return true;

                    fromDb.State = ProposalState.Declined;
                    return await context.Attach(fromDb).CommitAsync();
                }
                catch (InvitationException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding event to db", e);
                    _logger.Error(e);
                }
            }

            return false;
        }

        private async Task<ItemsResult<Invite>> GetInvitations(Expression<Func<Invite, bool>> selector, PageInfo pageInfo)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var query = context.Set<Invite>()
                        .Where(x => pageInfo.TimeStamp == null || x.TimeStamp > pageInfo.TimeStamp)
                        .Where(selector);

                    if (pageInfo.NoPaging)
                    {
                        return new ItemsResult<Invite>(await query.ToListAsync());
                    }

                    var temp = await query.Select(x => new
                        {
                            Item = x,
                            Total = query.Count()
                        })
                        .OrderBy(x => x.Item.TimeStamp)
                        .Skip((pageInfo.Page - 1) * pageInfo.PageSize)
                        .Take(pageInfo.PageSize)
                        .ToListAsync();

                    return new ItemsResult<Invite>(temp.Select(x => x.Item), temp.FirstOrDefault()?.Total ?? 0);
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                    return null;
                }
            }
        }
    }
}
