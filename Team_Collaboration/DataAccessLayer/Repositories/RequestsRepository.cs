﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using DataAccessLayer.UnitOfWork;
using DataLayer.Entity;
using Models;
using Models.Exceptions;
using Models.Paging;

namespace DataAccessLayer.Repositories
{
    public class RequestsRepository
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;

        public RequestsRepository(ILogger logger, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<ItemsResult<Request>> GetIncoming(int eventId, PageInfo pageInfo, ProposalState state = ProposalState.Pending)
        {
            return await GetRequests(x => x.EventId == eventId && x.State == state, pageInfo);
        }

        public async Task<ItemsResult<Request>> GetIncomingByUser(int hostId, PageInfo pageInfo, ProposalState state = ProposalState.Pending)
        {
            return await GetRequests(x => x.Event.AllUsers.Any(u => u.UserId == hostId && u.Role == EventRoles.Host) && x.State == state, pageInfo);
        }

        public async Task<ItemsResult<Request>> GetOutgoing(int userId, PageInfo pageInfo, ProposalState state = ProposalState.Pending)
        {
            return await GetRequests(x => x.UserId == userId && x.State == state, pageInfo);
        }
        
        public async Task<CreatedRequestModel> Create(int eventId, int userId)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var eventSet = context.Set<Event>();
                    var existing =
                        await eventSet
                            .Where(x => x.Id == eventId)
                            .Select(x => new
                            {
                                Event = x,
                                Dialog = x.Dialog,
                                UserIds = x.AllUsers.Where(u => u.Role != EventRoles.Host).Select(u => u.UserId),
                                HostId = x.AllUsers.Where(u => u.Role == EventRoles.Host).Select(u => u.UserId).FirstOrDefault()
                            }).FirstOrDefaultAsync();

                    if (existing == null)
                        throw new EventException($"Event {eventId} doesn't exist");

                    if (existing.HostId == userId)
                        throw new EventException("Host can't request his event");

                    if (existing.UserIds.Contains(userId))
                        throw new UserException($"User {userId} already participates in event {eventId}");

                    var user = await context.FindAsync<User>(userId);
                    if (user == null)
                        throw new UserException($"User {userId} doesn't exist");

                    var request = await context.FindAsync<Request>(eventId, userId);

                    if (request != null)
                    {
                        if (request.State == ProposalState.Pending)
                            throw new RequestException($"User {userId} already requested event {eventId}");
                    }

                    var invitation =
                        await
                            context.Set<Invite>()
                                .Where(i => i.State == ProposalState.Pending && i.EventId == eventId && i.InvitedUserId == userId)
                                .Select(i => new { Invite = i, InvitedUser = i.InvitedUser, UserDialogs = i.InvitedUser.Dialogs.Select(d => d.Id) })
                                .FirstOrDefaultAsync();

                    if (invitation != null)
                    {
                        invitation.Invite.State = ProposalState.Accepted;
                        context.Attach(invitation.Invite);

                        if (!invitation.UserDialogs.Contains(existing.Event.DialogId))
                            invitation.InvitedUser.Dialogs.Add(existing.Dialog);

                        UserToEvent userToEvent = new UserToEvent()
                        {
                            Event = existing.Event,
                            User = invitation.InvitedUser,
                            Role = EventRoles.User
                        };

                        context.Add(userToEvent);
                    }
                    else
                    {
                        if (request != null)
                        {
                            request.State = ProposalState.Pending;
                            request.TimeStamp = DateTime.UtcNow;
                            context.Attach(request);
                        }
                        else
                        {
                            context.Add(new Request()
                            {
                                UserId = userId,
                                EventId = eventId,
                                TimeStamp = DateTime.UtcNow,
                                State = ProposalState.Pending
                            });
                        }
                    }
                    
                    await context.CommitAsync();

                    return new CreatedRequestModel()
                    {
                        EventId = eventId,
                        UserId = userId,
                        UserIds = new [] { existing.HostId }
                    };
                }
                catch (RequestException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (EventException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (UserException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding event to db", e);
                    _logger.Error(e);
                }
            }

            return null;
        }

        public async Task<bool> Accept(Request request)
        {
            bool result = false;
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var fromDb = await context.Set<Request>()
                        .Where(i =>
                            i.UserId == request.UserId
                            && i.EventId == request.EventId)
                        .Select(r => new {Request = r, User = r.User, Dialog = r.Event.Dialog, UserDialogs = r.User.Dialogs.Select(d => d.Id)})
                        .FirstOrDefaultAsync();

                    if (fromDb == null)
                        throw new RequestException($"Request to event {request.EventId} for user {request.UserId} doesn't exist");

                    if (fromDb.Request.State != ProposalState.Pending)
                        return true;

                    fromDb.Request.State = ProposalState.Accepted;
                    if (!fromDb.UserDialogs.Contains(fromDb.Dialog.Id))
                        fromDb.User.Dialogs.Add(fromDb.Dialog);

                    UserToEvent userToEvent = new UserToEvent()
                    {
                        EventId = fromDb.Request.EventId,
                        User = fromDb.User,
                        Role = EventRoles.User
                    };

                    result = await context.Add(userToEvent).CommitAsync();
                }
                catch (RequestException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding event to db", e);
                    _logger.Error(e);
                }
            }

            return result;
        }

        public async Task<bool> Decline(Request request)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var fromDb = await context.FindAsync<Request>(request.EventId, request.UserId);
                    if (fromDb == null)
                        throw new RequestException($"Request to event {request.EventId} for user {request.UserId} doesn't exist");

                    if (fromDb.State != ProposalState.Pending)
                        return true;

                    fromDb.State = ProposalState.Declined;
                    return await context.Attach(fromDb).CommitAsync();
                }
                catch (RequestException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding event to db", e);
                    _logger.Error(e);
                }
            }

            return false;
        }

        public async Task<bool> Update(Request request)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var fromDb = await context.FindAsync<Request>(request.EventId, request.UserId);
                    if (fromDb == null)
                        throw new RequestException($"Request to event {request.EventId} for user {request.UserId} doesn't exist");

                    if (fromDb.State == request.State)
                        return true;

                    return await context.Detach(fromDb).Update(request).CommitAsync();
                }
                catch (RequestException e)
                {
                    _logger.Error(e);
                    throw;
                }
                catch (Exception e)
                {
                    _logger.Error("Error adding event to db", e);
                    _logger.Error(e);
                }
            }

            return false;
        }
        private async Task<ItemsResult<Request>> GetRequests(Expression<Func<Request, bool>> selector, PageInfo pageInfo)
        {
            using (var context = _unitOfWork.Create())
            {
                try
                {
                    var query = context.Set<Request>()
                        .Where(x => pageInfo.TimeStamp == null || x.TimeStamp > pageInfo.TimeStamp)
                        .Where(selector);

                    if (pageInfo.NoPaging)
                    {
                        return new ItemsResult<Request>(await query.ToListAsync());
                    }

                    var temp = await query.Select(x => new
                        {
                            Item = x,
                            Total = query.Count()
                        })
                        .OrderBy(x => x.Item.TimeStamp)
                        .Skip((pageInfo.Page - 1) * pageInfo.PageSize)
                        .Take(pageInfo.PageSize)
                        .ToListAsync();

                    return new ItemsResult<Request>(temp.Select(x => x.Item), temp.FirstOrDefault()?.Total ?? 0);
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                    return null;
                }
            }
        }
    }
}
