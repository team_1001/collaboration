﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServer;
using Autofac.Extras.NLog;

namespace DataAccessLayer.Configuration
{
    public class Migrator
    {
        private readonly ILogger _logger;

        public Migrator(ILogger logger)
        {
            _logger = logger;
        }

        public void Migrate(string connectionString)
        {
            try
            {
                var authConfig = new AuthMigrations.Configuration
                {
                    TargetDatabase = new DbConnectionInfo(connectionString, SqlProviderServices.ProviderInvariantName)
                };

                var migrator = new DbMigrator(authConfig);
                migrator.Update();

                var mainConfig = new DataMigrations.Configuration
                {
                    TargetDatabase = new DbConnectionInfo(connectionString, SqlProviderServices.ProviderInvariantName)
                };

                migrator = new DbMigrator(mainConfig);
                migrator.Update();
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
    }
}
