﻿using System.Data.Entity;

namespace DataAccessLayer.Configuration
{
    public class ActivityContextConfiguration : DbConfiguration
    {
        public ActivityContextConfiguration()
        {
            SetDatabaseInitializer(new MigrateDatabaseToLatestVersion<ActivityContext, DataMigrations.Configuration>());
        }
    }
}
