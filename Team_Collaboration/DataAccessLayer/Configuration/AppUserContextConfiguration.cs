﻿using System.Data.Entity;

namespace DataAccessLayer.Configuration
{
    public class AppUserContextConfiguration : DbConfiguration
    {
        public AppUserContextConfiguration()
        {
            SetDatabaseInitializer(new MigrateDatabaseToLatestVersion<AppUserDbContext, AuthMigrations.Configuration>());
        }
    }
}
