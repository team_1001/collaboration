﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using AuthLayer;
using AuthLayer.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAccessLayer.Identity
{
    public class ApplicationUserManager : UserManager<AppUser, int>, IApplicationUserManager
    {
        private readonly UserStore<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim> _store;

        public ApplicationUserManager(UserStore<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim> store) : base(store)
        {
            _store = store;
        }

        public async Task<AppClient> FindClientAsync(string clientId)
        {
            return await _store.Context.Set<AppClient>().FindAsync(clientId);
        }

        public async Task<bool> AddRefreshTokenAsync(RefreshToken token)
        {
            var existingToken = await _store.Context.Set<RefreshToken>()
                .SingleOrDefaultAsync(r => r.Subject == token.Subject && r.ClientId == token.ClientId);

            if (existingToken != null)
            {
                await RemoveRefreshTokenAsync(existingToken);
            }

            _store.Context.Set<RefreshToken>().Add(token);
            return await _store.Context.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshTokenAsync(RefreshToken refreshToken)
        {
            _store.Context.Set<RefreshToken>().Remove(refreshToken);
            return await _store.Context.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshTokenAsync(string refreshTokenId)
        {
            var token = await _store.Context.Set<RefreshToken>().FindAsync(refreshTokenId);

            if (token != null)
            {
                _store.Context.Set<RefreshToken>().Remove(token);
                return await _store.Context.SaveChangesAsync() > 0;
            }

            return false;
        }

        public async Task<RefreshToken> FindRefreshTokenAsync(string refreshTokenId)
        {
            return await _store.Context.Set<RefreshToken>().FindAsync(refreshTokenId);
        }

        public async Task<List<RefreshToken>> GetAllRefreshTokensAsync()
        {
            return await _store.Context.Set<RefreshToken>().ToListAsync();
        }
    }
}
