using System;
using System.Data.Entity.Infrastructure;
using AuthLayer;
using AuthLayer.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAccessLayer.Identity
{
    public class ApplicationUserManagerFactory : IApplicationUserManagerFactory
    {
        private readonly IDbContextFactory<AppUserDbContext> _userContextFactory;
        //private readonly IDataProtectionProvider _dataProtectionProvider;
        private ApplicationUserManager _userManager;

        public ApplicationUserManagerFactory(
            IDbContextFactory<AppUserDbContext> userContextFactory)//,
            //IDataProtectionProvider dataProtectionProvider)
        {
            _userContextFactory = userContextFactory;
            //_dataProtectionProvider = dataProtectionProvider;
        }

        public IApplicationUserManager Create()
        {
            //if (_userManager != null)
            //    return _userManager;

            var manager = new ApplicationUserManager(
                new UserStore<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>(
                    _userContextFactory.Create()));

            //var manager = new ApplicationUserManager(
            //    new UserStore<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>(
            //    _userContextFactory.Create()));

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<AppUser, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<AppUser, int>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<AppUser, int>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });

            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();

            //var dataProtectionProvider = _dataProtectionProvider;
            //if (dataProtectionProvider != null)
            //{
            //    manager.UserTokenProvider =
            //        new DataProtectorTokenProvider<AppUser, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            //}

            return manager;
            //return _userManager = manager;
        }
    }
}