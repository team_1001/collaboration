﻿using System.Security.Claims;
using System.Threading.Tasks;
using AuthLayer;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace DataAccessLayer.Identity
{
    public class ApplicationSignInManager : SignInManager<AppUser, int>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager) :
            base(userManager, authenticationManager)
        { }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(AppUser user)
        {
            return user.GenerateUserIdentityAsync(UserManager);
        }
    }
}
