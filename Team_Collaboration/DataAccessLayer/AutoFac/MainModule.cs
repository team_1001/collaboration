﻿using System.Data.Entity.Infrastructure;
using AuthLayer.Interfaces;
using Autofac;
using Autofac.Core;
using DataAccessLayer.Configuration;
using DataAccessLayer.Identity;
using DataAccessLayer.Repositories;
using DataAccessLayer.UnitOfWork;

namespace DataAccessLayer.AutoFac
{
    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Migrator>().AsSelf();

            builder.RegisterAdapter<IApplicationUserManagerFactory, IApplicationUserManager>(x => x.Create());

            builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerLifetimeScope();

            builder.RegisterType<ApplicationUserManagerFactory>()
                .WithParameter(new ResolvedParameter(
                    (p, ctx) => p.Name == "userContextFactory",
                    (p, ctx) => ctx.Resolve<AppUserDbContextFactory>()))
                .As<IApplicationUserManagerFactory>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EntityUnitOfWork<ActivityContext>>().As<IUnitOfWork>();

            builder.RegisterType<MessagesRepository>().AsSelf();
            builder.RegisterType<UsersRepository>().AsSelf();
            builder.RegisterType<EventsRepository>().AsSelf();
            builder.RegisterType<InvitationsRepository>().AsSelf();
            builder.RegisterType<RequestsRepository>().AsSelf();
            builder.RegisterType<NotificationsRepository>().AsSelf();

            builder.RegisterType<ActivityContextFactory>().As<IDbContextFactory<ActivityContext>>();

            builder.RegisterType<AppUserDbContextFactory>()
                .As<IDbContextFactory<AppUserDbContext>>()
                .AsSelf()
                .InstancePerLifetimeScope();
        }
    }
}
