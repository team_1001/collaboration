﻿namespace Team_Collaboration.Tests
{
   public class TestConfig
    {
       public static string SqlExpressConnectionString => @"Data Source=.\SQLEXPRESS;Initial Catalog=TestDb;Integrated Security=True";
       public static string LocalDbConnectionString => @"Data Source=(localdb)\v11.0;Initial Catalog=TestDb;Integrated Security=True";
    }
}
