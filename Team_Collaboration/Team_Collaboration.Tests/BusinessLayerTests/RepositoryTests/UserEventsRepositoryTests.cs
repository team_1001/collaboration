﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BusinessLayer.Models;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;
using FluentAssertions;
using FluentAssertions.Execution;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace Team_Collaboration.Tests.BusinessLayerTests.RepositoryTests
{
    [TestFixture]
    public  class UserEventsRepositoryTests
    {
        //private readonly Mock<IQueryBuilderFactory> _queryFactory;
        //private readonly UserEventsRepository _userEventsRepository;
        //private readonly List<IEventRetriever> _eventRetrievers; 
        //private readonly Fixture _fixture;

        //public UserEventsRepositoryTests()
        //{
        //    _fixture = new Fixture();
        //    _queryFactory = new Mock<IQueryBuilderFactory>();
        //    _eventRetrievers= new List<IEventRetriever>();
        //    _userEventsRepository = new UserEventsRepository(_queryFactory.Object, _eventRetrievers);
        //}

        //[Test]
        //public void GetAllEventsOfUser_GettingEventsOfUser_ShouldBeEquivalent()
        //{
        //    //arrange
        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();
        //    var queryBuilderEvent = new Mock<IQueryBuilder<UserToEvent>>();

        //    var events = _fixture.Build<UserToEvent>().Without(p=>p.Event).Without(p=>p.User).CreateMany().ToList();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);
        //    _queryFactory.Setup(x => x.Set<UserToEvent>()).Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.Setup(x => x.SelectMany(It.IsAny<Expression<Func<User, IEnumerable<UserToEvent>>>>()))
        //        .Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderEvent.Setup(m => m.Exec()).Returns(events);

        //    //act
        //    var result = _userEventsRepository.GetEvents(1);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderUser.Verify(
        //            m =>
        //                m.Where(
        //                    It.Is<Expression<Func<User, bool>>>(
        //                        expression => expression.Compile()(new User() {Id = 1}))));

        //        queryBuilderUser.Verify(
        //            m =>
        //                m.SelectMany(
        //                    It.Is<Expression<Func<User, IEnumerable<UserToEvent>>>>(
        //                        expression => expression.Compile()(new User() {Id = 1,Events = events}).SequenceEqual(events))));

        //         result.ShouldBeEquivalentTo(events);
        //    }
        //}

        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "not implemented handler")]
        //public void GetEvents_ThrowException_HandlerNotFound()
        //{
        //    //arrange

        //    //act
        //    _userEventsRepository.GetEvents(new QueryEventParameters());

        //    //assert
        //}


        //[Test]
        //public void AddUserToEvent_DoesNotThrow_UserToEventAdded()
        //{
        //    //arrange

        //    var user = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    var @event = _fixture.Build<Event>()
        //        .Without(p=>p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    var userToEvent = new UserToEvent() { EventId = @event.Id, UserId = user.Id, Role = "somerole" };
        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();
        //    var queryBuilderUserToEvent = new Mock<IQueryBuilder<UserToEvent>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);
        //    _queryFactory.Setup(x => x.Set<Event>()).Returns(queryBuilderEvent.Object);
        //    _queryFactory.Setup(x => x.Set<UserToEvent>()).Returns(queryBuilderUserToEvent.Object);

        //    queryBuilderUserToEvent.Setup(x => x.Add(It.IsAny<UserToEvent>())).Returns(queryBuilderUserToEvent.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);


        //    queryBuilderUser.Setup(m => m.ExecOne()).Returns(user);
        //    queryBuilderEvent.Setup(m => m.ExecOne()).Returns(@event);

        //    //act
        //    _userEventsRepository.AddUserToEvent(user.Id,@event.Id,"somerole");

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderUserToEvent.Verify(m=>m.Add(It.Is<UserToEvent>(p=>p.EventId == userToEvent.EventId 
        //            && p.UserId == userToEvent.UserId
        //            && p.Role == userToEvent.Role)));

        //        queryBuilderUserToEvent.Verify(m => m.SaveChanges());
        //    }
        //}

        //[Test]
        //public void AddUserToEvent_UserNotFound_ThrowException()
        //{
        //    //arrange
        //    User user = null;

        //    var @event = _fixture.Build<Event>()
        //        .Without(p=>p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    var userToEvent = new UserToEvent() { EventId = @event.Id, Role = "somerole" };

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);
        //    _queryFactory.Setup(x => x.Set<Event>()).Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.Setup(m => m.ExecOne()).Returns(user);
        //    queryBuilderEvent.Setup(m => m.ExecOne()).Returns(@event);

        //    //act
        //    TestDelegate actionNull =  () => _userEventsRepository.AddUserToEvent(1, @event.Id, "somerole");

        //    //assert
        //    Assert.Throws<Exception>(actionNull,"user is null");
        //}

        //[Test]
        //public void AddUserToEvent_EventNotFound_ThrowException()
        //{
        //    //arrange

        //    var user = _fixture.Build<User>()
        //       .Without(p => p.Friends)
        //       .Without(p => p.Events)
        //       .Without(p => p.IncomingInvites)
        //       .Without(p => p.OutgoingInvites)
        //       .Without(p => p.Requests)
        //       .With(p => p.Id, 1).Create<User>();

        //    Event @event = null;

        //    var userToEvent = new UserToEvent() { UserId = user.Id, Role = "somerole" };

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);
        //    _queryFactory.Setup(x => x.Set<Event>()).Returns(queryBuilderEvent.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.Setup(m => m.ExecOne()).Returns(user);
        //    queryBuilderEvent.Setup(m => m.ExecOne()).Returns(@event);

        //    //act
        //    TestDelegate actionNull = () => _userEventsRepository.AddUserToEvent(user.Id, 1, "somerole");

        //    //assert
        //    Assert.Throws<Exception>(actionNull, "event is null");
        //}

        //[Test]
        //public void RemoveUserFromEvent_UserNotFound_ThrowException()
        //{
        //    //arrange
        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Include(It.IsAny<Expression<Func<User, ICollection<UserToEvent>>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    User user = null;
        //    queryBuilderUser.Setup(m => m.ExecOne()).Returns(user);

        //    //act
        //    TestDelegate actionNull = () => _userEventsRepository.RemoveUserFromEvent(1, 1);

        //    //assert
        //    Assert.Throws<Exception>(actionNull, "user is null");
        //}

        //[Test]
        //public void RemoveUserFromEvent_UserToEventNotFound_ThrowException()
        //{
        //    //arrange
        //    var user = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .With(p => p.Events, new List<UserToEvent>())
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Include(It.IsAny<Expression<Func<User, ICollection<UserToEvent>>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(m => m.ExecOne()).Returns(user);

        //    //act
        //    TestDelegate actionNull = () => _userEventsRepository.RemoveUserFromEvent(user.Id, 1);

        //    //assert
        //    Assert.Throws<Exception>(actionNull, "event is null");
        //}

        //[Test]
        //public void RemoveUserFromEvent_UserIsHost_ThrowException()
        //{
        //    //arrange
        //    var @event = _fixture.Build<UserToEvent>()
        //        .With(p => p.UserId, 1)
        //        .With(p => p.EventId, 1)
        //        .With(p => p.Role, "host")
        //        .Without(p => p.Event)
        //        .Without(p => p.User)
        //        .Create<UserToEvent>();

        //    var user = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .With(p => p.Events, new List<UserToEvent>() { @event })
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Include(It.IsAny<Expression<Func<User, ICollection<UserToEvent>>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(m => m.ExecOne()).Returns(user);


        //    //act
        //    TestDelegate actionThrow = () => _userEventsRepository.RemoveUserFromEvent(user.Id, @event.EventId);

        //    //assert
        //    Assert.Throws<Exception>(actionThrow, "this user is owner of event!");
        //}

        //[Test]
        //public void RemoveUserFromEvent_DoesNotThrow_UserFromEventRemoved()
        //{
        //    //arrange
        //    var userToEvent = _fixture.Build<UserToEvent>()
        //        .With(p => p.UserId, 1)
        //        .With(p => p.EventId, 1)
        //        .With(p => p.Role, "role")
        //        .Without(p => p.Event)
        //        .Without(p => p.User)
        //        .Create<UserToEvent>();

        //    var user = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .With(p => p.Events, new List<UserToEvent>() { userToEvent })
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Include(It.IsAny<Expression<Func<User, ICollection<UserToEvent>>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Attach(It.IsAny<User>())).Returns(queryBuilderUser.Object);
        //    queryBuilderUser.Setup(m => m.ExecOne()).Returns(user);

        //    //act
        //    _userEventsRepository.RemoveUserFromEvent(user.Id, userToEvent.EventId);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        user.Events.Should().HaveCount(0);
        //        queryBuilderUser.Verify(m => m.Attach(It.Is<User>(p=>p.Id==user.Id)));
        //        queryBuilderUser.Verify(m => m.SaveChanges());
        //    }
        //}


        //[Test]
        //[ExpectedException(typeof(Exception),ExpectedMessage = "user is null")]
        //public void InviteFriendToEvent_UserNotFound_ThrowExecption()
        //{
        //   //assert
        //    User user = null;

        //    var friend = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 2).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);



        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);


        //    queryBuilderUser.SetupSequence(m => m.ExecOne()).Returns(user).Returns(friend);


        //    //act
        //    _userEventsRepository.InviteFriendToEvent(1, friend.Id,1);

        //    //assert
        //}

        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "friend is null")]
        //public void InviteFriendToEvent_FriendNotFound_ThrowExecption()
        //{
        //    //assert
        //    User user = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    User friend = null;


        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.SetupSequence(m => m.ExecOne()).Returns(user).Returns(friend);


        //    //act
        //    _userEventsRepository.InviteFriendToEvent(user.Id, 2, 1);

        //    //assert
        //}

        //[Test]
        //public void InviteFriendToEvent_DoesNotThrow_FriendToEventInvited()
        //{
        //    //assert
        //    var @event = _fixture.Build<Event>()
        //        .Without(p=>p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    User user = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    User friend = _fixture.Build<User>()
        //       .Without(p => p.Friends)
        //       .Without(p => p.Events)
        //       .Without(p => p.IncomingInvites)
        //       .Without(p => p.OutgoingInvites)
        //       .Without(p => p.Requests)
        //       .With(p => p.Id, 2).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();
        //    var queryBuilderInvite = new Mock<IQueryBuilder<Invite>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);
        //    _queryFactory.Setup(x => x.Set<Invite>()).Returns(queryBuilderInvite.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);


        //    queryBuilderInvite.Setup(x => x.Add(It.IsAny<Invite>()))
        //        .Returns(queryBuilderInvite.Object);

        //    queryBuilderUser.SetupSequence(m => m.ExecOne()).Returns(user).Returns(friend);


        //    //act
        //    _userEventsRepository.InviteFriendToEvent(user.Id, friend.Id, @event.Id);

        //    //assert
        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderInvite.Verify(m => m.Add(It.Is<Invite>(p => p.EventId == @event.Id
        //            && p.HostUserId == user.Id
        //            && p.InvitedUserId == friend.Id)));

        //        queryBuilderInvite.Verify(m => m.SaveChanges());
        //    }
        //}


        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "user not found")]
        //public void CreateRequest_UserNotFound_ThrowExecption()
        //{
        //    //assert
        //    User user = null;

        //    var @event = _fixture.Build<Event>()
        //       .Without(p => p.Category)
        //       .Without(p => p.Location)
        //       .Without(p => p.AllUsers)
        //       .Without(p => p.Invites)
        //       .Without(p => p.Requests)
        //       .With(p => p.Id, 1).Create<Event>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);
        //    _queryFactory.Setup(x => x.Set<Event>()).Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //       .Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.SetupSequence(m => m.ExecOne()).Returns(user);
        //    queryBuilderEvent.SetupSequence(m => m.ExecOne()).Returns(@event);


        //    //act
        //    _userEventsRepository.CreateRequestToEvent(1,@event.Id);

        //    //assert
        //}

        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "event not found")]
        //public void CreateRequest_EventNotFound_ThrowExecption()
        //{
        //    //assert
        //    User user = _fixture.Build<User>()
        //       .Without(p => p.Friends)
        //       .Without(p => p.Events)
        //       .Without(p => p.IncomingInvites)
        //       .Without(p => p.OutgoingInvites)
        //       .Without(p => p.Requests)
        //       .With(p => p.Id, 1).Create<User>();

        //    Event @event = null;

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);
        //    _queryFactory.Setup(x => x.Set<Event>()).Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //       .Returns(queryBuilderEvent.Object);

        //    queryBuilderUser.SetupSequence(m => m.ExecOne()).Returns(user);
        //    queryBuilderEvent.SetupSequence(m => m.ExecOne()).Returns(@event);


        //    //act
        //    _userEventsRepository.CreateRequestToEvent(user.Id, 1);

        //    //assert
        //}

        //[Test]
        //public void CreateRequest_DoesNotThrow_RequestCreated()
        //{
        //    //assert
        //    var @event = _fixture.Build<Event>()
        //        .Without(p => p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    User user = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();
        //    var queryBuilderRequest = new Mock<IQueryBuilder<Request>>();

        //    _queryFactory.Setup(x => x.Set<Event>()).Returns(queryBuilderEvent.Object);
        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);
        //    _queryFactory.Setup(x => x.Set<Request>()).Returns(queryBuilderRequest.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);
        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //     .Returns(queryBuilderEvent.Object);


        //    queryBuilderRequest.Setup(x => x.Add(It.IsAny<Request>()))
        //        .Returns(queryBuilderRequest.Object);

        //    queryBuilderUser.SetupSequence(m => m.ExecOne()).Returns(user);
        //    queryBuilderEvent.SetupSequence(m => m.ExecOne()).Returns(@event);

        //    //act
        //    _userEventsRepository.CreateRequestToEvent(user.Id, @event.Id);

        //    //assert
        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderRequest.Verify(m => m.Add(It.Is<Request>(p => p.EventId == @event.Id
        //            && p.UserId == user.Id)));

        //        queryBuilderRequest.Verify(m => m.SaveChanges());
        //    }
        //}
    }
}
