﻿using NUnit.Framework;

namespace Team_Collaboration.Tests.BusinessLayerTests.RepositoryTests
{
    [TestFixture]
    public class UserFriendsRepositoryTests
    {
        //private readonly Mock<IQueryBuilderFactory> _queryFactory;
        //private readonly UserFriendsRepository _userFriendsRepository;
        //private readonly Fixture _fixture;

        //public UserFriendsRepositoryTests()
        //{
        //    _fixture = new Fixture();
        //    _queryFactory = new Mock<IQueryBuilderFactory>();
        //    _userFriendsRepository = new UserFriendsRepository(_queryFactory.Object);
        //}

        //[Test]
        //[ExpectedException(typeof(Exception),ExpectedMessage = "user is null")]
        //public void AddFriend_UserNotFound_ThrowException()
        //{
        //    //arrange
        //    User user = null;
        //    var friend = _fixture.Build<User>()
        //      .Without(p => p.Friends)
        //      .Without(p => p.Events)
        //      .Without(p => p.IncomingInvites)
        //      .Without(p => p.OutgoingInvites)
        //      .Without(p => p.Requests)
        //      .With(p => p.Id, 1).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.SetupSequence(p => p.ExecOne()).Returns(user).Returns(friend);

        //    //act
        //    _userFriendsRepository.AddFriend(friend.Id, 2);
        //    //assert
        //}

        //[Test]
        //[ExpectedException(typeof (Exception), ExpectedMessage = "friend is null")]
        //public void AddFriend_FriendNotFound_ThrowException()
        //{
        //    //arrange
        //    var user = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    User friend = null;

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);


        //    queryBuilderUser.SetupSequence(p => p.ExecOne()).Returns(user).Returns(friend);

        //    //act
        //    _userFriendsRepository.AddFriend(1, user.Id);
        //    //assert
        //}

        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "user already has this friend")]
        //public void AddFriend_AlreadyFriends_ThrowException()
        //{
        //    //arrange
        //    var friend = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 2).Create<User>();

        //    var user = _fixture.Build<User>()
        //        .With(p => p.Friends,new List<User>() {friend})
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);


        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.SetupSequence(p => p.ExecOne()).Returns(user).Returns(friend);

        //    //act
        //    _userFriendsRepository.AddFriend(friend.Id, user.Id);
        //    //assert
        //}


        //[Test]
        //public void AddFriend_DoesNotThrow_FriendAdded()
        //{
        //    //arrange
        //    var user = _fixture.Build<User>()
        //        .With(p => p.Friends, new List<User>())
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();

        //    var friend = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 2).Create<User>();

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Attach(It.IsAny<User>())).Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.SetupSequence(p => p.ExecOne()).Returns(user).Returns(friend);

        //    //act
        //    _userFriendsRepository.AddFriend(friend.Id, user.Id);
        //    //assert
        //    using (new AssertionScope())
        //    {
        //        user.Friends.Should().HaveCount(1);
        //        user.Friends.Should().Contain(friend);
        //        queryBuilderUser.Verify(m=>m.Attach(It.Is<User>(p=>p.Id == user.Id)));
        //        queryBuilderUser.Verify(m => m.SaveChanges());
        //    }
        //}

        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "user is null")]
        //public void RemoveFriend_UserNotFound_ThrowException()
        //{
        //    //arrange
        //    User user = null;

        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);


        //    queryBuilderUser.Setup(x => x.Include(It.IsAny<Expression<Func<User, ICollection<User>>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(p => p.ExecOne()).Returns(user);

        //    //act
        //    _userFriendsRepository.RemoveFriend(1, 2);
        //    //assert
        //}

        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "friend is null")]
        //public void RemoveFriend_FriendNotFound_ThrowException()
        //{
        //    //arrange
        //    var user = _fixture.Build<User>()
        //        .With(p => p.Friends,new List<User>())
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();


        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Include(It.IsAny<Expression<Func<User, ICollection<User>>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(p => p.ExecOne()).Returns(user);

        //    //act
        //    _userFriendsRepository.RemoveFriend(2, user.Id);
        //    //assert
        //}

        //[Test]
        //public void RemoveFriend_DoesNotThrow_FriendRemoved()
        //{
        //    //arrange
        //    var friend = _fixture.Build<User>()
        //        .Without(p => p.Friends)
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 2).Create<User>();

        //    var user = _fixture.Build<User>()
        //        .With(p => p.Friends, new List<User>() {friend})
        //        .Without(p => p.Events)
        //        .Without(p => p.IncomingInvites)
        //        .Without(p => p.OutgoingInvites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<User>();


        //    var queryBuilderUser = new Mock<IQueryBuilder<User>>();

        //    _queryFactory.Setup(x => x.Set<User>()).Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Attach(It.IsAny<User>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Include(It.IsAny<Expression<Func<User, ICollection<User>>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(x => x.Where(It.IsAny<Expression<Func<User, bool>>>()))
        //        .Returns(queryBuilderUser.Object);

        //    queryBuilderUser.Setup(p => p.ExecOne()).Returns(user);

        //    //act
        //    _userFriendsRepository.RemoveFriend(friend.Id, user.Id);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        user.Friends.Should().HaveCount(0);
        //        queryBuilderUser.Verify(m => m.Attach(It.Is<User>(p => p.Id == user.Id)));
        //        queryBuilderUser.Verify(m => m.SaveChanges());
        //    }
        //}
    }
}
