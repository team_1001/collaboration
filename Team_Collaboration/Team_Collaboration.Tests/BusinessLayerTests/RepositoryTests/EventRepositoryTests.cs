﻿using NUnit.Framework;

namespace Team_Collaboration.Tests.BusinessLayerTests.RepositoryTests
{
    [TestFixture]
    public class EventRepositoryTests
    {
        //private readonly Mock<IQueryBuilderFactory> _queryFactory;
        //private readonly EventRepository _eventRepository;
        //private readonly Fixture _fixture;

        //public EventRepositoryTests()
        //{
        //    _fixture = new Fixture();
        //    _queryFactory = new Mock<IQueryBuilderFactory>();
        //    _eventRepository = new EventRepository(_queryFactory.Object);
        //}

        //[Test]
        //public void GetEvent_EventGetting_EventShouldBeEquivalent()
        //{
        //    //arrange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    var @event = _fixture.Build<Event>()
        //        .Without(p => p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.Setup(m => m.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);

        //    queryBuilderEvent.Setup(m => m.ExecOne()).Returns(@event);

        //    //act
        //    var expectedEvent = _eventRepository.GetEvent(@event.Id);

        //    //assert
        //    expectedEvent.Id.ShouldBeEquivalentTo(@event.Id);
        //}

        //[Test]
        //public void GetAllRequests_GettingRequestsOfEvent_ShouldBeEquivalent()
        //{
        //    //arrange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();
        //    var queryBuilderRequest = new Mock<IQueryBuilder<Request>>();

        //    var requests = _fixture.Build<Request>()
        //        .Without(p => p.User)
        //        .Without(p => p.Event).CreateMany().ToList();

        //    var @event = _fixture.Build<Event>()
        //        .Without(p => p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .With(p => p.Requests, new List<Request>(requests))
        //        .With(p => p.Id, 1).Create<Event>();

        //    _queryFactory.Setup(m => m.Set<Request>()).Returns(queryBuilderRequest.Object);
        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);

        //    queryBuilderEvent.Setup(m => m.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);

        //    queryBuilderEvent.Setup(m => m.SelectMany(It.IsAny<Expression<Func<Event, IEnumerable<Request>>>>()))
        //        .Returns(queryBuilderRequest.Object);

        //    queryBuilderRequest.Setup(m => m.Exec()).Returns(requests);

        //    //act
        //    var result = _eventRepository.GetAllRequests(@event.Id);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderEvent.Verify(
        //            m =>
        //                m.SelectMany(
        //                    It.Is<Expression<Func<Event, IEnumerable<Request>>>>(
        //                        expression =>
        //                            expression.Compile()(@event)
        //                                .SequenceEqual(requests))));
        //        result.ShouldBeEquivalentTo(requests);
        //    }
        //}

        //[Test]
        //public void GetAllInvites_GettingInvitesOfEvent_ShouldBeEquivalent()
        //{
        //    //arrange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();
        //    var queryBuilderInvite = new Mock<IQueryBuilder<Invite>>();

        //    var invites = _fixture.Build<Invite>()
        //        .Without(p => p.HostUser)
        //        .Without(p => p.Event)
        //        .Without(p => p.InvitedUser)
        //        .Without(p => p.Event).CreateMany().ToList();

        //    var @event = _fixture.Build<Event>()
        //        .Without(p => p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .With(p => p.Invites, new List<Invite>(invites))
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    _queryFactory.Setup(m => m.Set<Invite>()).Returns(queryBuilderInvite.Object);
        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);

        //    queryBuilderEvent.Setup(m => m.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);

        //    queryBuilderEvent.Setup(m => m.SelectMany(It.IsAny<Expression<Func<Event, IEnumerable<Invite>>>>()))
        //        .Returns(queryBuilderInvite.Object);

        //    queryBuilderInvite.Setup(m => m.Exec()).Returns(invites);

        //    //act
        //    var result = _eventRepository.GetAllInvites(@event.Id);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderEvent.Verify(
        //            m =>
        //                m.SelectMany(
        //                    It.Is<Expression<Func<Event, IEnumerable<Invite>>>>(
        //                        expression =>
        //                            expression.Compile()(@event)
        //                                .SequenceEqual(invites))));
        //        result.ShouldBeEquivalentTo(invites);
        //    }
        //}

        //[Test]
        //public void GetAllUsers_GettingUsersOfEvent_ShouldBeEquivalent()
        //{
        //    //arrange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();
        //    var queryBuilderUserToEvent = new Mock<IQueryBuilder<UserToEvent>>();

        //    var allUsers = _fixture.Build<UserToEvent>()
        //        .Without(p => p.User)
        //        .Without(p => p.Event).CreateMany().ToList();

        //    var @event = _fixture.Build<Event>()
        //        .Without(p => p.Category)
        //        .Without(p => p.Location)
        //        .With(p => p.AllUsers, new List<UserToEvent>(allUsers))
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    _queryFactory.Setup(m => m.Set<UserToEvent>()).Returns(queryBuilderUserToEvent.Object);
        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);

        //    queryBuilderEvent.Setup(m => m.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);

        //    queryBuilderEvent.Setup(m => m.SelectMany(It.IsAny<Expression<Func<Event, IEnumerable<UserToEvent>>>>()))
        //        .Returns(queryBuilderUserToEvent.Object);

        //    queryBuilderUserToEvent.Setup(m => m.Exec()).Returns(allUsers);

        //    //act
        //    var result = _eventRepository.GetAllUsers(@event.Id);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderEvent.Verify(
        //            m =>
        //                m.SelectMany(
        //                    It.Is<Expression<Func<Event, IEnumerable<UserToEvent>>>>(
        //                        expression =>
        //                            expression.Compile()(@event)
        //                                .SequenceEqual(allUsers))));
        //        result.ShouldBeEquivalentTo(allUsers);
        //    }
        //}

        //[Test]
        //public void CreateEvent_CreatedEvent_ShouldBeSaved()
        //{
        //    //arange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    var @event = _fixture.Build<Event>()
        //        .Without(p => p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.Setup(x => x.Add(It.IsAny<Event>())).Returns(queryBuilderEvent.Object);

        //    //act
        //    _eventRepository.Create(1, @event);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderEvent.Verify(m => m.Add(@event));
        //        queryBuilderEvent.Verify(m => m.SaveChanges());
        //    }
        //}

        //[Test]
        //public void EditEvent_EditedEvent_ShouldBeSaved()
        //{
        //    //arange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    var @event = _fixture.Build<Event>()
        //        .Without(p => p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.Setup(x => x.Attach(It.IsAny<Event>())).Returns(queryBuilderEvent.Object);

        //    //act
        //    _eventRepository.Edit(1, @event);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderEvent.Verify(m => m.Attach(@event));
        //        queryBuilderEvent.Verify(m => m.SaveChanges());
        //    }
        //}

        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "event not found")]
        //public void EditEvent_ThrowException_EventNotFound()
        //{
        //    //arange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    var @event = _fixture.Build<Event>()
        //        .Without(p => p.Category)
        //        .Without(p => p.Location)
        //        .Without(p => p.AllUsers)
        //        .Without(p => p.Invites)
        //        .Without(p => p.Requests)
        //        .With(p => p.Id, 1).Create<Event>();

        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.SetupSequence(m => m.ExecOne()).Returns(null);

        //    //act
        //    _eventRepository.Edit(1, @event);

        //    //assert

        //}

        //[Test]
        //[ExpectedException(typeof(Exception), ExpectedMessage = "event not found")]
        //public void RemoveEvent_ThrowException_EventNotFound()
        //{
        //    //arange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    Event @event = null;

        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.SetupSequence(m => m.ExecOne()).Returns(@event);

        //    //act
        //    _eventRepository.Remove(1, 1);

        //    //assert
        //}

        //[Test]
        //public void RemoveEvent_EventRemoved_ShouldBeRemoved()
        //{
        //    //arange
        //    var queryBuilderEvent = new Mock<IQueryBuilder<Event>>();

        //    var @event = _fixture.Build<Event>()
        //       .Without(p => p.Category)
        //       .Without(p => p.Location)
        //       .Without(p => p.AllUsers)
        //       .Without(p => p.Invites)
        //       .Without(p => p.Requests)
        //       .With(p => p.Id, 1).Create<Event>();

        //    _queryFactory.Setup(m => m.Set<Event>()).Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.Setup(x => x.Where(It.IsAny<Expression<Func<Event, bool>>>()))
        //        .Returns(queryBuilderEvent.Object);
        //    queryBuilderEvent.SetupSequence(m => m.ExecOne()).Returns(@event);
        //    queryBuilderEvent.SetupSequence(m => m.Remove(It.IsAny<Event>())).Returns(queryBuilderEvent.Object);

        //    //act
        //    _eventRepository.Remove(1, @event.Id);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        queryBuilderEvent.Verify(p=>p.Remove(@event));
        //        queryBuilderEvent.Verify(p=>p.SaveChanges());
        //    }
        //}
    }
}
