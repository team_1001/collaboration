﻿using BusinessLayer.Services.Implementations;
using BusinessLayer.Services.Interfaces;
using FluentAssertions;
using NUnit.Framework;

namespace Team_Collaboration.Tests.BusinessLayerTests.ServicesTests
{
    [TestFixture]
    public class GeoLocatorTest
    {
        private readonly IGeoLocator _geoLocatorService;

        public GeoLocatorTest()
        {
            _geoLocatorService = new GeoLocator();
        }

        [Test]
        public void GetCountryCode_GettingCode_ShouldReturnCodeRu()
        {
            //arrange
            string userIp = "83.219.138.184";

            //act
            string userCountryCode = _geoLocatorService.GetCountryCode(userIp);

            //assert
            userCountryCode.Should().BeEquivalentTo("RU");
        }
    }
}
