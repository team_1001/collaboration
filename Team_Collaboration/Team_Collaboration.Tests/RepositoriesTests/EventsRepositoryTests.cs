﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using AuthLayer;
using Autofac.Extras.NLog;
using Common.Configuration;
using DataAccessLayer;
using DataAccessLayer.Configuration;
using DataAccessLayer.Repositories;
using DataAccessLayer.UnitOfWork;
using DataLayer.Entity;
using FluentAssertions;
using Models;
using Models.Paging;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace Team_Collaboration.Tests.RepositoriesTests
{
    [TestFixture]
    public class EventsRepositoryTests
    {
        private readonly Fixture _fixture = new Fixture();
        private string _connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=TestDb;Integrated Security=True";
        private IConfigurationProvider _configuration;
        private ILogger _logger;

        [TestFixtureSetUp]
        public void GlobalSetup()
        {
            var configMock = new Mock<IConfigurationProvider>();
            NameValueCollection connStrings = new NameValueCollection()
            {
                { "DefaultConnection", _connectionString }
            };

            configMock.Setup(x => x.ConnectionStrings).Returns(connStrings);
            _configuration = configMock.Object;

            _logger = new Mock<ILogger>().Object;
        }

        [TestFixtureTearDown]
        public void GlobalTeardown()
        {
        }

        [SetUp]
        public void Setup()
        {
            try
            {
                var migrator = new Migrator(_logger);
                migrator.Migrate(_connectionString);

                Random rand = new Random(DateTime.Now.Millisecond);

                string email = _fixture.Create<string>();

                var user = new AppUser
                {
                    UserName = email,
                    Email = email,
                    User = new User() { Email = email, UserName = email }
                };

                using (var authUow = new EntityUnitOfWork<AppUserDbContext>(new AppUserDbContextFactory(_configuration)).Create())
                {
                    authUow.Add(user).CommitAsync().Wait();
                }

                List<UserToEvent> events = new List<UserToEvent>();
                for (int i = 0; i < rand.Next(5, 12); i++)
                {
                    UserToEvent @ev = new UserToEvent()
                    {
                        Event = new Event()
                        {
                            CategoryId = 1,
                            Dialog = new Dialog()
                            {
                                Id = Guid.NewGuid().ToString("n")
                            },

                            HostId = 1,
                            EventType = EventType.Public,
                            Location = new Location()
                            {
                                Latitude = rand.Next(55, 56),
                                Longitude = rand.Next(53, 54)
                            }
                        },
                        UserId = 1,
                        Role = EventRoles.Host
                    };

                    events.Add(@ev);
                }

                using (var uow = new EntityUnitOfWork<ActivityContext>(new ActivityContextFactory(_configuration)).Create())
                {
                    uow.AddRange(events).CommitAsync().Wait();
                }
            }
            catch (Exception e)
            {
            }
        }

        [TearDown]
        public void Teardown()
        {
            try
            {
                var ctx = new ActivityContext(_connectionString);
                if (ctx.Database.Exists())
                    ctx.Database.Delete();
            }
            catch (Exception e)
            {
            }
        }

        [Test]
        public void GetGeoEvents_AnyState_WorksAsExpected()
        {
            double latitude = 55.55;
            double longitude = 53.43;
            double radius = 250.0;

            GeoInfo geo = new GeoInfo()
            {
                Latitude = latitude,
                Longitude = longitude,
                Radius = radius
            };

            var uow = new EntityUnitOfWork<ActivityContext>(new ActivityContextFactory(_configuration));
            var repo = new EventsRepository(_logger, uow);

            var result = repo.GetGeoEvents(geo, new PageInfo()).Result;
            result.Should().BeOfType<ItemsResult<Event>>().Which.Total.Should().BePositive();
        }
    }
}
