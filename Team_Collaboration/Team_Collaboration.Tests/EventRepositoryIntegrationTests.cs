﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;
using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;

namespace Team_Collaboration.Tests
{
    [TestFixture]
    public class EventRepositoryIntegrationTests
    {
        //private  EventRepository _eventRepository;
        //private  ActivityContext _context;

        //public EventRepositoryIntegrationTests()
        //{
        //    _context = new ActivityContext(TestConfig.SqlExpressConnectionString);
        //    _eventRepository = new EventRepository(new EntityQueryBuilderFactory(_context));
        //}

        //[Test]
        //public void Create_AddingEventToContext_AddedEvent()
        //{
        //    //arrange
        //    Event ev = new Event()
        //    {
        //        Location = new Location() {Adress = "someadress",Id = 200},
        //        LocationId = 200,
        //        AllUsers = new List<UserToEvent>(),
        //        Invites = new List<Invite>(),
        //        Requests = new List<Request>(),
        //        Category = new EventCategory(),
        //        DateStarts = new DateTime(1991,1,1),
        //        DateEnds = new DateTime(2016,1,1),
        //        Description = "some my event for this test"
        //    };

        //    //act
        //    _eventRepository.Create(1, ev);

        //    //assert
        //    _context.Events.FirstOrDefault(e => e.Description.Equals("some my event for this test")).Should().NotBeNull();
        //}

        //[Test]
        //public void Remove_RemovingEventFromContext_EventRemoved()
        //{
        //    //arrange

        //    //act
        //    _eventRepository.Remove(1, 1);

        //    //assert
        //    _context.Events.FirstOrDefault(e => e.Id == 1).Should().BeNull();
        //}

        //[Test]
        //public void Edit_EditingEvent_EventUpdated()
        //{
        //    //arrange
        //    var eventCategory = new EventCategory() {Description = "evenCat",Name = "Testing"};

        //    Event @event = new Event()
        //    {
        //        Location = new Location() { Adress = "someadress", Id = 200 },
        //        LocationId = 200,
        //        AllUsers = new List<UserToEvent>(),
        //        Invites = new List<Invite>(),
        //        Requests = new List<Request>(),
        //        Category = eventCategory,
        //        DateStarts = new DateTime(1991, 1, 1),
        //        DateEnds = new DateTime(2016, 1, 1),
        //        Description = "some my event for this test"
        //    };

        //    _eventRepository.Create(1, @event);
        //    _context.Dispose();

        //    _context = new ActivityContext(TestConfig.SqlExpressConnectionString);
        //    _eventRepository = new EventRepository(new EntityQueryBuilderFactory(_context));

        //    //id is 101 becouse in seed there are only 100 events
        //    var evs = _context.Events.ToList();
        //    @event = _context.Events.Include(e=>e.Category).First(e => e.Id == 101);
        //    @event.Description = "my new descr for test";
            
        //    //act
        //    _eventRepository.Edit(1, @event);

        //    //assert
        //    using (new AssertionScope())
        //    {
        //        var updatedEvent = _context.Events.First(e => e.Id == 101);
        //        updatedEvent.Description.Should().BeEquivalentTo("my new descr for test");
        //        updatedEvent.Category.Should().NotBeNull();
        //        updatedEvent.Category.Name.Should().BeEquivalentTo("Testing");
        //    }
            
        //}

        //[TearDown]
        //public void DropDB()
        //{
        //    var context = new ActivityContext(TestConfig.SqlExpressConnectionString);
        //    context.Database.Delete();
        //}
    }
}
