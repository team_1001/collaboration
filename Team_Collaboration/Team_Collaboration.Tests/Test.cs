﻿using System.Linq;
using DataAccessLayer;
using DataAccessLayer.QueryBuilder;
using DataLayer.Entity;
using NUnit.Framework;
using User = DataLayer.Entity.User;

namespace Team_Collaboration.Tests
{
    [TestFixture]
    public class Test
    {

        [Test]
        public void Test1()
        {

            EntityQueryBuilderFactory q = new EntityQueryBuilderFactory(new ActivityContext());
            var q1 = q.Set<Invite>().Select(x => new {x.HostUserId, x.State});
            var result =
                q.Set<User>()
                    .Where(x => x.FirstName == "Nick")
                    .Join(x => x.Id, (UserToEvent e) => e.UserId, (x, y) => new {y.Role, x.FirstName, x.Id})
                    .Join(q1, x => x.Id, x => x.HostUserId, (x, y) => new {x.Id, x.Role, x.FirstName, y.State});
            
            //execute when context will be ready
            //result.Exec();
        }

        [Test]
        public void TestContext()
        {
            var context = new ActivityContext(TestConfig.SqlExpressConnectionString);
            var user = context.Users.FirstOrDefault();
            var friend = context.Users.OrderBy(x => x.Id).Skip(1).FirstOrDefault();
            if (user != null)
            {
                user.Friends.Add(friend);
                context.Set<User>().Attach(user);
                context.SaveChanges();

                context.Users.Local.Clear();
            }

            Assert.IsTrue(user != null);
        }

        [TearDown]
        public void DropDB()
        {
            var context = new ActivityContext(TestConfig.SqlExpressConnectionString);
            context.Database.Delete();
        }
    }

    public class A
    {
        public int X { get; set; }
    }

    public class B
    {
        public int X { get; set; }
    }
}
