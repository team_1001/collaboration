﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;

namespace Team_Collaboration.Controllers
{
    public class FriendsController : BaseApiController
    {
        private readonly IUserProvider _userProvider;

        public FriendsController(IUserProvider userProvider)
        {
            _userProvider = userProvider;
        }

        public IHttpActionResult Get()
        {
            return NotFound();
        }
    }
}