﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Routing;
using BusinessLayer;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using NLog;

namespace Team_Collaboration.Controllers
{
    public class UserEventsController : BaseApiController
    {
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        private readonly IUserProvider _userProvider;

        public UserEventsController(IUserProvider userProvider)
        {
            _userProvider = userProvider;
        }

        public IHttpActionResult Post([FromUri] int id)
        {
            try
            {
                _userProvider.Join(UserId, id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        public IHttpActionResult Delete([FromUri] int id)
        {
            try
            {
                _userProvider.Leave(UserId, id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        public IEnumerable<Event> Get()
        {
            _logger.Trace("Getting event for the user {0}", UserId);
            return _userProvider.GetEvents(UserId);
        } 
    }
}