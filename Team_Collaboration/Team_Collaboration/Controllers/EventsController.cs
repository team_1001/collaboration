﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;

namespace Team_Collaboration.Controllers
{
    public class EventsController : BaseApiController
    {
        private readonly IEventProvider _eventProvider;

        public EventsController(IEventProvider eventProvider)
        {
            _eventProvider = eventProvider;
        }

        public IEnumerable<Event> Get()
        {
            return _eventProvider.GetAll();
        }

        public dynamic Get(int id)
        {
            Event @event = _eventProvider.Get(id);

            return new { Event = @event, IsHost = @event.HostId == UserId };
        }

        public IHttpActionResult Post(Event model)
        {
            try
            {
                _eventProvider.Add(model, UserId);
                return JsonResult(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Put(Event model)
        {
            try
            {
                _eventProvider.Update(model, UserId);
                return JsonResult(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Delete([FromUri] int id)
        {
            try
            {
                _eventProvider.Delete(id, UserId);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }
    }
}