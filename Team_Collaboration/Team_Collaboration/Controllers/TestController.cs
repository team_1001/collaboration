﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Team_Collaboration.Controllers
{
    public class TestController : ApiController
    {
        public IEnumerable<string> Get()
        {
            return Enumerable.Repeat("test", 10);
        } 


    }
}