﻿using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Team_Collaboration.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected int UserId => User.Identity.GetUserId<int>();

        protected virtual JsonResult<T> JsonResult<T>(T content)
        {
            return Json(content, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                PreserveReferencesHandling = PreserveReferencesHandling.None
            }, Encoding.UTF8);
        }
    }
}