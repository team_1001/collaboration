﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using BusinessLayer;
using BusinessLayer.Services.Interfaces;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using Team_Collaboration.Helpers;

namespace Team_Collaboration.AutoFac
{
    public class MainModule : Module
    {
        private readonly IAppBuilder _appBuilder;

        public MainModule(IAppBuilder appBuilder)
        {
            _appBuilder = appBuilder;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(x => _appBuilder.GetDataProtectionProvider());
            builder.Register(x => HttpContext.Current.GetOwinContext().Authentication);

            builder.RegisterType<DataStorage>().As<IDataStorage>();
            builder.RegisterType<EventProvider>().As<IEventProvider>();
            builder.RegisterType<UserProvider>().As<IUserProvider>();
            builder.RegisterApiControllers(typeof (MainModule).Assembly);
            builder.RegisterControllers(typeof (MainModule).Assembly);
        }
    }
}