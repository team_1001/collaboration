﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Common;
using Microsoft.Owin;
using Owin;
using Team_Collaboration.AutoFac;

[assembly: OwinStartup(typeof(Team_Collaboration.Startup))]

namespace Team_Collaboration
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration configuration = new HttpConfiguration();

            WebApiConfig.Register(configuration);

            ConfigureAuth(app);

            var builder = AutoFacCore.Init(new MainModule(app), new BusinessLayer.AutoFac.MainModule());
            var scope = builder.BeginLifetimeScope();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(scope));
            configuration.DependencyResolver = new AutofacWebApiDependencyResolver(scope);
            app.UseAutofacMiddleware(scope);
            app.UseAutofacMvc();
            app.UseWebApi(configuration);
        }
    }
}
