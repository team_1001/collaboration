﻿(function(window) {
    'use strict';

    /* Services */

    var getHost = function() {
        return window.location.protocol + '//' + window.location.host;
    }

    var testServices = angular.module('testServices', ['ngResource']);

    testServices.value('host', getHost());

    testServices.factory('Cache', function ($cacheFactory) {
        var $httpDefaultCache = $cacheFactory.get('$http');
 
        return {
            invalidate: function (key) {
                $httpDefaultCache.remove(key);
            }
        }
    });

    testServices.factory('Data', ['$resource', 'host',
        function ($resource, host) {
            return $resource(host + '/api/test/:id', {}, {
                query: {
                    method: 'GET',
                    isArray: true,
                    cache: true
                },

                save: {
                    method: 'POST'
                },

                update: {
                    method: 'PUT'
                }
            });
        }]);

    testServices.factory('Events', ['$resource', 'Cache', 'host',
        function ($resource, Cache, host) {
            var Events = $resource(host + '/api/Events/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: true,
                    cache: true
                },
                
                get: {
                    method: 'GET',
                    isArray: false,
                    cache: true
                },

                save: {
                    method: 'POST'
                },

                update: {
                    method: 'PUT'
                }
            });

            Events.prototype.reset = function (id) {
                if (id) {
                    Cache.invalidate(host + '/api/Events/' + id);
                } else {
                    Cache.invalidate(host + '/api/Events/');
                }
            };

            return Events;
        }]);

    testServices.factory('Users', ['$resource', 'host',
        function ($resource, host) {
            return $resource(host + '/api/UserEvents/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: true
                },

                save: {
                    method: 'POST'
                },

                remove: {
                    method: 'DELETE'
                },

                update: {
                    method: 'PUT'
                }
            });
        }]);
})(window);

