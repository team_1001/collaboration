﻿(function () {
    var app = angular.module('App', ['ngResource', 'ngRoute', 'ui.router', 'ui.bootstrap', 'testControllers', 'testServices']);

    app.directive('sortable', [
        function () {
            return {
                link: function(scope, element) {
                    $(element).sortable().disableSelection();
                }
            }
            
        }
    ]);

    app.config([
        '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/test");

            $stateProvider
                .state('main', {
                    url: "/test",
                    templateUrl: "views/test.html",
                    controller: 'MainCtrl'
                }).state('main.profile', {
                    url: "/profile",
                    views: {
                        "main": {
                            templateUrl: "views/profile.html"
                        }
                    },
                    controller: 'ProfileCtrl'
                }).state('main.feed', {
                    url: "/feed",
                    views: {
                        "main": {
                            templateUrl: "views/feed.html",
                            controller: 'FeedCtrl'
                        }
                    }

                }).state('main.feed.view', {
                    url: "/view/:id",
                    templateUrl: "views/event.html",
                    controller: 'FeedViewCtrl'
                }).state('main.events', {
                    url: "/events",
                    views: {
                        "main": {
                            templateUrl: "views/events.html",
                            controller: 'EventsCtrl'
                        }
                    }
                }).state('main.events.view', {
                    url: "/view/:id",
                    templateUrl: "views/event.html",
                    controller: 'EventViewCtrl'
                }).state('main.events.create', {
                    url: "/add",
                    templateUrl: "views/addEvent.html",
                    controller: 'CreateEventCtrl'
                }).state('main.events.edit', {
                    url: "/edit/:id",
                    templateUrl: "views/editEvent.html",
                    controller: 'EventEditCtrl'
                }).state('main.friends', {
                    url: "/friends",
                    views: {
                        "main": {
                            templateUrl: "views/friends.html"
                        }
                    },
                    controller: 'FriendsCtrl'
                }).state('main.messages', {
                    url: "/messages",
                    views: {
                        "main": {
                            templateUrl: "views/messages.html"
                        }
                    },
                    controller: 'MessagesCtrl'
                }).state('main.settings', {
                    url: "/settings",
                    views: {
                        "main": {
                            templateUrl: "views/settings.html"
                        }
                    },
                    controller: 'SettingsCtrl'
                });

            //.state('main.add', {
            //    url: "/add",
            //    templateUrl: "partials/sitemap.html",
            //    controller: 'AddSiteMapCtrl'
            //})

            //.state('main.edit', {
            //    url: "/edit",
            //    templateUrl: "partials/sitemap.html",
            //    controller: 'EditSiteMapCtrl'
            //});

        }
    ]);
    //.run(['$state', function ($state)
    //{
    //    $state.transitionTo('main');
    //}]);
})();



//app.controller('HomeCtrl', ['$scope', '$resource',
//    function ($scope, $resource) {
//        var Scrape = $resource('/scrape');

//        $scope.config = '';
//        $scope.result = '';

//        $scope.save = function () {
//            if (!$scope.config)
//                return;

//            console.log($scope.config);

//            var parsed;

//            try {
//                parsed = eval('(' + $scope.config + ')');
//            }
//            catch (err) {

//            }

//            var config = {
//                config: {
//                    parse: parsed || {},
//                    request: {
//                        uri: 'http://localhost:3000/data/test.html'
//                    }
//                }
//            };

//            Scrape.save(config).$promise
//                .then(function (result) {
//                    console.log(result);
//                    var json = result.toJSON();
//                    if (json)
//                        $scope.result = JSON.stringify();
//                }, function (err) {
//                    console.log(err);
//                });
//        };

//    }]);

/**
 * Created by Дмитрий on 3/18/2016.
 */
