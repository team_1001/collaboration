﻿(function() {
    'use strict';

    /* Controllers */

    var testControllers = angular.module('testControllers', []);

    testControllers.controller('MainCtrl', ['$scope', 'Data',
        function ($scope, Data) {
            $scope.data = [];

            Data.query().$promise.then(function(items) {
                $scope.data = items.map(function(item) {
                    return {
                        name: item
                    }
                });
            });
        }]);

    testControllers.controller('ProfileCtrl', ['$scope', 'Data',
        function ($scope, Data) {
            $scope.data = [];

            Data.query().$promise.then(function (items) {
                $scope.data = items.map(function (item) {
                    return {
                        name: item
                    }
                });
            });
        }]);

    testControllers.controller('FeedCtrl', ['$scope', 'Events', '$stateParams',
        function ($scope, Events) {
            $scope.sortOpts = [{
                id: 1,
                label: 'name'
            }, {
                id: 2,
                label: 'index'
            }];

            $scope.sortOpt = $scope.sortOpts[0];

            $scope.events = [];

            Events.query().$promise.then(function (items) {
                $scope.events = items.map(function (event) {
                    return {
                        name: event.name,
                        index: event.id,
                        id: event.id
                    }
                });
            });

        }]);

    testControllers.controller('FeedViewCtrl', ['$scope', 'Users', '$stateParams', '$state',
       function ($scope, Users, $stateParams, $state) {
           $scope.event = { name: "Event_" + $stateParams.id };
           $scope.action = "Join";
           $scope.act = function () {
               Users.save({ id: $stateParams.id })
                   .$promise
                   .then(function() {
                       console.log('done');
                       $state.go('main.events');
                   }).catch(function() {
                       console.log('failed');
                   });
           };
       }]);

    testControllers.controller('EventsCtrl', ['$scope', 'Users', '$stateParams',
        function ($scope, Users, $stateParams) {
            $scope.sortOpts = [{
                id: 1,
                label: 'name'
            }, {
                id: 2,
                label: 'index'
            }];

            $scope.sortOpt = $scope.sortOpts[0];

            $scope.events = [];

            $scope.remove = function(id) {
                var events = this.events.filter(function (ev) { return ev.id === +id });
                if (!events.length)
                    return;

                this.events.splice(this.events.indexOf(events[0]), 1);
            };

            $scope.add = function (event) {
                this.events.push(event);
            };

            $scope.update = function (event) {
                var events = this.events.filter(function (ev) { return ev.id === event.id });
                if (!events.length)
                    return;

                $.extend(events[0], event);
            };

            Users.query().$promise.then(function (items) {
                $scope.events = items.map(function (event) {
                    return {
                        name: event.name,
                        index: event.id,
                        id: event.id,
                        userId: $stateParams.userId
                    }
                });
            });
        }]);

    testControllers.controller('EventViewCtrl', ['$scope', 'Users', 'Events', '$stateParams', '$state', '$uibModal',
       function ($scope, Users, Events, $stateParams, $state, $uibModal) {
           $scope.event = { name: "Event_" + $stateParams.id };
           $scope.action = "Leave";

           Events.get({ id: $stateParams.id })
               .$promise
               .then(function(data) {
                   $scope.event = $.extend(data.event, { canEdit: data.isHost === true });
               }).catch(function() {
                   console.log('failed');
               });

           $scope.delete = function () {
               var modalInstance = $uibModal.open({
                   templateUrl: 'views/confirmation.html',
                   controller: 'EventDeleteCtrl',
                   resolve: {
                       id: function () {
                           return $stateParams.id;
                       }
                   }
               });

               modalInstance.result.then(function () {
                   $scope.$parent.remove($stateParams.id);
                   console.log('done');
                   $state.go('main.events');
               }, function () {
                   console.log('canceled');
               });
           };

           $scope.act = function () {
               Users.remove({ id: $stateParams.id })
                   .$promise
                   .then(function () {
                       $scope.$parent.remove($stateParams.id);
                       console.log('done');
                       $state.go('main.events');
                   }).catch(function () {
                       console.log('failed');
                   });
           };
       }]);

    testControllers.controller('EventDeleteCtrl', [
        '$scope', 'Events', '$uibModalInstance', 'id',
        function($scope, Events, $uibModalInstance, id) {

            $scope.save = function() {
                Events.remove({ id: id })
                    .$promise
                    .then(function () {
                        $uibModalInstance.close();
                    }).catch(function () {
                        console.log('failed');
                        $uibModalInstance.dismiss();
                    });
            };

            $scope.cancel = function() {
                $uibModalInstance.dismiss();
            };
        }
    ]);


    testControllers.controller('EventEditCtrl', ['$scope', 'Events', '$stateParams', '$state',
       function ($scope, Events, $stateParams, $state) {
           $scope.event = { name: "Event_" + $stateParams.id };

           Events.get({ id: $stateParams.id })
               .$promise
               .then(function (data) {
                   $scope.event = $.extend(data.event, { canEdit: data.isHost === true });
               }).catch(function () {
                   console.log('failed');
               });

           $scope.save = function () {
               Events.update($scope.event)
                   .$promise
                   .then(function (data) {
                       $scope.$parent.update($scope.event);
                       Events.prototype.reset(data.id);
                       console.log('done');
                       $state.go('main.events');
                   }).catch(function () {
                       console.log('failed');
                   });
           };
       }]);

    testControllers.controller('CreateEventCtrl', ['$scope', 'Events', '$stateParams', '$state',
       function ($scope, Events, $stateParams, $state) {
           $scope.event = { name: "Event_" + $stateParams.id };

           $scope.event = {
               name: 'Event_name',
               description: 'Event_description'
           };

           $scope.save = function () {
               Events.save($scope.event)
                   .$promise
                   .then(function (data) {
                       $scope.$parent.add(data);
                       console.log('done');
                       $state.go('main.events');
                   }).catch(function () {
                       console.log('failed');
                   });
           };
       }]);

    testControllers.controller('FriendsCtrl', ['$scope', 'Data',
        function ($scope, Data) {
            $scope.data = [];

            Data.query().$promise.then(function (items) {
                $scope.data = items.map(function (item) {
                    return {
                        name: item
                    }
                });
            });
        }]);

    testControllers.controller('MessagesCtrl', ['$scope', 'Data',
        function ($scope, Data) {
            $scope.data = [];

            Data.query().$promise.then(function (items) {
                $scope.data = items.map(function (item) {
                    return {
                        name: item
                    }
                });
            });
        }]);

    testControllers.controller('SettingsCtrl', ['$scope', 'Data',
        function ($scope, Data) {
            $scope.data = [];

            Data.query().$promise.then(function (items) {
                $scope.data = items.map(function (item) {
                    return {
                        name: item
                    }
                });
            });
        }]);
})();