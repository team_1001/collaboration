﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.Services.Interfaces;
using DataLayer.Entity;
using User = DataLayer.Entity.User;

namespace Team_Collaboration.Helpers
{
    public class DataHelper
    {
    }

    public interface IDataStorage
    {
        IEnumerable<User> GetUserDataFromSession();

        IEnumerable<Event> GetEventDataFromSession();

        void SaveUserDataToSession(IEnumerable<User> users);

        void SaveEventDataToSession(IEnumerable<Event> events);
    }

    public class DataStorage : IDataStorage
    {
        private const string CUsers = "USERS";
        private const string CEvents = "EVENTS";


        public IEnumerable<User> GetUserDataFromSession()
        {
            var users = HttpContext.Current.Session[CUsers] as IEnumerable<User>;
            if (users == null)
            {
                users = GenerateFakeUsers();
                HttpContext.Current.Session[CUsers] = users;
            }

            return users;
        }

        public IEnumerable<Event> GetEventDataFromSession()
        {
            var events = HttpContext.Current.Session[CEvents] as IEnumerable<Event>;
            if (events == null)
            {
                events = GenerateFakeEvents();
                HttpContext.Current.Session[CEvents] = events;
            }

            return events;
        }

        public void SaveUserDataToSession(IEnumerable<User> users)
        {
            HttpContext.Current.Session[CUsers] = users;
        }

        public void SaveEventDataToSession(IEnumerable<Event> events)
        {
            HttpContext.Current.Session[CEvents] = events;
        }

        private IEnumerable<User> GenerateFakeUsers()
        {
            List<User> users = new List<User>();
            Random rand = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < rand.Next(200, 300); i++)
            {
                int year = rand.Next(1950, 2000);
                int month = rand.Next(1, 13);
                int day = rand.Next(1, DateTime.DaysInMonth(year, month) + 1);

                users.Add(new User()
                {
                    Id = i,
                    Birthdate = new DateTime(year, month, day),
                    Email = $"mailbox_{i}@gmail.com",
                    FirstName = $"fName_{i}",
                    LastName = $"lName_{i}",
                    Contacts = $"contacts_{i}",
                });
            }

            return users;
        }

        private IEnumerable<Event> GenerateFakeEvents()
        {
            var users = GetUserDataFromSession().ToList();

            List<Event> events = new List<Event>();
            Random rand = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < rand.Next(200, 300); i++)
            {
                int year = rand.Next(2015, 2016);
                int month = rand.Next(1, 13);
                int day = rand.Next(1, DateTime.DaysInMonth(year, month) + 1);

                DateTime start = new DateTime(year, month, day);
                DateTime end = start.AddDays(rand.Next(1, 50));

                var @event = new Event()
                {
                    Id = i,
                    Name = $"Event_{i}",
                    Description = $"Description_{i}",
                    DateStarts = start,
                    DateEnds = end
                };

                var host = users.ElementAt(rand.Next(0, users.Count));
                @event.AllUsers.Add(new UserToEvent()
                {
                    Event = @event,
                    Role = "Host",
                    User = host,
                    UserId = host.Id,
                    EventId = @event.Id
                });

                @events.Add(@event);
            }

            return events;
        }
    }

    public class EventProvider : IEventProvider
    {
        private readonly IDataStorage _dataStorage;

        public EventProvider(IDataStorage dataStorage)
        {
            _dataStorage = dataStorage;
        }

        public IEnumerable<Event> GetAll()
        {
            return _dataStorage.GetEventDataFromSession();
        }

        public Event Get(int eventId)
        {
            return _dataStorage.GetEventDataFromSession().FirstOrDefault(x => x.Id == eventId);
        }

        public bool Add(Event @event, int userId)
        {
            var events = _dataStorage.GetEventDataFromSession().ToList();
            var users = _dataStorage.GetUserDataFromSession().ToList();

            var user = users.FirstOrDefault(x => x.Id == userId);

            if (user == null)
                throw new InvalidOperationException($"User {userId} doesn't exist");

            events.Add(@event);
            int newId = events.Max(x => x.Id) + 1;

            @event.Id = newId;
            @event.AllUsers.Add(new UserToEvent() { EventId = newId, Event = @event, Role = "host", User = user, UserId = userId });
            _dataStorage.SaveEventDataToSession(events);
            return true;
        }

        public bool Update(Event @event, int userId)
        {
            var events = _dataStorage.GetEventDataFromSession().ToList();
            var existing = events.FirstOrDefault(x => x.Id == @event.Id);

            if (existing == null)
                throw new InvalidOperationException($"Event {@event.Id} doesn't exist");

            int index = events.IndexOf(existing);
            events.Remove(existing);
            events.Insert(index, @event);
            _dataStorage.SaveEventDataToSession(events);
            return true;
        }

        public bool Delete(int eventId, int userId)
        {
            var events = _dataStorage.GetEventDataFromSession().ToList();
            var existing = events.FirstOrDefault(x => x.Id == eventId);

            if (existing == null)
                throw new InvalidOperationException($"Event {eventId} doesn't exist");

            if (existing.HostId != userId)
                throw new InvalidOperationException($"User {userId} is not a host of {eventId}. Event can't be removed");

            events.Remove(existing);
            _dataStorage.SaveEventDataToSession(events);
            return true;
        }
    }

    public class UserProvider : IUserProvider
    {
        private readonly IDataStorage _dataStorage;

        public UserProvider(IDataStorage dataStorage)
        {
            _dataStorage = dataStorage;
        }


        public IEnumerable<User> GetAll()
        {
            return _dataStorage.GetUserDataFromSession();
        }

        public User Get(int userId)
        {
            return _dataStorage.GetUserDataFromSession().FirstOrDefault(x => x.Id == userId);
        }

        public IEnumerable<Event> GetEvents(int userId)
        {
            return _dataStorage.GetEventDataFromSession().Where(x => x.AllUsers.Any(u => u.UserId == userId));
        }

        public IEnumerable<Event> GetEvents()
        {
            return _dataStorage.GetEventDataFromSession().Take(30);
        }

        public bool Join(int userId, int eventId)
        {
            var events = _dataStorage.GetEventDataFromSession().ToList();
            var users = _dataStorage.GetUserDataFromSession().ToList();

            var @event = events.FirstOrDefault(x => x.Id == eventId);
            var user = users.FirstOrDefault(x => x.Id == userId);

            if (@event == null)
                throw new InvalidOperationException($"Event {eventId} doesn't exist");

            if (user == null)
                throw new InvalidOperationException($"User {userId} doesn't exist");

            @event.AllUsers.Add(new UserToEvent() { EventId = @eventId, Event = @event, Role = "guest", User = user, UserId = userId});
            _dataStorage.SaveEventDataToSession(events);
            return true;
        }

        public bool Leave(int userId, int eventId)
        {
            var events = _dataStorage.GetEventDataFromSession().ToList();

            var @event = events.FirstOrDefault(x => x.Id == eventId);

            if (@event == null)
                throw new InvalidOperationException($"Event {eventId} doesn't exist");

            var user = @event.AllUsers.FirstOrDefault(x => x.UserId == userId);
            if (user == null)
                throw new InvalidOperationException($"User {userId} doesn't exist");

            @event.AllUsers.Remove(user);
            _dataStorage.SaveEventDataToSession(events);
            return true;
        }
    }
}