﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Common.Extensions
{
    public static class ObjectExtensions
    {
        public static byte[] ToByteArray<T>(this T obj) where T : class
        {
            if (obj == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static T FromByteArray<T>(this byte[] raw) where T : class
        {
            if (raw == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(raw))
            {
                return bf.Deserialize(ms) as T;
            }
        }
    }
}
