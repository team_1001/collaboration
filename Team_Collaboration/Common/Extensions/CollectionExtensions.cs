﻿using System;
using System.Collections.Generic;

namespace Common.Extensions
{
    public static class CollectionExtensions
    {
        public static IEnumerable<TSource> ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            foreach (var item in source)
            {
                action(item);
                yield return item;
            }
        } 
    }
}
