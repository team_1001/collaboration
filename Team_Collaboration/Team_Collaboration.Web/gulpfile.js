﻿/// <binding AfterBuild='copy' ProjectOpened='watch' />
// include plug-ins
var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var del = require('del');

var itemsToCopy = {
    "./node_modules/signalr/*signalR*.js": "./Scripts/signalR/",
    "./node_modules/ngGeolocation/ngGeolocation*.js": "./Scripts/ngGeolocation/",
    "./node_modules/ng-file-upload/dist/*.js": "./Scripts/angular/ng-file-upload/",
    "./node_modules/lodash/lodash*.js": "./Scripts/lodash/",
    "./node_modules/angular*/**/angular*.js": "./Scripts/angular/",
    "./node_modules/angular-ui-bootstrap/dist/*.js": "./Scripts/angular/angular-ui-bootstrap/dist/",
    "./node_modules/angular-ui-bootstrap/template/modal/*.html": "./Scripts/app/template/modal/",
    "./node_modules/angular-ui-bootstrap/template/datepicker/*.html": "./Scripts/app/template/datepicker/",
    "./node_modules/angular-ui-bootstrap/template/datepickerPopup/*.html": "./Scripts/app/template/datepickerPopup/",
    "./node_modules/jquery-ui/**/jquery-ui*.js": "./Scripts/jquery-ui/"
};

var paths = {
    //scripts: ['./public/javascripts/**/*.js'],
    //scripts: ['./public/js/**/*.js', '!client/external/**/*.js'],
    //images: './public/images/**/*',
    input: {
        less: "./Content/Less/**/*.less"
    },

    output: {
        css: "./Content/Build/"
    }
};

gulp.task('copy', function () {
    for (var src in itemsToCopy) {
        if (!itemsToCopy.hasOwnProperty(src))
            continue;

        gulp.src(src)
            .pipe(gulp.dest(itemsToCopy[src]));
    }
});

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use any package available on npm
gulp.task('clean', function () {
    // You can use multiple globbing patterns as you would with `gulp.src`
    return del(['../../Content/Build/']);
});

gulp.task('css', ['clean'], function () {
    return gulp.src(paths.input.less)
        .pipe(less())
        //.pipe(less({
        //    paths: [path.join(__dirname, 'less', 'includes')]
        //}))
        .pipe(gulp.dest(paths.output.css));
});

// Rerun the task when a file changes
gulp.task('watch', function () {
    //gulp.watch(paths.scripts, ['scripts']);
    //gulp.watch(paths.images, ['images']);
    gulp.watch(paths.input.less, ['css']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'css']);

//var config = {
//    //Include all js files but exclude any min.js files
//    src: ['app/**/*.js', '!app/**/*.min.js']
//}

////delete the output file(s)
//gulp.task('clean', function () {
//    //del is an async function and not a gulp plugin (just standard nodejs)
//    //It returns a promise, so make sure you return that from this task function
//    //  so gulp knows when the delete is complete
//    return del(['app/all.min.js']);
//});

//// Combine and minify all files from the app folder
//// This tasks depends on the clean task which means gulp will ensure that the 
//// Clean task is completed before running the scripts task.
//gulp.task('scripts', ['clean'], function () {

//    return gulp.src(config.src)
//      .pipe(uglify())
//      .pipe(concat('all.min.js'))
//      .pipe(gulp.dest('app/'));
//});

////Set a default tasks
//gulp.task('default', ['scripts'], function () { });