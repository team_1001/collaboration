﻿(function() {
    'use strict';
    var services = angular.module('services');

    services.factory('Friends', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/Friends/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false
                },

                save: {
                    method: 'POST'
                },

                remove: {
                    method: 'DELETE'
                },

                update: {
                    method: 'PUT'
                }
            });
        }]);
})();

