﻿(function () {
    'use strict';
    var services = angular.module('services');

    services.factory('Users', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/Users/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false
                }

                //save: {
                //    method: 'POST'
                //},

                //remove: {
                //    method: 'DELETE'
                //}

                //update: {
                //    method: 'PUT'
                //}
            });
        }]);
})();

