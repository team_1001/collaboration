﻿(function () {
    'use strict';
    var services = angular.module('services');

    services.factory('Profile', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/Profile', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                },

                //remove: {
                //    method: 'DELETE'
                //}

                update: {
                    method: 'PUT'
                }
            });
        }]);
})();

