﻿(function () {
    'use strict';

    /* Services */

    var services = angular.module('services');

    services.factory('UserEvents', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/UserEvents/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false,
                    url: common.host + '/resource/api/UserEvents?:roles',
                    params: {
                        roles: "@roles"
                    }
                },

                participants: {
                    method: 'GET',
                    isArray: false,
                    url: common.host + '/resource/api/UserEvents/Participants/:id?:roles',
                    params: {
                        id: "@id",
                        roles: "@roles"
                    }
                },

                save: {
                    method: 'POST'
                },

                remove: {
                    method: 'DELETE'
                },

                update: {
                    method: 'PUT'
                }
            });
        }]);
})();

