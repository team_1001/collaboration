﻿(function(window) {
    'use strict';
    var auth = angular.module('auth');

    auth.service('AuthRedirectService', ['$state', 'localStorageService', 'AuthService', function($state, $localStorage, Auth) {
        this.checkAccess = function(event, toState, toParams, fromState, fromParams) {
            if (toState.data && toState.data.anonymous === true) {
                // anonymous
            } else {
                var authData = $localStorage.get('authData');

                if (authData) {
                    // authorized
                    //$scope.$root.user = $sessionStorage.user;
                } else {
                    // not authorized - login
                    event.preventDefault();
                    Auth.refreshToken().then(function() {
                        $state.go(toState, toParams);
                    }).catch(function() {
                        $state.go('auth.login');
                    });
                }
            }
        };
    }]);
})(window);