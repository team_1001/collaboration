﻿(function(window) {
    'use strict';

    var auth = angular.module('auth');

    auth.factory('AuthInterceptorService', ['$q', '$injector', '$location', 'localStorageService', '$log',
        function ($q, $injector, $location, $localStorage, $log) {

            var authInterceptorServiceFactory = {};
            var $http;

            var retryHttpRequest = function (config, deferred) {
                $http = $http || $injector.get('$http');
                $http(config)
                    .then(function(response) {
                        deferred.resolve(response);
                    }).catch(function(response) {
                        deferred.reject(response);
                    });
            }

            authInterceptorServiceFactory.request = function(config) {

                config.headers = config.headers || {};

                var authData = $localStorage.get('authData');
                if (authData) {
                    config.headers.Authorization = 'Bearer ' + authData.token;
                }

                return config;
            }

            authInterceptorServiceFactory.responseError = function (rejection) {
                $log.error(rejection);

                var deferred = $q.defer();
                if (rejection.status === 401) {
                    var authService = $injector.get('AuthService');
                    authService.refreshToken().then(function (response) {
                        retryHttpRequest(rejection.config, deferred);
                    }, function () {
                        authService.logOut();
                        $location.path('/login');
                        deferred.reject(rejection);
                    });
                } else {
                    return $q.reject(rejection);
                }
            }

            return authInterceptorServiceFactory;
        }
    ]);
})(window);