﻿(function (window) {
    'use strict';
    var auth = angular.module('auth', ['ngCookies']);

    auth.factory('AuthService', ['$http', '$q', '$interval', '$cookies', 'localStorageService', '$httpParamSerializerJQLike', 'common',
        function ($http, $q, $interval, $cookies, $localStorage, $httpParamSerializerJQLike, common) {
            var authService = {},
                $promise = null;

            // using token cookie for signalR because this is the most secure way to authorize client on hub and there is no other way
            // yet for OAuth bearer token authentication
            function setTokenCookie(token) {
                if (token)
                    $cookies.put('access_token', token);
            }

            function clearTokenCookie() {
                $cookies.remove('access_token');
            }

            authService.logOut = function() {
                $localStorage.remove('authData');
                clearTokenCookie();
            };

            authService.register = function(registerData) {
                this.logOut();

                return $http.post(common.host + '/resource/api/account', registerData);
            };

            authService.login = function(loginData) {
                loginData.grant_type = "password";

                if (loginData.useRefreshTokens) {
                    loginData.client_id = common.clientId;
                }

                var data = $httpParamSerializerJQLike(loginData);

                var deferred = $q.defer();

                $http.post(common.host + '/resource/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
                    .then(function(response) {
                        if (loginData.useRefreshTokens) {
                            $localStorage.set('authData', {
                                token: response.data.access_token,
                                userId: response.data.userId,
                                userName: loginData.userName,
                                refreshToken: response.data.refresh_token,
                                useRefreshTokens: true
                            });
                        } else {
                            $localStorage.set('authData', {
                                token: response.data.access_token,
                                userId: response.data.userId,
                                userName: loginData.userName,
                                refreshToken: "",
                                useRefreshTokens: false
                            });
                        }

                        setTokenCookie(response.data.access_token);
                        deferred.resolve(response.data);

                    }).catch(function(err, status) {
                        this.logOut();
                        deferred.reject(err);
                    }.bind(this));

                return deferred.promise;
            };

            authService.checkToken = function() {
                $interval(function () {
                    authService.refreshToken();
                }, 1200000);
            };

            authService.refreshToken = function () {
                if ($promise)
                    return $promise;

                var deferred = $q.defer();

                var authData = $localStorage.get('authData');

                if (authData) {
                    if (authData.useRefreshTokens) {
                        var data = {
                            grant_type: "refresh_token",
                            refresh_token: authData.refreshToken,
                            client_id: common.clientId
                        };

                        $localStorage.remove('authData');

                        $promise = deferred.promise;

                        $http.post(common.host + '/resource/token', $httpParamSerializerJQLike(data), { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
                            .then(function (response) {
                                $localStorage.set('authData', {
                                    token: response.data.access_token,
                                    userName: response.data.userName,
                                    userId: response.data.userId,
                                    refreshToken: response.data.refresh_token,
                                    useRefreshTokens: true
                                });

                                setTokenCookie(response.data.access_token);

                                deferred.resolve(response);
                            }).catch(function (err, status) {
                                this.logOut();
                                deferred.reject(err);
                            }.bind(this)).finally(function() {
                                $promise = null;
                            });
                    }

                    return $promise;
                }

                return $q.reject();
            };

            return authService;
        }
    ]);
})(window);