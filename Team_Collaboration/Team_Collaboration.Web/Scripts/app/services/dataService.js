﻿(function() {
    'use strict';

    /* Services */

    var services = angular.module('services', ['ngResource']);

    services.factory('Data', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/test/:id', {}, {
                query: {
                    method: 'GET',
                    isArray: true,
                    cache: true
                },

                save: {
                    method: 'POST'
                },

                update: {
                    method: 'PUT'
                }
            });
        }]);

})();

