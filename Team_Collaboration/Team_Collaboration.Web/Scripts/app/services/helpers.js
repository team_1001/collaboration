﻿(function(window) {
    'use strict';

    /* Services */

    var helpers = angular.module('helpers', []);

    helpers.factory('Cache', [
        '$cacheFactory', function($cacheFactory) {
            var $httpDefaultCache = $cacheFactory.get('$http');

            return {
                invalidate: function(key) {
                    $httpDefaultCache.remove(key);
                }
            }
        }
    ]);

    helpers.factory('ParamSerializer', [
        '$httpParamSerializerJQLike', function ($httpParamSerializerJQLike) {
            return {
                serialize: function (options) {
                    return options.values.map(function(v) {
                            var item = {};
                            item[options.name] = v;
                            return item;
                        }).map(function(x) { return $httpParamSerializerJQLike(x); })
                        .join("&");
                }
            }
        }
    ]);

    helpers.factory('EventHandlerFactory', [
        function () {
            var EventHandler = function () {
                this.counter = 0;
                this.handlers = [];
                this.additionalData = {};
            };

            EventHandler.prototype.add = function (handler) {
                var id = ++this.counter;
                this.handlers.push({
                    handler: handler,
                    id: id
                });

                return id;
            };

            EventHandler.prototype.remove = function (handlerId) {
                var handler = _.find(this.handlers, function (h) { return h.id === handlerId; });
                if (handler)
                    _.remove(this.handlers, handler);
            };

            return {
                create: function () {
                    return new EventHandler();
                }
            }
        }
    ]);

    helpers.constant('_', window._);
    helpers.constant('$', window.jQuery);
})(window);