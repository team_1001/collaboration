﻿(function () {
    'use strict';
    var services = angular.module('services');

    services.factory('ResourceFactory', ['$resource', 'common',
         function ($resource, common) {
             var createResource = function (endpoint) {
                 return $resource(common.host + endpoint + '/:id', { id: "@id" }, {
                     query: {
                         method: 'GET',
                         isArray: false
                     },

                     incoming: {
                         method: 'GET',
                         isArray: false,
                         url: common.host + endpoint + '/Incoming/:id',
                         params: {
                             id: "@id",
                             page: "@page",
                             pageSize: "@pageSize",
                             timeStampTicks: "@timeStampTicks"
                         }
                     },

                     outgoing: {
                         method: 'GET',
                         isArray: false,
                         url: common.host + endpoint + '/Outgoing/:id',
                         params: {
                             id: "@id",
                             page: "@page",
                             pageSize: "@pageSize",
                             timeStampTicks: "@timeStampTicks"
                         }
                     },

                     accept: {
                         method: 'POST',
                         url: common.host + endpoint + '/Accept'
                     },

                     decline: {
                         method: 'POST',
                         url: common.host + endpoint + '/Decline'
                     },

                     save: {
                         method: 'POST'
                     },

                     remove: {
                         method: 'DELETE'
                     },

                     update: {
                         method: 'PUT'
                     }
                 });
             }

             return {
                 _invitations: null,
                 _requests: null,

                 invitations: function () {
                     if (this._invitations)
                         return this._invitations;

                     this._invitations = createResource('/resource/api/Invitations');;
                     return this._invitations;
                 },

                 requests: function () {
                     if (this._requests)
                         return this._requests;

                     this._requests = createResource('/resource/api/Requests');;
                     return this._requests;
                 }
             }
         }]);
})();