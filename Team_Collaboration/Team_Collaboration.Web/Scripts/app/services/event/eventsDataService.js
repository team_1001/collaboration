﻿(function () {
    'use strict';

    /* Services */

    var services = angular.module('services');

    services.factory('Events', ['$resource', 'Cache', 'common',
        function ($resource, Cache, common) {
            var Events = $resource(common.host + '/resource/api/Events/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false,
                    cache: true,
                    params: {
                        page: "@page",
                        pageSize: "@pageSize",
                        lastId: "@lastId"
                    }
                },

                geo: {
                    method: 'GET',
                    isArray: false,
                    cache: true,
                    url: common.host + '/resource/api/Events/Geo',
                    params: {
                        latitude: "@latitude",
                        longitude: "@longitude",
                        radius: "@radius"
                    }
                },

                get: {
                    method: 'GET',
                    isArray: false,
                    cache: true
                },

                categories: {
                    method: 'GET',
                    isArray: false,
                    cache: true,
                    url: common.host + '/resource/api/Events/Categories'
                },

                save: {
                    method: 'POST'
                },

                update: {
                    method: 'PUT'
                }
            });

            Events.reset = function (id) {
                if (id) {
                    Cache.invalidate(common.host + '/resource/api/Events/' + id);
                } else {
                    Cache.invalidate(common.host + '/resource/api/Events/');
                }
            };

            return Events;
        }]);
})();

