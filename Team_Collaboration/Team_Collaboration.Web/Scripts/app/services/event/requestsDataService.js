﻿(function () {
    'use strict';
    var services = angular.module('services');

    services.factory('Invitations', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/Requests/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false
                },

                incoming: {
                    method: 'GET',
                    isArray: false,
                    url: common.host + '/resource/api/Requests/Incoming/:id'
                },

                outgoing: {
                    method: 'GET',
                    isArray: false,
                    url: common.host + '/resource/api/Requests/Outgoing/:id'
                },

                accept: {
                    method: 'POST',
                    url: common.host + '/resource/api/Requests/Accept'
                },

                decline: {
                    method: 'POST',
                    url: common.host + '/resource/api/Requests/Decline'
                },

                save: {
                    method: 'POST'
                },

                remove: {
                    method: 'DELETE'
                },

                update: {
                    method: 'PUT'
                }
            });
        }]);
})();