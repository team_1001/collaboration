﻿(function () {
    'use strict';
    var services = angular.module('services');

    services.factory('Dialogs', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/Dialogs/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });
        }]);
})();

