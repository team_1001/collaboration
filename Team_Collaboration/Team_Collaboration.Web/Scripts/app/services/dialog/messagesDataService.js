﻿(function () {
    'use strict';
    var services = angular.module('services');

    services.factory('MessagesData', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/Messages/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false,
                    url: common.host + '/resource/api/Messages/:id?page=:page&pageSize=:pageSize',
                    params: {
                        id: "@id",
                        page: '@page',
                        pageSize: '@pageSize'
                    }
                },

                count: {
                    method: 'GET',
                    isArray: false,
                    url: common.host + '/resource/api/Messages/missed/:id'
                }
            });
        }]);
})();

