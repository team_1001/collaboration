﻿(function () {
    'use strict';
    var services = angular.module('services');

    services.factory('Messages', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/Messages/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });
        }]);
})();