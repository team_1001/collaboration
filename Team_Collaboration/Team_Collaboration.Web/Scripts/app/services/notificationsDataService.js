﻿(function () {
    'use strict';
    var services = angular.module('services');

    services.factory('NotificationsData', ['$resource', 'common',
        function ($resource, common) {
            return $resource(common.host + '/resource/api/Notifications/:id', { id: "@id" }, {
                query: {
                    method: 'GET',
                    isArray: false,
                    cache: true,
                    url: common.host + '/resource/api/Notifications?page=:page&pageSize=:pageSize&:types',
                    params: {
                        page: '@page',
                        pageSize: '@pageSize',
                        types: '@types'
                    }
                },

                update: {
                    method: 'POST'
                },

                updateOfType: {
                    method: 'POST'
                }
            });
        }]);
})();