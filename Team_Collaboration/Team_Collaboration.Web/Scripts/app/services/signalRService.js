﻿(function() {
    'use strict';
    var signalR = angular.module('signalr', []);
    signalR.factory('SignalR', ['$', '_', 'common', 'localStorageService', 'EventHandlerFactory', function ($, _, common, $localStorage, EventHandlerFactory) {
        var PING_INTERVAL = 60000,
            RECONNECT_INTERVAL = 5000,
            callsQueue = [];

        var connectedHandlers = [];

        var EventHandler = function() {
            this.counter = 0;
            this.handlers = [];
            this.additionalData = {};
        };

        EventHandler.prototype.add = function (handler) {
            var id = ++this.counter;
            this.handlers.push({
                handler: handler,
                id: id
            });

            return id;
        };

        EventHandler.prototype.remove = function(handlerId) {
            var handler = _.find(this.handlers, function(h) { return h.id === handlerId; });
            if (handler)
                _.remove(this.handlers, handler);
        };

        var eventHandlers = {
            pingBack: EventHandlerFactory.create(),
            messageReceived: EventHandlerFactory.create(),
            messageDelivered: EventHandlerFactory.create(),
            messageSent: EventHandlerFactory.create(),
            onEventCreated: EventHandlerFactory.create(),
            onInvitationCreated: EventHandlerFactory.create(),
            onRequestCreated: EventHandlerFactory.create(),
            needUpdateDashboardInfo: EventHandlerFactory.create(),
            userStateChanged: EventHandlerFactory.create(),
            updateOnlineUsersCount: EventHandlerFactory.create(),
            updateUserStatistics: EventHandlerFactory.create(),
            updateProductStatistics: EventHandlerFactory.create(),
            updateActivityLog: EventHandlerFactory.create(),
            updateSalesStatistics: EventHandlerFactory.create()
        };

        var currentState,
            pingIntervalId,
            tryingToReconnect = false;

        // setup
        var connection = $.hubConnection(common.host + '/hub/signalr', { useDefaultPath: false });
        var hub = connection.createHubProxy('chathub');

        //$.connection.transports.longPolling.supportsKeepAlive = function () {
        //    return false;
        //}

        hub.logging = true;

        var getStateString = function(state) {
            switch (state) {
            case $.signalR.connectionState.connected:
                return "Connected";
            case $.signalR.connectionState.connecting:
                return "Connecting...";
            case $.signalR.connectionState.disconnected:
                return "Disconnected";
            case $.signalR.connectionState.reconnecting:
                return "Reconnecting...";
            }

            return "UNKNOWN [" + state + "]";
        };

        var queueOrCall = function(serverCall) {
            if (currentState !== $.signalR.connectionState.connected) {
                callsQueue.push(serverCall);

                return;
            }

            serverCall();
        }

        //start the connection
        var start = function () {
            if (currentState === $.signalR.connectionState.connected) {
                console.log('already connected');
                return;
            }

            connection.start({ waitForPageLoad: false })
                .done(function () {
                    for (let i = 0; i < connectedHandlers.length; i++) {
                        connectedHandlers[i]();
                    }

                    while (callsQueue.length) {
                        var call = callsQueue.shift();
                        call();
                    }

                    if (pingIntervalId)
                        clearInterval(pingIntervalId);

                    pingIntervalId = setInterval(function () {
                        if (connection.state !== $.signalR.connectionState.connected)
                            return;

                        hub.invoke('Ping');
                    }, PING_INTERVAL);
                }).fail(function () {
                    console.log("[" + new Date() + "] SignalR: Failed to connect to the server");
                });
        };

        connection.error(function (error) {
            console.log("[" + new Date() + "] SignalR: Error occured - " + error);
        });

        connection.reconnecting(function () {
            console.log("[" + new Date() + "] SignalR: Reconnecting...");
            tryingToReconnect = true;
        });

        connection.reconnected(function () {
            console.log("[" + new Date() + "] SignalR: Reconnected...");
            tryingToReconnect = false;
        });

        connection.stateChanged(function (state) {
            currentState = state.newState;

            console.log("[" +
                new Date() +
                "] SignalR: State Changed " +
                getStateString(state.oldState) +
                " --> " +
                getStateString(state.newState));
        });

        connection.disconnected(function () {
            if (connection.lastError) {
                console.error("[" + new Date() + "] SignalR: Error: " + connection.lastError.message);
            }

            if (!tryingToReconnect) {
                if (pingIntervalId) {
                    clearInterval(pingIntervalId);
                    pingIntervalId = null;
                }

                setTimeout(function () {
                    //todo
                    start();
                }, RECONNECT_INTERVAL);
            }
        });

        var getFunctionForEvent = function(eventName) {
            return function() {
                var handlers = eventHandlers[eventName].handlers;

                for (var i = 0; i < handlers.length; i++) {
                    handlers[i].handler.apply(this, arguments);
                }
            }
        }

        //subscribe for all events
        for (var eventName in eventHandlers) {
            if (eventHandlers.hasOwnProperty(eventName)) {
                hub.on(eventName, getFunctionForEvent(eventName));
            }
        }

        return {
            start: start,

            onMessageReceived: function () {
                return eventHandlers["messageReceived"];
            },

            onMessageDelivered: function () {
                return eventHandlers["messageDelivered"];
            },

            onMessageSent: function () {
                return eventHandlers["messageSent"];
            },

            onEventCreated: function () {
                return eventHandlers["onEventCreated"];
            },

            onInvitationCreated: function () {
                return eventHandlers["onInvitationCreated"];
            },

            onRequestCreated: function () {
                return eventHandlers["onRequestCreated"];
            },

            readMessage: function(message) {
                queueOrCall(function () {
                    /// <summary>Update user state</summary>
                    /// <param name="user" type="Object"></param>
                    hub.invoke('ReceiveMessage', message);
                });
            },

            sendMessage: function(message) {
                queueOrCall(function () {
                    /// <summary>Update user state</summary>
                    /// <param name="user" type="Object"></param>
                    hub.invoke('SendMessage', message);
                });
            },

            onUserStateChanged: function (func) {
                /// <summary>Registers handler for userStateChanged event</summary>
                /// <param name="func" type="Function">Handler for event</param>
                return eventHandlers["userStateChanged"].add(func);
            },

            //onNeedUpdateDashboardInfo: function(func) {
            //    /// <summary>Registers handler for needUpdateDashboardInfo event</summary>
            //    /// <param name="func" type="Function">Handler for event</param>
            //    eventHandlers["needUpdateDashboardInfo"].push(func);
            //},
            
            //onUpdateOnlineUsersCount: function(func) {
            //    /// <summary>Registers handler for updateOnlineUsersCount event</summary>
            //    /// <param name="func" type="Function">Handler for event</param>
            //    eventHandlers["updateOnlineUsersCount"].push(func);
            //},
            onConnected: function(handler) {
                connectedHandlers.push(handler);

                queueOrCall(handler);
            },

            updateUserState: function(user) {
                queueOrCall(function() {
                    /// <summary>Update user state</summary>
                    /// <param name="user" type="Object"></param>
                    hub.server.updateUserState(user);
                });
            },
            getUsersOnline: function() {
                queueOrCall(function() {
                    hub.server.getUsersOnline();
                });
            },
            getUserState: function() {
                queueOrCall(function() {
                    hub.server.getUserState();
                });
            },
            //onUpdateUserStatistics: function(func) {
            //    /// <summary>Registers handler for updateUserStatistics event</summary>
            //    /// <param name="func" type="Function">Handler for event</param>
            //    eventHandlers["updateUserStatistics"].push(func);
            //},
            //onUpdateProductStatistics: function(func) {
            //    /// <summary>Registers handler for updateProductStatistics event</summary>
            //    /// <param name="func" type="Function">Handler for event</param>
            //    eventHandlers["updateProductStatistics"].push(func);
            //},
            //onUpdateActivityLog: function(func) {
            //    /// <summary>Registers handler for updateActivityLog event</summary>
            //    /// <param name="func" type="Function">Handler for event</param>
            //    eventHandlers["updateActivityLog"].push(func);
            //},
            //onUpdateSalesStatistics: function(func) {
            //    /// <summary>Registers handler for updateSalesStatistics event</summary>
            //    /// <param name="func" type="Function">Handler for event</param>
            //    eventHandlers["updateSalesStatistics"].push(func);
            //}
        }
    }]);
})();