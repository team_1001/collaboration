﻿(function () {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('DialogCtrl', ['$scope', '$stateParams', '_', 'SignalR', 'MessagesData', 'localStorageService', '$sce',
        function ($scope, $stateParams, _, SignalR, Messages, $localStorage, $sce) {
            var authData = $localStorage.get('authData') || {};

            var id = 0,
                onMessageReceivedId = null,
                onMessageSentId = null,
                onMessageDeliveredId = null,
                currentPage = 1,
                dialogId = $stateParams.id;
            
            $scope.messages = [];
            $scope.message = {};
            $scope.hasMoreItems = false;
            $scope.dialogName = $stateParams.name;

            var prepareMessage = function (message) {
                Object.defineProperty(message, 'html', {
                    enumerable: true,
                    get: function () {
                        return $sce.trustAsHtml((this.content || '').replace('\n', '<br/>'));
                    }
                });

                Object.defineProperty(message, 'date', {
                    enumerable: true,
                    get: function () {
                        return new Date(this.timeStamp).toDateString();
                    }
                });

                message.scrolled = false;
                return message;
            };

            var read = function (messages) {
                messages.forEach(function (m) {
                    SignalR.readMessage(m);
                    m.missed = false;
                    $scope.$emit('messageReceived', m);
                });
            };

            var getMessages = function(options) {
                Messages.query(angular.extend({ id: $stateParams.id }, options))
                    .$promise
                    .then(function(data) {
                        $scope.messages.unshift.apply($scope.messages,
                            _.chain(data.messages.items.map(function(m) {
                                return prepareMessage(angular.extend(m, {
                                    isSelf: +authData.userId === m.senderId,
                                    animate: false
                                }));
                            })).concat($scope.messages)
                            .uniqWith(function(left, right) { return left.id === right.id; })
                            .value());

                        $scope.hasMoreItems = $scope.messages.length < data.messages.total;
                        read(_.filter($scope.messages, function(m) { return m.missed === true; }));
                    });
            };


            var init = function () {
                $scope.message = prepareMessage({
                    senderName: authData.userName,
                    content: "",
                    dialogId: $stateParams.id,
                    missed: false,
                    animate: true,
                    isSelf: true,
                    type: 1
                });

                getMessages({ page: currentPage++, pageSize: 50 });

                onMessageSentId = SignalR.onMessageSent().add(function (messageInfo) {
                    if (messageInfo.dialogId !== dialogId)
                        return;

                    var message = _.find($scope.messages, function (m) { return m.id === messageInfo.id && m.dialogId === messageInfo.dialogId; });
                    if (message) {
                        message.id = messageInfo.newId;
                    }
                });

                onMessageReceivedId = SignalR.onMessageReceived().add(function (message) {
                    if (message.dialogId !== dialogId)
                        return;

                    message.missed = true;
                    message.animate = true;

                    $scope.messages.push(prepareMessage(message));

                    read([message]);
                    $scope.$apply();
                });

                onMessageDeliveredId = SignalR.onMessageDelivered().add(function (messageInfo) {
                    if (messageInfo.dialogId !== dialogId)
                        return;

                    var message = _.find($scope.messages, function (m) { return m.id === messageInfo.id && m.dialogId === messageInfo.dialogId; });
                    if (message) {
                        message.viewed = true;
                        $scope.$apply();
                    }
                });
            };

            $scope.send = function () {
                if (!$scope.message.content || $scope.message.content === '')
                    return;

                var message = $scope.message;
                message.timeStamp = new Date().getTime();
                message.id = ++id;

                $scope.messages.push(angular.extend({}, message));
                SignalR.sendMessage(message);

                $scope.message.content = '';
            };

            $scope.onKeyPress = function (e) {
                if ((e.ctrlKey || e.metaKey) && (e.keyCode === 13 || e.keyCode === 10)) {
                    $scope.message.content += '\n';
                    return;
                }

                if (e.keyCode === 13) {
                    e.preventDefault();
                    $scope.send();
                }
            };

            $scope.isLastMessage = function(message) {
                var existing = _.find($scope.messages, function (m) { return m.id === message.id; });
                return existing && $scope.messages.indexOf(existing) === ($scope.messages.length - 1);
            };

            $scope.getMessages = function () {
                getMessages({ page: currentPage++, pageSize: 50 });
            };

            $scope.$on('$destroy', function () {
                SignalR.onMessageReceived().remove(onMessageReceivedId);
                SignalR.onMessageSent().remove(onMessageSentId);
                SignalR.onMessageDelivered().remove(onMessageDeliveredId);
            });

            init();
        }]);
})();
