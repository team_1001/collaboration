﻿(function () {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('DialogsCtrl', ['$scope', '_', '$stateParams', '$state', 'Dialogs', 'SignalR', '$sce',
        function ($scope, _, $stateParams, $state, Dialogs, SignalR, $sce) {
            var onMessageReceivedId = null;
            $scope.dialogs = [];

            var prepareMessage = function (message) {
                Object.defineProperty(message, 'html', {
                    enumerable: true,
                    get: function () {
                        return $sce.trustAsHtml((this.content || '').replace('\n', '<br/>'));
                    }
                });

                return message;
            };

            $scope.openDialog = function (id) {
                var dialog = _.find($scope.dialogs, function (d) { return d.id === id; });
                dialog.isActive = true;
                $state.go(Constants.States.Main.Dialogs.View.State, { id: id, name: dialog.fullName });
            };

            $scope.$on('messageReceived', function (e, message) {
                var dialog = _.find($scope.dialogs, function (d) { return d.id === message.dialogId; });
                if (dialog) {
                    if (dialog.lastMessage)
                        dialog.lastMessage.content = message.content;
                    else {
                        dialog.lastMessage = prepareMessage(message);
                    }
                    
                    dialog.unreadCount -= 1;
                }
            });

            onMessageReceivedId = SignalR.onMessageReceived().add(function (message) {
                // todo probably not the best solution for checking if current dialog is opened...
                //if (SignalR.onMessageReceived().handlers.length > 2)
                //    return;

                var dialog = _.find($scope.dialogs, function (d) { return d.id === message.dialogId; });
                if (dialog) {
                    dialog.unreadCount += 1;
                }

                $scope.$apply();
            });

            Dialogs.query()
                .$promise
                .then(function(data) {
                    $scope.dialogs.push.apply($scope.dialogs, data.messages.map(function(m) {
                        if (m.lastMessage)
                            prepareMessage(m.lastMessage);

                        return m;
                    }));
                });

            $scope.$on('$destroy', function () {
                SignalR.onMessageReceived().remove(onMessageReceivedId);
            });
        }]);
})();
