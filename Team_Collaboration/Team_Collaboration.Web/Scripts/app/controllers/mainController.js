﻿(function () {
    'use strict';

    var controllers = angular.module('controllers', ['uiGmapgoogle-maps', 'ngGeolocation', 'ngFileUpload']);

    controllers.config([
        'uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
            GoogleMapApi.configure({
                key: Constants.GoogleApiKey,
                libraries: 'places,weather,geometry,visualization'
            });
        }
    ]);

    controllers.controller('MainCtrl', ['$scope', '_', '$state', '$stateParams', 'MessagesData', 'NotificationsData', 'SignalR', 'ngAudio', 'ParamSerializer',
        function ($scope, _, $state, $stateParams, MessagesData, NotificationsData, SignalR, ngAudio, ParamSerializer) {
            var onMessageReceivedId = null,
                onEventCreatedId = null,
                onInvitationCreatedId = null,
                messageNotificationTimeoutId = null;

            $scope.sound = ngAudio.load("Content/Sounds/job-done.mp3");

            $scope.items = {
                messages: 0,
                events: 0,
                invitations: 0
            };

            onMessageReceivedId = SignalR.onMessageReceived().add(function (message) {
                $scope.items.messages += 1;
                $scope.$apply();
                if (messageNotificationTimeoutId)
                    return;

                //$scope.sound.play();

                messageNotificationTimeoutId = setTimeout(function () {
                    messageNotificationTimeoutId = null;
                }, 300);
            });

            onEventCreatedId = SignalR.onEventCreated().add(function (eventData) {
                $scope.items.events += 1;
                $scope.$apply();
            });

            onInvitationCreatedId = SignalR.onInvitationCreated().add(function (invitationData) {
                $scope.items.invitations += 1;
                $scope.$apply();
            });

            MessagesData.count()
                .$promise
                .then(function(data) {
                    $scope.items.messages = data.count;
                });

            var types = ParamSerializer.serialize({
                name: "types",
                values: _.values(Constants.NotificationTypes)
            });

            NotificationsData.query({ types: types, page: 0, pageSize: 0 })
                .$promise
                .then(function(data) {
                    $scope.items.events = data.items.filter(function(x) {
                        return x.type === Constants.NotificationTypes.eventCreated;
                    }).length;

                    $scope.items.invitations = data.items.filter(function(x) {
                        return x.type === Constants.NotificationTypes.invitationCreated;
                    }).length;
                });

            $scope.isActive = function (state) {
                return $state.includes(state);
            };

            $scope.$on('messageReceived', function() {
                $scope.items.messages -= 1;
            });

            $scope.$on('eventsReceived', function (e, count) {
                $scope.items.events -= count;
            });

            $scope.$on('invitationsReceived', function (e, count) {
                $scope.items.invitations -= count;
            });

            $scope.$on('$destroy', function () {
                SignalR.onMessageReceived().remove(onMessageReceivedId);
                SignalR.onEventCreated().remove(onEventCreatedId);
                SignalR.onInvitationCreated().remove(onInvitationCreatedId);
            });
        }]);
})();
