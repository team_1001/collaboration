﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('MyEventsCtrl', [
        '$scope', 'UserEvents', '$stateParams', '$state', 'ParamSerializer',
        function ($scope, UserEvents, $stateParams, $state, ParamSerializer) {
            $scope.events = [];

            $scope.remove = function (id) {
                var events = this.events.filter(function (ev) { return ev.id === +id });
                if (!events.length)
                    return;

                this.events.splice(this.events.indexOf(events[0]), 1);
            };

            $scope.add = function (event) {
                this.events.push(event);
            };

            $scope.update = function (event) {
                var events = this.events.filter(function (ev) { return ev.id === event.id });
                if (!events.length)
                    return;

                $.extend(events[0], event);
            };

            $scope.view = function (eventId) {
                $state.go(Constants.States.Main.Events.My.View.State, { id: eventId });
            };

            var roles = ParamSerializer.serialize({
                name: "roles",
                values: _.values(Constants.Roles)
            });

            UserEvents.query({ roles : roles }).$promise
                .then(function(data) {
                    $scope.events.push.apply($scope.events, data.events.map(function(event) {
                        return {
                            name: event.name,
                            index: event.id,
                            id: event.id,
                            userId: $stateParams.userId
                        }
                    }));
                }).catch(function(e) {
                    console.log(e);
                });
        }
    ]);
})();