﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('EventsCtrl', [
        '$scope', '$state', 'SignalR',
        function ($scope, $state, SignalR) {
            var onInvitationCreatedId = null;

            $scope.invitations = 0;

            onInvitationCreatedId = SignalR.onInvitationCreated().add(function (invitationData) {
                $scope.invitations += 1;
                $scope.$apply();
            });

            $scope.isActive = function(state) {
                return $state.includes(state);
            };

            $scope.$on('invitationsReceived', function () {
                $scope.invitations = 0;
            });

            $scope.$on('$destroy', function () {
                SignalR.onInvitationCreated().remove(onInvitationCreatedId);
            });
        }
    ]);
})();