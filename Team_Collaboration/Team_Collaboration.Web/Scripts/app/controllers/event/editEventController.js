﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('EventEditCtrl', [
        '$scope', 'Events', '$stateParams', '$state', '$uibModal', '$q',
        function ($scope, Events, $stateParams, $state, $uibModal, $q) {
            $scope.loading = true;
            $scope.action = "Update";

            // TODO fix angular datepicker onfocus undefined issue (low)
            // TODO refactor into shared class
            $scope.event = {
                errors: []
            };

            var $categories = Events.categories()
                .$promise
                .then(function (data) {
                    $scope.categories.push.apply($scope.categories, data.categories);
                    $scope.eventTypes.push.apply($scope.eventTypes, data.types);
                }).catch(function (e) {
                    console.log(e);
                });

            var $event = Events.get({ id: $stateParams.id })
                .$promise
                .then(function (data) {
                    angular.merge($scope.event, data.event, {
                        start: new Date(data.event.dateStartsTicks),
                        end: new Date(data.event.dateEndsTicks),
                        location: {
                            marker: data.event.location.coordinates ? {
                                latitude: +data.event.location.coordinates.split(';')[0],
                                longitude: +data.event.location.coordinates.split(';')[1]
                            } : null
                        }
                    });

                    Object.defineProperty($scope.event, "dateStartsTicks", {
                        enumerable: true,
                        get: function () {
                            return this.start.getTime();
                        }
                    });

                    Object.defineProperty($scope.event, "dateEndsTicks", {
                        enumerable: true,
                        get: function () {
                            return this.end.getTime();
                        }
                    });

                    Object.defineProperty($scope.event.location, "coordinates", {
                        enumerable: true,
                        get: function () {
                            return this.marker ? this.marker.latitude + ';' + this.marker.longitude : null;
                        }
                    });

                    Object.defineProperty($scope.event.location, "coordinatesDisplay", {
                        enumerable: true,
                        get: function () {
                            return this.marker ? this.marker.latitude.toFixed(2) + ';' + this.marker.longitude.toFixed(2) : null;
                        }
                    });
                }).catch(function () {
                    console.log('failed');
                });

            $q.all([$categories, $event]).then(function () {
                $scope.loading = false;
            });

            $scope.dates = {
                start: false,
                end: false
            };

            $scope.currentPosition = null;
            $scope.categories = [];
            $scope.eventTypes = [];

            $scope.openDate = function (date) {
                $scope.dates[date] = true;
            }

            $scope.pickLocation = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: 'views/location.html',
                    controller: 'LocationCtrl',
                    resolve: {
                        location: function () {
                            return {
                                marker: $scope.event.location.marker
                            };
                        }
                    }
                });

                modalInstance.result.then(function (marker) {
                    console.log('done');
                    if (marker) {
                        $scope.event.location.marker = {
                            latitude: marker.position.lat(),
                            longitude: marker.position.lng()
                        };
                    }
                }, function () {
                    console.log('canceled');
                });
            };

            $scope.save = function () {
                $scope.event.errors.length = 0;

                Events.update($scope.event)
                    .$promise
                    .then(function (data) {
                        $scope.$parent.update(data);
                        //clear cache
                        Events.reset(data.id);
                        console.log('done');
                        $state.go('^');
                    }).catch(function (result) {
                        if (result.data && result.data.errors)
                            $scope.event.errors.push.apply($scope.event.errors, result.data.errors);
                    });
            };
        }
    ]);
})();