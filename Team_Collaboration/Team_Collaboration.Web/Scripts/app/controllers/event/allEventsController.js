﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('AllEventsCtrl', [
        '$scope', '$state', 'UserEvents', '$httpParamSerializerJQLike',
        function ($scope, $state, UserEvents, $httpParamSerializerJQLike) {
            $scope.events = [];

            var roles = [{ roles: Constants.Roles.host }, { roles: Constants.Roles.admin }, { roles: Constants.Roles.user }]
                .map(function (x) { return $httpParamSerializerJQLike(x); })
                .join("&");

            $scope.view = function (eventId) {
                $state.go(Constants.States.Main.Events.All.View.State, { id: eventId });
            };

            UserEvents.query({ roles: roles }).$promise
                .then(function(data) {
                    $scope.events.push.apply($scope.events, data.events.map(function(event) {
                        return {
                            name: event.name,
                            id: event.id
                        }
                    }));
                }).catch(function(e) {
                    console.log(e);
                });
        }
    ]);
})();