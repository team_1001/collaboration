﻿(function () {
    'use strict';

    /* Controllers */

    var controllers = angular.module('controllers');

    controllers.controller('FeedGeoCtrl', [
        '$scope', '$state', 'Events', '$geolocation', '$timeout', 'uiGmapGoogleMapApi', 'uiGmapIsReady', '$q',
        function ($scope, $state, Events, $geolocation, $timeout, uiGmapGoogleMapApi, uiGmapIsReady, $q) {
            $scope.loading = true;

            $scope.map = {
                zoom: 8
            };

            var getCurrentPosition = function() {
                if ($geolocation.position.coords)
                    return $q.resolve($geolocation.position.coords);

                return $geolocation.getCurrentPosition({
                    timeout: 60000
                }).then(function(position) {
                    return position.coords;
                });
            };

            var currentState = $state.current.name,
                $position = getCurrentPosition(),
                $geo = $position.then(function (coords) {
                    return Events.geo({
                        radius: 250.0,
                        latitude: coords.latitude.toFixed(2),
                        longitude: coords.longitude.toFixed(2)
                    }).$promise;
                });

            var init = function () {
                $q.all([$position, uiGmapGoogleMapApi, $geo]).then(function (values) {
                    var coords = values[0],
                        maps = values[1],
                        events = values[2].items;

                    angular.extend($scope.map, {
                        center: {
                            latitude: coords.latitude,
                            longitude: coords.longitude
                        }
                    });

                    $scope.loading = false;

                    console.log('maps loaded...');

                    uiGmapIsReady.promise(1).then(function (instances) {
                        function placeMarker(options, map) {

                            var marker = new maps.Marker({
                                position: options.position,
                                map: map,
                                title: options.title
                            });

                            marker.addListener('click', function (e) {
                                $state.go(Constants.States.Main.Feed.Geo.View.State, { id: options.id });
                            });

                            map.panTo(options.position);
                        }

                        // workaround to avoid https://github.com/allenhwkim/angularjs-google-maps/issues/88
                        var map = instances[0].map;
                        $timeout(function () {
                            var center = map.getCenter();

                            maps.event.trigger(map, 'resize');
                            map.setCenter(center);

                            events.forEach(function (e) {
                                placeMarker({
                                    position: {
                                        lat: e.location.latitude,
                                        lng: e.location.longitude
                                    },

                                    title: e.name,
                                    id: e.id
                                }, map);
                            });
                        }, 100);
                    });
                });
            };

            $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (toState.name === currentState)
                    init();
            });

            init();
        }
    ]);
})();