﻿(function () {
    'use strict';

    /* Controllers */

    var controllers = angular.module('controllers');

    controllers.controller('FeedListCtrl', ['$scope', '_', '$state', 'Events', 'NotificationsData', '$stateParams', '$q', 'SignalR', 'ParamSerializer',
        function ($scope, _, $state, Events, NotificationsData, $stateParams, $q, SignalR, ParamSerializer) {
            var pageSize = 50,
                page = 1,
                onEventCreatedId = null,
                $currentRequest = null;

            var loadEvents = function (options) {
                var createRequest = function (options) {
                    if (options.eventId && _.find($scope.events, function(e) { return e.id === options.eventId }))
                        return $q.resolve({ items: [], total: $scope.events.length });

                    return Events.query(options).$promise.then(function (data) {
                        if (options.reset === true)
                            page = 1;

                        var difference = _.chain(data.items.map(function(event) {
                            return {
                                name: event.name,
                                id: event.id,
                                date: event.dateStartsTicks
                            };
                        })).differenceBy($scope.events, 'id').orderBy(["id"], ["desc"]).value();

                        if (options.reverse === true)
                            $scope.events.unshift.apply($scope.events, _.reverse(difference));
                        else
                            $scope.events.push.apply($scope.events, difference);

                        $scope.hasMoreItems = $scope.events.length < data.total;
                        if ($scope.hasMoreItems)
                            page++;

                        return $scope.events;
                    });
                };

                return $currentRequest = !$currentRequest
                    ? createRequest(options)
                    : $currentRequest.then(function() {
                        return createRequest(angular.extend(options, {
                            lastId: $scope.events.length ? $scope.events[$scope.events.length - 1].id : null
                        }));
                    });
            };

            var init = function () {
                onEventCreatedId = SignalR.onEventCreated().add(function (eventData) {
                    loadEvents({
                        page: 1, // start over, with the new id
                        pageSize: pageSize,
                        reset: true,
                        eventId: eventData.id,
                        reverse: true
                    });

                    NotificationsData.update({ id: eventData.notificationId })
                        .$promise
                        .then(function () {
                            $scope.$emit('eventsReceived', 1);
                        });
                });

                var types = ParamSerializer.serialize({
                    name: "types",
                    values: _.values(Constants.NotificationTypes)
                });

                NotificationsData.query({ types: types, page: 0, pageSize: 0 })
                    .$promise
                    .then(function(data) {
                        return data.items.filter(function (x) {
                            return x.type === Constants.NotificationTypes.eventCreated;
                        }).length;
                    }).then(function(length) {
                        if (length === 0)
                            return $q.reject();

                        return NotificationsData.updateOfType([Constants.NotificationTypes.eventCreated]).$promise;
                    }).then(function() {
                        console.log('updated');
                    });
                
                loadEvents({ page: page, pageSize: pageSize, reset: false }).then(function(events) {
                    $scope.$emit('eventsReceived', events.length);
                });
            };

            $scope.hasMoreItems = true;

            $scope.events = [];

            $scope.remove = function (id) {
                var events = this.events.filter(function (ev) { return ev.id === +id });
                if (!events.length)
                    return;

                this.events.splice(this.events.indexOf(events[0]), 1);
            };

            $scope.add = function (event) {
                this.events.push(event);
            };

            $scope.view = function(eventId) {
                $state.go(Constants.States.Main.Feed.List.View.State, { id: eventId });
            };

            $scope.$on('$destroy', function () {
                SignalR.onEventCreated().remove(onEventCreatedId);
            });

            init();
        }]);
})();