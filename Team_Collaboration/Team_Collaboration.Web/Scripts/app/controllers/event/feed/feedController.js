﻿(function () {
    'use strict';

    /* Controllers */

    var controllers = angular.module('controllers');

    controllers.controller('FeedCtrl', [
        '$scope', '$state',
        function($scope, $state) {
            $scope.isActive = function(state) {
                return $state.includes(state);
            };
        }
    ]);
})();