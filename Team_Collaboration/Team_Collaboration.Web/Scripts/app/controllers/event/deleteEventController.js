﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('EventDeleteCtrl', [
        '$scope', 'Events', '$uibModalInstance', 'id',
        function ($scope, Events, $uibModalInstance, id) {

            $scope.save = function () {
                Events.remove({ id: id })
                    .$promise
                    .then(function () {
                        $uibModalInstance.close();
                    }).catch(function () {
                        console.log('failed');
                        $uibModalInstance.dismiss();
                    });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();