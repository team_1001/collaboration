﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('RequestsCtrl', [
       '$scope', '$state',
        function($scope, $state) {
            $scope.isActive = function(state) {
                return $state.includes(state);
            }
        }
    ]);
})();