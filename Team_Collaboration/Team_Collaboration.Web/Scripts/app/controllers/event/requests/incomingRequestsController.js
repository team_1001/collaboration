﻿(function () {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('IncomingRequestsCtrl', ['$scope', '_', '$stateParams', '$state', 'ResourceFactory', 'SignalR',
        function ($scope, _, $stateParams, $state, ResourceFactory, SignalR) {
            var Requests = ResourceFactory.requests(),
                page = 1,
                pageSize = 50,
                $currentRequest = null,
                onRequestCreatedId;

            $scope.requests = [];

            $scope.hasMoreItems = true;

            var loadRequests = function (options) {
                var createRequest = function (options) {
                    return Requests.incoming(options)
                        .$promise.then(function (data) {
                            if (options.reset === true)
                                page = 1;

                            $scope.requests.push.apply($scope.requests, _.chain(data.items.map(function (i) {
                                return {
                                    timeStampTicks: i.timeStampTicks,
                                    eventId: i.eventId,
                                    userId: i.userId
                                };
                            })).differenceWith($scope.invitations, function (arrVal, othVal) {
                                return arrVal.eventId === othVal.eventId && arrVal.userId === othVal.userId;
                            }).sortBy([function (x) { return x.timeStampTicks; }]).value());

                            $scope.hasMoreItems = $scope.requests.length < data.total;
                            if ($scope.hasMoreItems)
                                page++;
                        });
                }

                return $currentRequest = !$currentRequest
                    ? createRequest(options)
                    : $currentRequest.then(function () {
                        return createRequest(angular.extend(options, {
                            timeStamp: $scope.requests.length ? $scope.requests[$scope.requests.length - 1].timeStampTicks : null
                        }));
                    });
            };

            var init = function () {
                onRequestCreatedId = SignalR.onRequestCreated().add(function (requestData) {
                    loadRequests({
                        page: 1, // start over, with the new id
                        pageSize: pageSize,
                        reset: true
                    });
                });

                loadRequests({ page: page, pageSize: pageSize, reset: false });
            };

            $scope.accept = function (request) {
                Requests.accept(request)
                    .$promise
                    .then(function (data) {
                        _.remove($scope.requests, request);
                        console.log('success');
                    }).catch(function () {
                        console.log('failed');
                    });
            };

            $scope.decline = function (request) {
                Requests.decline(request)
                    .$promise
                    .then(function (data) {
                        _.remove($scope.requests, requests);
                        console.log('success');
                    }).catch(function () {
                        console.log('failed');
                    });
            };

            $scope.$on('$destroy', function () {
                SignalR.onRequestCreated().remove(onRequestCreatedId);
            });

            init();
        }]);
})();
