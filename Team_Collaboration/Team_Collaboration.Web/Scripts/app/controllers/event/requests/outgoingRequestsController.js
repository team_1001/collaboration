﻿(function () {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('OutgoingRequestsCtrl', ['$scope', '_', '$stateParams', '$state', 'ResourceFactory',
        function ($scope, _, $stateParams, $state, ResourceFactory) {
            var Requests = ResourceFactory.requests(),
                page = 1,
                pageSize = 50;;

            $scope.requests = [];

            $scope.cancel = function (request) {
                Requests.remove(request)
                    .$promise
                    .then(function (data) {
                        console.log('success');
                        _.remove($scope.requests, request);
                    }).catch(function() {
                        console.log('failed');
                    });
            };

            Requests.outgoing({ page: page, pageSize: pageSize })
                .$promise
                .then(function (data) {
                    $scope.requests.push.apply($scope.requests, data.items);
                }).catch(function () {
                    console.log('failed');
                });
        }]);
})();
