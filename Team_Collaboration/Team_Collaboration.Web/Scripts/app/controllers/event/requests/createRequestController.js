﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('CreateRequestCtrl', [
        '$scope', 'ResourceFactory', 'Users', '$stateParams', '$state', '$uibModal', '$q', '_',
        function ($scope, ResourceFactory, Users, $stateParams, $state, $uibModal, $q, _) {
            var Requests = ResourceFactory.requests();

            $scope.users = [];

            $scope.noneIsChecked = true;

            var init = function () {
                var $users = Users.query()
                    .$promise
                    .catch(function (e) {
                        console.log(e);
                    }).then(function (data) {
                        return data.items
                            .filter(function (u) { return u.isSelf === false; })
                            .map(function (u) { return angular.extend(u.user, { isChecked: false }); });
                    });

                var $requests = Requests.outgoing({ id: $stateParams.id })
                    .$promise
                    .catch(function (e) {
                        console.log(e);
                    }).then(function (data) {
                        return data.items;
                    });

                $q.all([$users, $requests]).then(function (result) {
                    $scope.users.push.apply($scope.users, _.differenceBy(result[0], result[1].map(function (i) {
                        return { id: i.invitedUserId };
                    }), function (x) { return x.id; }));
                });
            };

            $scope.check = function () {
                $scope.noneIsChecked = $scope.users.filter(function (u) { return u.isChecked === true }).length === 0;
            };

            $scope.save = function () {
                var data = {
                    eventId: $stateParams.id,
                    userIds: $scope.users
                        .filter(function (u) { return u.isChecked === true; })
                        .map(function (u) { return u.id; })
                };

                Requests.save(data)
                    .$promise
                    .then(function (data) {
                        console.log('done');
                        $state.go('^');
                    }).catch(function () {
                        console.log('failed');
                    });
            };

            init();
        }
    ]);
})();