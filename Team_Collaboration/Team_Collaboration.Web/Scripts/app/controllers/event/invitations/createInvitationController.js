﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('CreateInvitationCtrl', [
        '$scope', 'ResourceFactory', 'Users', 'Events', 'UserEvents', '$stateParams', '$state', '$uibModal', '$q', '_',
        function ($scope, ResourceFactory, Users, Events, UserEvents, $stateParams, $state, $uibModal, $q, _) {
            var Invitations = ResourceFactory.invitations(),
                Requests = ResourceFactory.requests();

            $scope.users = [];

            $scope.noneIsChecked = true;

            var init = function() {
                var $users = Users.query()
                    .$promise
                    .catch(function (e) {
                        console.log(e);
                    }).then(function (data) {
                        return data.items
                            .filter(function (u) { return u.isSelf === false; })
                            .map(function (u) { return angular.extend(u.user, { isChecked: false }); });
                    });

                var $userEvents = UserEvents.participants({ id: $stateParams.id })
                    .$promise
                    .catch(function(e) {
                        console.log(e);
                    }).then(function(data) {
                        return data.events;
                    });

                var $invitations = Invitations.outgoing({ id: $stateParams.id })
                    .$promise
                    .catch(function (e) {
                        console.log(e);
                    }).then(function (data) {
                        return data.items;
                    });

                var $requests = Requests.incoming({ id: $stateParams.id })
                    .$promise
                    .catch(function (e) {
                        console.log(e);
                    }).then(function (data) {
                        return data.items;
                    });

                var $event = Events.query({ id: $stateParams.id })
                    .$promise
                    .catch(function(e) {
                        console.log(e);
                    }).then(function(data) {
                        return data.event;
                    });

                $q.all([$users, $invitations, $requests, $event, $userEvents]).then(function (result) {
                    $scope.users.push.apply($scope.users, _.differenceBy(result[0], result[1].map(function (i) {
                        return { id: i.invitedUserId };
                    }).concat(result[2].map(function (r) { return { id: r.userId }; })).concat([{ id: result[3].hostId }]).concat(result[4].map(function (r) { return { id: r.userId }; })), function (x) { return x.id; }));
                });
            };

            $scope.check = function() {
                $scope.noneIsChecked = $scope.users.filter(function(u) { return u.isChecked === true }).length === 0;
            };

            $scope.save = function () {
                var data = {
                    eventId: $stateParams.id,
                    userIds: $scope.users
                        .filter(function(u) { return u.isChecked === true; })
                        .map(function(u) { return u.id; })
                };

                Invitations.save(data)
                    .$promise
                    .then(function(data) {
                        console.log('done');
                        $state.go('^');
                    }).catch(function() {
                        console.log('failed');
                    });
            };

            init();
        }
    ]);
})();