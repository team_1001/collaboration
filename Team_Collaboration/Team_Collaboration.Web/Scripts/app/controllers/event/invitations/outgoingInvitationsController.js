﻿(function () {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('OutgoingInvitationsCtrl', ['$scope', '_', '$stateParams', '$state', 'ResourceFactory',
        function ($scope, _, $stateParams, $state, ResourceFactory) {
            var Invitations = ResourceFactory.invitations(),
                page = 1,
                pageSize = 50;

            $scope.invitations = [];

            $scope.cancel = function(invitation) {
                console.log(invitation);
            };

            Invitations.outgoing({ page: page, pageSize: pageSize })
                .$promise
                .then(function (data) {
                    $scope.invitations.push.apply($scope.invitations, data.items);
                }).catch(function () {
                    console.log('failed');
                });
        }]);
})();
