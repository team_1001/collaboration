﻿(function () {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('IncomingInvitationsCtrl', ['$scope', '_', '$stateParams', '$state', 'ResourceFactory', 'NotificationsData', 'SignalR', 'ParamSerializer',
        function ($scope, _, $stateParams, $state, ResourceFactory, NotificationsData, SignalR, ParamSerializer) {
            var Invitations = ResourceFactory.invitations(),
                onInvitationCreatedId,
                page = 1,
                pageSize = 50,
                $currentRequest = null;

            $scope.invitations = [];
            $scope.hasMoreItems = true;

            var loadInvitations = function (options) {
                var createRequest = function (options) {
                    if (options.invitation && _.find($scope.invitations, function(i) {
                        return i.eventId === options.invitation.eventId && _.filter(options.invitation.userIds, function (u) { return u === i.invitedUserId; });
                    })) return $q.resolve({ items: [], total: $scope.invitations.length });

                    return Invitations.incoming(_.omit(options, ["invitation"]))
                        .$promise.then(function (data) {
                            if (options.reset === true)
                                page = 1;

                            var difference = _.chain(data.items.map(function(i) {
                                return {
                                    timeStampTicks: i.timeStampTicks,
                                    eventId: i.eventId,
                                    invitedUserId: i.invitedUserId
                                };
                            })).differenceWith($scope.invitations, function(arrVal, othVal) {
                                return arrVal.eventId === othVal.eventId && arrVal.invitedUserId === othVal.invitedUserId;
                            }).orderBy(["timeStampTicks"], ["desc"]).value();

                            if (options.reverse === true)
                                $scope.invitations.unshift.apply($scope.invitations, _.reverse(difference));
                            else {
                                $scope.invitations.push.apply($scope.invitations, difference);
                            }

                            $scope.hasMoreItems = $scope.invitations.length < data.total;
                            if ($scope.hasMoreItems)
                                page++;

                            return $scope.invitations;
                        });
                }

                return $currentRequest = !$currentRequest
                    ? createRequest(options)
                    : $currentRequest.then(function() {
                        return createRequest(angular.extend(options, {
                            timeStampTicks: $scope.invitations.length ? $scope.invitations[$scope.invitations.length - 1].timeStampTicks : null
                        }));
                    });
            };

            var init = function () {
                onInvitationCreatedId = SignalR.onInvitationCreated().add(function (invitationData) {
                    loadInvitations({
                        page: 1, // start over, with the new id
                        pageSize: pageSize,
                        reset: true,
                        reverse: true,
                        invitation: invitationData
                    });

                    NotificationsData.update({ id: invitationData.notificationId })
                        .$promise
                        .then(function() {
                            $scope.$emit('invitationsReceived', 1);
                        });
                });

                var types = ParamSerializer.serialize({
                    name: "types",
                    values: _.values(Constants.NotificationTypes)
                });

                NotificationsData.query({ types: types, page: 0, pageSize: 0 })
                    .$promise
                    .then(function (data) {
                        return data.items.filter(function (x) {
                            return x.type === Constants.NotificationTypes.invitationCreated;
                        }).length;
                    }).then(function (length) {
                        if (length === 0)
                            return $q.reject();

                        return NotificationsData.updateOfType([Constants.NotificationTypes.invitationCreated]).$promise;
                    }).then(function () {
                        console.log('updated');
                    });

                loadInvitations({ page: page, pageSize: pageSize, reset: false }).then(function (invitations) {
                    $scope.$emit('invitationsReceived', invitations.length);
                });;
            };

            $scope.accept = function(invitation) {
                Invitations.accept(invitation)
                    .$promise
                    .then(function (data) {
                        _.remove($scope.invitations, invitation);
                        console.log('success');
                    }).catch(function() {
                        console.log('failed');
                    });
            };

            $scope.decline = function (invitation) {
                Invitations.decline(invitation)
                    .$promise
                    .then(function (data) {
                        _.remove($scope.invitations, invitation);
                        console.log('success');
                    }).catch(function () {
                        console.log('failed');
                    });
            };

            $scope.$on('$destroy', function () {
                SignalR.onInvitationCreated().remove(onInvitationCreatedId);
            });

            init();
        }]);
})();
