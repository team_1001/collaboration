﻿(function() {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('EventViewCtrl', [
        '$scope', 'Users', 'Events', 'UserEvents', 'ResourceFactory', '$stateParams', '$state', '$uibModal', 'SignalR',
        function ($scope, Users, Events, UserEvents, ResourceFactory, $stateParams, $state, $uibModal, SignalR) {
            var Invitations = ResourceFactory.invitations();

            $scope.event = { name: "Event_" + $stateParams.id };

            Events.get({ id: $stateParams.id })
                .$promise
                .then(function(data) {
                    angular.extend($scope.event, data.event, {
                        canEdit: data.isHost === true,
                        canInvite: data.isHost === true || data.isPublic,
                        canLeave: data.isJoined,
                        canJoin: data.isHost === false && data.isJoined === false && data.isRequested === false && data.isInvited === false,
                        canAccept: data.isInvited === true,
                        userId: data.userId
                    });
                }).catch(function() {
                    console.log('failed');
                });

            $scope.edit = function() {
                $state.go(Constants.States.concat($state.get('^').name, Constants.States.Edit.State), { id: $stateParams.id });
            };

            $scope.delete = function() {
                var modalInstance = $uibModal.open({
                    templateUrl: 'views/confirmation.html',
                    controller: 'EventDeleteCtrl',
                    resolve: {
                        id: function() {
                            return $stateParams.id;
                        }
                    }
                });

                modalInstance.result.then(function() {
                    $scope.$parent.remove($stateParams.id);
                    console.log('done');
                    $state.go('^');
                }, function() {
                    console.log('canceled');
                });
            };

            $scope.invite = function() {
                $state.go(Constants.States.concat($state.get('^').name, Constants.States.Invite.State), { id: $stateParams.id });
            };

            $scope.leave = function() {
                UserEvents.remove({ id: $stateParams.id })
                    .$promise
                    .then(function() {
                        console.log('done');
                        Events.reset($stateParams.id);
                        $state.go('^');
                    }).catch(function() {
                        console.log('failed');
                    });
            };

            $scope.join = function () {
                UserEvents.save({ id: $stateParams.id })
                    .$promise
                    .then(function (data) {
                        console.log('done');
                        if (data.request)
                            SignalR.notifyRequestCreated(data.request);

                        Events.reset($stateParams.id);
                        $state.go('^');
                    }).catch(function () {
                        console.log('failed');
                    });
            };

            $scope.accept = function () {
                var invitation = {
                    eventId: $stateParams.id,
                    invitedUserId: $scope.event.userId
                };

                Invitations.accept(invitation)
                    .$promise
                    .then(function (data) {
                        console.log('success');
                        Events.reset($stateParams.id);
                        $state.go('^');
                    }).catch(function () {
                        console.log('failed');
                    });
            };

            $scope.decline = function () {
                var invitation = {
                    eventId: $stateParams.id,
                    invitedUserId: $scope.event.userId
                };

                Invitations.decline(invitation)
                    .$promise
                    .then(function (data) {
                        console.log('success');
                        Events.reset($stateParams.id);
                        $state.go('^');
                    }).catch(function () {
                        console.log('failed');
                    });
            };
        }
    ]);
})();