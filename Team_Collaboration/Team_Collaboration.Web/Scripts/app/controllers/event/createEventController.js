﻿(function() {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('CreateEventCtrl', [
        '$scope', 'Events', '$stateParams', '$state', '$uibModal',
        function ($scope, Events, $stateParams, $state, $uibModal) {
            // TODO fix angular datepicker onfocus undefined issue (low)
            // TODO refactor into shared class
            $scope.action = "Create";

            $scope.event = {
                errors: [],
                name: 'Event_name',
                description: 'Event_description',
                categoryId: null,
                eventType: null,
                start: new Date(),
                end: new Date(),

                get dateStartsTicks() {
                    return this.start.getTime();
                },

                get dateEndsTicks() {
                    return this.end.getTime();
                },

                location: {
                    name: 'some_name',
                    adress: 'some_adress',
                    marker: null,

                    get coordinates() {
                        return this.marker ? this.marker.latitude + ';' + this.marker.longitude : null;
                    },

                    get coordinatesDisplay() {
                        return this.marker ? this.marker.latitude.toFixed(2) + ';' + this.marker.longitude.toFixed(2) : null;
                    }
                }
            };

            $scope.dates = {
                start: false,
                end: false
            };

            $scope.currentPosition = null;
            $scope.categories = [];

            $scope.eventTypes = [];

            $scope.openDate = function (date) {
                $scope.dates[date] = true;
            }

            Events.categories()
                .$promise
                .then(function (data) {
                    $scope.categories.push.apply($scope.categories, data.categories);
                    if ($scope.categories.length)
                        $scope.event.categoryId = $scope.categories[0].id;

                    $scope.eventTypes.push.apply($scope.eventTypes, data.types);
                    if ($scope.eventTypes.length)
                        $scope.event.eventType = $scope.eventTypes[0];

                }).catch(function (e) {
                    console.log(e);
                });

            $scope.pickLocation = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: 'views/location.html',
                    controller: 'LocationCtrl',
                    resolve: {
                        location: function () {
                            return {
                                marker: $scope.event.location.marker
                            };
                        }
                    }
                });

                modalInstance.result.then(function (marker) {
                    console.log('done');
                    if (marker) {
                        $scope.event.location.marker = {
                            latitude: marker.position.lat(),
                            longitude: marker.position.lng()
                        };
                    }
                }, function () {
                    console.log('canceled');
                });;
            };

            $scope.save = function () {
                $scope.event.errors.length = 0;
                Events.save($scope.event)
                    .$promise
                    .then(function (data) {
                        console.log('done');
                        $scope.$parent.add(data);
                        Events.reset();
                        $state.go('^');
                    }).catch(function (result) {
                        if (result.data && result.data.errors)
                            $scope.event.errors.push.apply($scope.event.errors, result.data.errors);
                    });
            };
        }
    ]);
})();