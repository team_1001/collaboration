﻿(function() {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('LocationCtrl', [
        '$scope', '$uibModalInstance', '$geolocation', 'location', '$timeout', 'uiGmapGoogleMapApi', 'uiGmapIsReady', '$q',
        function ($scope, $uibModalInstance, $geolocation, location, $timeout, uiGmapGoogleMapApi, uiGmapIsReady, $q) {
            var marker = null,
                getCurrentPosition = function () {
                if ($geolocation.position.coords)
                    return $q.resolve($geolocation.position.coords);

                return $geolocation.getCurrentPosition({
                    timeout: 60000
                }).then(function (position) {
                    return position.coords;
                });
            };

            $scope.loading = true;

            $scope.map = {
                zoom: 8
            };

            $scope.confirm = function () {
                $uibModalInstance.close(marker);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };

            $q.all([getCurrentPosition(), uiGmapGoogleMapApi]).then(function (values) {
                var maps = values[1];

                angular.extend($scope.map, {
                    center: {
                        latitude: values[0].latitude,
                        longitude: values[0].longitude
                    }
                });

                $scope.loading = false;

                console.log('maps loaded...');

                uiGmapIsReady.promise(1).then(function (instances) {
                    function placeMarker(position, map) {

                        marker = new maps.Marker({
                            position: position,
                            map: map
                        });

                        map.panTo(position);
                    }

                    // workaround to avoid https://github.com/allenhwkim/angularjs-google-maps/issues/88
                    var map = instances[0].map;
                    $timeout(function () {
                        var center = map.getCenter();

                        maps.event.trigger(map, 'resize');
                        map.setCenter(center);

                        if (location.marker && location.marker.latitude > 0 && location.marker.longitude > 0) {
                            placeMarker({
                                lat: location.marker.latitude,
                                lng: location.marker.longitude
                            }, map);
                        }
                    }, 100);

                    map.addListener('click', function (e) {
                        if (marker) {
                            marker.setMap(null);
                        }

                        placeMarker(e.latLng, map);
                    });
                });
            });
        }
    ]);
})();