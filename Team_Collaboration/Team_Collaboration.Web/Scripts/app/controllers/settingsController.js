﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('SettingsCtrl', ['$scope', 'AuthService',
        function ($scope, Auth) {
            $scope.data = [];

            $scope.refreshToken = function () {
                Auth.refreshToken();
            };
        }]);
})();