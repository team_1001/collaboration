﻿(function() {
    'use strict';

    /* Controllers */

    var controllers = angular.module('controllers');

    controllers.controller('LoginCtrl', [
        '$scope', 'AuthService', '$state', 'SignalR', '$log',
        function ($scope, Auth, $state, SignalR, $log) {
            $scope.loginData = {
                userName: "",
                password: "",
                useRefreshTokens: true,
                errors: []
            };

            $scope.login = function () {
                $scope.loginData.errors.length = 0;

                Auth.login($scope.loginData)
                    .then(function (response) {
                        $log.log('successful login');
                        SignalR.start();
                        $state.go('main');
                    }).catch(function (err) {
                        if (err.data && err.data.error_description)
                            $scope.loginData.errors.push(err.data.error_description);
                    });
            };
        }
    ]);
})();