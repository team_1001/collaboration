﻿(function() {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('RegisterCtrl', ['$scope', 'AuthService', '$timeout', '$state', '$log',
        function ($scope, Auth, $timeout, $state, $log) {

            $scope.savedSuccessfully = false;

            $scope.registration = {
                userName: "",
                firstName: "",
                lastName: "",
                password: "",
                confirmPassword: "",
                errors: []
            };

            var startTimer = function () {
                var timer = $timeout(function () {
                    $timeout.cancel(timer);
                    $state.go('auth.login');
                }, 2000);
            }

            $scope.signUp = function () {
                $scope.registration.errors.length = 0;

                Auth.register($scope.registration)
                    .then(function (response) {
                        $scope.savedSuccessfully = true;
                        $scope.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
                        $log.log($scope.message);
                        startTimer();
                    }).catch(function (response) {
                        if (response.data && response.data.errors)
                            $scope.registration.errors.push.apply($scope.registration.errors, response.data.errors);
                        
                        $log.log($scope.message);
                    });
            };
        }]);
})();