﻿(function () {
    'use strict';

    /* Controllers */

    var controllers = angular.module('controllers');

    controllers.controller('ProfileCtrl', ['$scope', 'Profile', '$timeout', 'Upload', 'common', 'localStorageService',
        function ($scope, Profile, $timeout, Upload, common, $localStorage) {
            $scope.user = {
                avatar: null,               
                get hasAvatar() {
                    return this.avatar !== null;
                },

                errors: []
            };

            $scope.showStatus = false;
            $scope.progress = {
                max: 100,
                value: 0,
                loading: false
            };

            $scope.uploadFiles = function ($file, $errors) {
                if ($file) {
                    $file.upload = Upload.upload({
                        url: common.host + '/resource/api/Files',
                        data: { file: $file },
                        headers: { 'Authorization': 'Bearer ' + $localStorage.get('authData').token }
                    });

                    $scope.progress.loading = true;

                    $file.upload.then(function (response) {
                        $timeout(function () {
                            $scope.user.avatar = common.host + '/resource/api/Files/' + response.data.fileName;
                            $scope.$apply();
                        });
                    }).then(null, null, function (r) {
                        $scope.progress.value = r.loaded * $scope.max / r.total;
                    }).catch(function (response) {
                        console.log(response);

                        //if (response.status > 0)
                        //    $scope.errorMsg = response.status + ': ' + response.data;
                    }).finally(function(response) {
                        $scope.progress.loading = false;
                    });
                }

                if ($errors) {
                    $errors.forEach(function ($err) {
                        if ($err.$error === 'pattern')
                            alert("This file is not allowed. The allowed extensions are: " + $err.$errorParam); // TODO user-friendly dialog
                    });
                }
            };

            $scope.update = function () {
                $scope.user.errors.length = 0;

                Profile.update($scope.user)
                    .$promise
                    .then(function (userData) {
                        // todo: temporary solution to show status
                        console.log('success');
                        $scope.showStatus = true;

                        $timeout(function() {
                            $scope.showStatus = false;
                        }, 1000);
                    }).catch(function (result) {
                        if (result.data && result.data.errors)
                            $scope.user.errors.push.apply($scope.user.errors, result.data.errors);
                    });
            };

            Profile.query()
                .$promise
                .then(function(userData) {
                    angular.extend($scope.user, userData);
                });
        }]);
})();