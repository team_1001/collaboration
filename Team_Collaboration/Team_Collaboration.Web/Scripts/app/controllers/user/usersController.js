﻿(function () {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('UsersCtrl', ['$scope', '$state', 'Friends', 'Users',
        function ($scope, $state, Friends, Users) {
            $scope.add = function (userId) {
                Friends.save({ id: userId })
                    .$promise
                    .then(function() {
                        console.log('done');
                        $state.go(Constants.States.Main.Friends.State); //todo
                    });
            };

            $scope.users = [];
            Users.query({})
                .$promise
                .then(function(data) {
                    $scope.users.push.apply($scope.users, data.items.map(function(u) {
                        angular.extend(u.user, {
                            isFriend: u.isFriend, isSelf: u.isSelf
                        });

                        Object.defineProperty(u.user, 'hasAvatar', {
                            enumerable: true,
                            get: function () {
                                return this.avatar !== null;
                            }
                        });

                        return u.user;
                    }));
                });
        }]);
})();
