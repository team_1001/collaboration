﻿(function () {
    'use strict';
    var controllers = angular.module('controllers');

    controllers.controller('FriendDeleteCtrl', [
        '$scope', 'Friends', '$uibModalInstance', 'id',
        function ($scope, Friends, $uibModalInstance, id) {

            $scope.save = function () {
                Friends.remove({ id: id })
                    .$promise
                    .then(function () {
                        $uibModalInstance.close();
                    }).catch(function () {
                        console.log('failed');
                        $uibModalInstance.dismiss();
                    });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();