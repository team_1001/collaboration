﻿(function () {
    'use strict';

    var controllers = angular.module('controllers');

    controllers.controller('FriendsCtrl', ['$scope', '_', '$stateParams', '$state', 'Friends', '$uibModal',
        function ($scope, _, $stateParams, $state, Friends, $uibModal) {
            $scope.friends = [];
            $scope.loaded = false;

            $scope.remove = function (id) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'views/confirmation.html',
                    controller: 'FriendDeleteCtrl',
                    resolve: {
                        id: function () {
                            return id;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    _.remove($scope.friends, function (f) { return f.id === id });
                    console.log('done');
                }, function () {
                    console.log('canceled');
                });
            };

            Friends.query()
                .$promise
                .then(function(data) {
                    $scope.friends.push.apply($scope.friends, data.friends);
                    $scope.loaded = true;
                });
        }]);
})();
