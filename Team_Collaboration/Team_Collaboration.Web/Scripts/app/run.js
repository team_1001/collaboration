﻿(function () {
    var app = angular.module('App');

    app.run([
        '$rootScope', '$state', '$stateParams', 'AuthRedirectService', 'AuthService', 'SignalR',
        function($rootScope, $state, $stateParams, AuthRedirectService, Auth, SignalR) {
            // add 20 minutes interval to refresh token
            Auth.checkToken();

            // initial token refresh
            Auth.refreshToken().then(function() {
                SignalR.start();
            });

            $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams) {
                    AuthRedirectService.checkAccess(event, toState, toParams, fromState, fromParams);
                }
            );
        }
    ]);
})();