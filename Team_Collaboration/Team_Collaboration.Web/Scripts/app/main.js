﻿(function () {
    var app = angular.module('App', ['ngRoute', 'ui.router', 'ngAudio', 'ui.bootstrap', 'LocalStorageModule', 'auth', 'controllers', 'services', 'signalr', 'helpers']);

    app.constant('common', { host: Constants.Host, clientId: Constants.ClientId });

    app.config([
        '$stateProvider', '$urlRouterProvider', '$httpProvider', '$qProvider', function($stateProvider, $urlRouterProvider, $httpProvider, $qProvider) {
            $qProvider.errorOnUnhandledRejections(false);

            $httpProvider.interceptors.push('AuthInterceptorService');

            Router.configure($stateProvider, $urlRouterProvider);
        }
    ]);
})();