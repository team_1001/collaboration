﻿(function (Constants) {
    Constants.ClientId = 'WebApp';
    Constants.Host = window.location.protocol + '//' + window.location.hostname + ':4343';
    Constants.Roles = {
        host: "host",
        user: "user",
        admin: "admin"
    };

    Constants.NotificationTypes = {
        eventCreated: "EventCreated",
        invitationCreated: "InvitationCreated"
    };

    Constants.GoogleApiKey = "AIzaSyBEKo6Zz9UfXXob0_asSgi9d0LBGrIS8H0";
    Constants.States = {
        concat: function(left, right) {
            return left + '.' + right;
        },

        Edit: {
            get State() {
                return "edit";
            }
        },

        Login: {
            get State() {
                return "login";
            }
        },

        Register: {
            get State() {
                return "register";
            }
        },

        View: {
            get State() {
                return "view";
            }
        },

        Create: {
            get State() {
                return "create";
            }
        },

        Invite: {
            get State() {
                return "invite";
            }
        },

        Events: {
            get State() {
                return "events";
            }
        },

        Profile: {
            get State() {
                return "profile";
            }
        },

        Settings: {
            get State() {
                return "settings";
            }
        },

        Feed: {
            get State() {
                return "feed";
            }
        },

        List: {
            get State() {
                return "list";
            }
        },

        Geo: {
            get State() {
                return "geo";
            }
        },

        My: {
            get State() {
                return "my";
            }
        },

        All: {
            get State() {
                return "all";
            }
        },

        Invitations: {
            get State() {
                return "invitations";
            }
        },

        Requests: {
            get State() {
                return "requests";
            }
        },

        Dialogs: {
            get State() {
                return "dialogs";
            }
        },

        Users: {
            get State() {
                return "users";
            }
        },

        Friends: {
            get State() {
                return "friends";
            }
        },

        Incoming: {
            get State() {
                return "incoming";
            }
        },

        Outgoing: {
            get State() {
                return "outgoing";
            }
        },

        Auth: {
            get State() {
                return "auth";
            },

            Login: {
                get State() {
                    return Constants.States.concat(Constants.States.Auth.State, Constants.States.Login.State);
                }
            },

            Register: {
                get State() {
                    return Constants.States.concat(Constants.States.Auth.State, Constants.States.Register.State);
                }
            }
        },

        Main: {
            get State() {
                return "main";
            },

            Events: {
                get State() {
                    return Constants.States.concat(Constants.States.Main.State, Constants.States.Events.State);
                },

                My: {
                    get State() {
                        return Constants.States.concat(Constants.States.Main.Events.State, Constants.States.My.State);
                    },

                    View: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.My.State, Constants.States.View.State);
                        }
                    },

                    Edit: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.My.State, Constants.States.Edit.State);
                        }
                    },

                    Create: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.My.State, Constants.States.Create.State);
                        }
                    },

                    Invite: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.My.State, Constants.States.Invite.State);
                        }
                    }
                },

                All: {
                    get State() {
                        return Constants.States.concat(Constants.States.Main.Events.State, Constants.States.All.State);
                    },

                    View: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.All.State, Constants.States.View.State);
                        }
                    },

                    Edit: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.All.State, Constants.States.Edit.State);
                        }
                    },

                    Invite: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.All.State, Constants.States.Invite.State);
                        }
                    }
                },

                Invitations: {
                    get State() {
                        return Constants.States.concat(Constants.States.Main.Events.State, Constants.States.Invitations.State);
                    },

                    Incoming: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.Invitations.State, Constants.States.Incoming.State);
                        }
                    },

                    Outgoing: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.Invitations.State, Constants.States.Outgoing.State);
                        }
                    }
                },

                Requests: {
                    get State() {
                        return Constants.States.concat(Constants.States.Main.Events.State, Constants.States.Requests.State);
                    },

                    Incoming: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.Requests.State, Constants.States.Incoming.State);
                        }
                    },

                    Outgoing: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Events.Requests.State, Constants.States.Outgoing.State);
                        }
                    }
                }
            },

            Feed: {
                get State() {
                    return Constants.States.concat(Constants.States.Main.State, Constants.States.Feed.State);
                },

                List: {
                    get State() {
                        return Constants.States.concat(Constants.States.Main.Feed.State, Constants.States.List.State);
                    },

                    View: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Feed.List.State, Constants.States.View.State);
                        }
                    },

                    Edit: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Feed.List.State, Constants.States.Edit.State);
                        }
                    },

                    Invite: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Feed.List.State, Constants.States.Invite.State);
                        }
                    }
                },

                Geo: {
                    get State() {
                        return Constants.States.concat(Constants.States.Main.Feed.State, Constants.States.Geo.State);
                    },

                    View: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Feed.Geo.State, Constants.States.View.State);
                        }
                    },

                    Invite: {
                        get State() {
                            return Constants.States.concat(Constants.States.Main.Feed.Geo.State, Constants.States.Invite.State);
                        }
                    }
                }
            },

            Dialogs: {
                get State() {
                    return Constants.States.concat(Constants.States.Main.State, Constants.States.Dialogs.State);
                },

                View: {
                    get State() {
                        return Constants.States.concat(Constants.States.Main.Dialogs.State, Constants.States.View.State);
                    }
                }
            },

            Users: {
                get State() {
                    return Constants.States.concat(Constants.States.Main.State, Constants.States.Users.State);
                }
            },

            Friends: {
                get State() {
                    return Constants.States.concat(Constants.States.Main.State, Constants.States.Friends.State);
                }
            },

            Profile: {
                get State() {
                    return Constants.States.concat(Constants.States.Main.State, Constants.States.Profile.State);
                }
            },

            Settings: {
                get State() {
                    return Constants.States.concat(Constants.States.Main.State, Constants.States.Settings.State);
                }
            }
        }
    };

})(Constants = {});