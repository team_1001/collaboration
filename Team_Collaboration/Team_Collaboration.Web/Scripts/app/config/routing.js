﻿(function (Router) {
    'use strict';

    Router.configure = function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/test");

        $stateProvider
            .state(Constants.States.Auth.State, {
                abstract: true,
                url: '/auth',
                template: '<ui-view/>'
            }).state(Constants.States.Auth.Login.State, {
                url: "/login",
                templateUrl: "views/auth/login.html",
                controller: 'LoginCtrl',
                data: {
                    anonymous: true
                }
            }).state(Constants.States.Auth.Register.State, {
                url: "/register",
                templateUrl: "views/auth/register.html",
                controller: 'RegisterCtrl',
                data: {
                    anonymous: true
                }
            });

        $stateProvider
            .state(Constants.States.Main.Feed.State, {
                url: "/feed",
                abstract: true,
                views: {
                    "main": {
                        templateUrl: "views/events/feed/feedTabs.html",
                        controller: 'FeedCtrl'
                    }
                }
            }).state(Constants.States.Main.Feed.List.State, {
                url: "/list",
                views: {
                    "feed": {
                        templateUrl: "views/events/feed/feedList.html",
                        controller: 'FeedListCtrl'
                    }
                }
            }).state(Constants.States.Main.Feed.List.View.State, {
                url: "/view/:id",
                templateUrl: "views/events/viewEvent.html",
                controller: 'EventViewCtrl'
            }).state(Constants.States.Main.Feed.List.Invite.State, {
                url: "/invite/:id",
                templateUrl: "views/events/invitations/createInvitation.html",
                controller: 'CreateInvitationCtrl'
            }).state(Constants.States.Main.Feed.Geo.State, {
                url: "/geo",
                views: {
                    "feed": {
                        templateUrl: "views/events/feed/feedGeo.html",
                        controller: 'FeedGeoCtrl'
                    }
                }
            }).state(Constants.States.Main.Feed.Geo.View.State, {
                url: "/view/:id",
                templateUrl: "views/events/viewEvent.html",
                controller: 'EventViewCtrl'
            }).state(Constants.States.Main.Feed.Geo.Invite.State, {
                url: "/invite/:id",
                templateUrl: "views/events/invitations/createInvitation.html",
                controller: 'CreateInvitationCtrl'
            });

        $stateProvider
            .state(Constants.States.Main.Events.State, {
                abstract: true,
                url: "/events",
                views: {
                    "main": {
                        templateUrl: "views/events/events.html",
                        controller: 'EventsCtrl'
                    }
                }
            }).state(Constants.States.Main.Events.All.State, {
                url: "/all",
                views: {
                    "tab": {
                        templateUrl: "views/events/allEvents.html",
                        controller: 'AllEventsCtrl'
                    }
                }
            }).state(Constants.States.Main.Events.All.Invite.State, {
                url: "/invite/:id",
                templateUrl: "views/events/invitations/createInvitation.html",
                controller: 'CreateInvitationCtrl'
            }).state(Constants.States.Main.Events.All.View.State, {
                url: "/view/:id",
                templateUrl: "views/events/viewEvent.html",
                controller: 'EventViewCtrl'
            }).state(Constants.States.Main.Events.All.Edit.State, {
                url: "/edit/:id",
                templateUrl: "views/events/event.html",
                controller: 'EventEditCtrl'
            });

        $stateProvider
            .state(Constants.States.Main.Events.Invitations.State, {
                abstract: true,
                url: "/invites",
                views: {
                    "tab": {
                        templateUrl: "views/events/invitations/invitationsTab.html",
                        controller: 'InvitationsCtrl'
                    }
                }
            }).state(Constants.States.Main.Events.Invitations.Incoming.State, {
                url: "/incoming",
                views: {
                    "invitations": {
                        templateUrl: "views/events/invitations/incomingInvitations.html",
                        controller: 'IncomingInvitationsCtrl'
                    }
                }

            }).state(Constants.States.Main.Events.Invitations.Outgoing.State, {
                url: "/outgoing",
                views: {
                    "invitations": {
                        templateUrl: "views/events/invitations/outgoingInvitations.html",
                        controller: 'OutgoingInvitationsCtrl'
                    }
                }
            });

        $stateProvider
            .state(Constants.States.Main.Events.Requests.State, {
                abstract: true,
                url: "/requests",
                views: {
                    "tab": {
                        templateUrl: "views/events/requests/requestsTab.html",
                        controller: 'RequestsCtrl'
                    }
                }
            }).state(Constants.States.Main.Events.Requests.Incoming.State, {
                url: "/incoming",
                views: {
                    "requests": {
                        templateUrl: "views/events/requests/incomingRequests.html",
                        controller: 'IncomingRequestsCtrl'
                    }
                }
            }).state(Constants.States.Main.Events.Requests.Outgoing.State, {
                url: "/outgoing",
                views: {
                    "requests": {
                        templateUrl: "views/events/requests/outgoingRequests.html",
                        controller: 'OutgoingRequestsCtrl'
                    }
                }
            });

        $stateProvider
            .state(Constants.States.Main.Events.My.View.State, {
                url: "/view/:id",
                templateUrl: "views/events/viewEvent.html",
                controller: 'EventViewCtrl'
            }).state(Constants.States.Main.Events.My.Create.State, {
                url: "/add",
                templateUrl: "views/events/event.html",
                controller: 'CreateEventCtrl'
            }).state(Constants.States.Main.Events.My.Edit.State, {
                url: "/edit/:id",
                templateUrl: "views/events/event.html",
                controller: 'EventEditCtrl'
            }).state(Constants.States.Main.Events.My.State, {
                url: "/my",
                views: {
                    "tab": {
                        templateUrl: "views/events/myEvents.html",
                        controller: 'MyEventsCtrl'
                    }
                }
            }).state(Constants.States.Main.Events.My.Invite.State, {
                url: "/invite/:id",
                templateUrl: "views/events/invitations/createInvitation.html",
                controller: 'CreateInvitationCtrl'
            });

        $stateProvider
           .state(Constants.States.Main.State, {
               url: "/test",
               templateUrl: "views/test.html",
               controller: 'MainCtrl'
           }).state(Constants.States.Main.Profile.State, {
               url: "/profile",
               views: {
                   "main": {
                       templateUrl: "views/user/profile.html",
                       controller: 'ProfileCtrl'
                   }
               }
           }).state(Constants.States.Main.Friends.State, {
                url: "/friends",
                views: {
                    "main": {
                        templateUrl: "views/user/friends.html",
                        controller: 'FriendsCtrl'
                    }
                }
            }).state(Constants.States.Main.Users.State, {
                url: "/users",
                views: {
                    "main": {
                        templateUrl: "views/user/users.html",
                        controller: 'UsersCtrl'
                    }
                }
            });

        $stateProvider
            .state(Constants.States.Main.Dialogs.State, {
                url: "/dialogs",

                views: {
                    "main": {
                        templateUrl: "views/dialog/dialogs.html",
                        controller: 'DialogsCtrl'
                    }
                }
            }).state(Constants.States.Main.Dialogs.View.State, {
                url: "/:id?name",
                templateUrl: "views/dialog/dialog.html",
                controller: 'DialogCtrl'
            }).state(Constants.States.Main.Settings.State, {
                url: "/settings",
                views: {
                    "main": {
                        templateUrl: "views/settings.html",
                        controller: 'SettingsCtrl'
                    }
                }
            });
    };
})(Router = {})