﻿(function() {
    'use strict';

    var app = angular.module('App');

    app.directive('sortable', [
        function () {
            return {
                link: function (scope, element) {
                    $(element).sortable().disableSelection();
                }
            }
        }
    ]);

    app.directive('tabs', [
        function () {
            return {
                link: function (scope, element) {
                    $(element).tabs();
                }
            }
        }
    ]);

    // initial scrolling to the bottom when page first renders
    app.directive('scrollDown', ['$timeout', function ($timeout) {
        return function (scope, element, attrs) {
            if (scope.$eval(attrs.scrollDown) === false)
                return;

            var $element = $(element),
                $parent = $element.closest('.scroll-parent'),
                animate = scope.message.animate,
                scrolled = scope.message.scrolled,
                factor = 8;

            if (scrolled === true)
                return;

            scope.message.scrolled = true;

            var getPosition = function () {
                return $element.position().top + $parent.scrollTop();
            };

            if (animate === true) {
                // avoid jumping animation
                var running = $parent.data('animation');
                if (running && running.state() !== 'resolved')
                    running.then(function () {
                        if ($element.position().top - $parent.scrollTop() > ($element.outerHeight() * factor))
                            return $.Deferred().resolve();

                        return $parent.animate({
                            scrollTop: getPosition()
                        }, 300).promise();
                    });
                else {
                    $timeout(function () {
                        if ($element.position().top - $parent.scrollTop() > ($element.outerHeight() * factor))
                            return;

                        $parent.data('animation', $parent.animate({
                            scrollTop: getPosition()
                        }, 300).promise());
                    }, 0);
                }

            } else {
                if ($parent.scrollTop() > 0)
                    return;

                $timeout(function () {
                    $parent.scrollTop(getPosition());
                }, 10);
            }
        }
    }]);

    /* Validation */

    app.directive('compare', function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                scope.$watch(attrs.compare, function () {
                    ctrl.$validate();
                });

                ctrl.$validators.compare = function (modelValue, viewValue) {
                    return viewValue === scope.$eval(attrs.compare);
                };
            }
        };
    });
})();