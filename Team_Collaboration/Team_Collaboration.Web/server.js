var express = require('express'),
    crypto = require('crypto'),
    path = require('path'),
    fs = require('fs'),
    https = require('https'),
    http = require('http'),
    args = require('command-line-args');

var optionDefinitions = [
    { name: 'secure', alias: 's', type: Boolean },
    { name: 'port', type: String, alias: 'p' },
    { name: 'proxy', alias: 'x', type: String }
];

var options = args(optionDefinitions);

var app = express();

var privateKey = fs.readFileSync('key.pem').toString();
var certificate = fs.readFileSync('cert.pem').toString();

var credentials = crypto.createCredentials({ key: privateKey, cert: certificate });

app.use(express.static(__dirname));
app.use('/views', express.static(path.join(__dirname + '/Scripts/app/views')));
app.use('/uib/template', express.static(path.join(__dirname + '/Scripts/app/template')));

// Listen for requests
var securePort = options.port || 4545;
var isSecure = options.secure;

var server = isSecure ? https.createServer({
    key: privateKey,
    cert: certificate
}, app) : http.createServer(app);

server.listen(securePort, function () {
    var port = server.address().port;
    console.log('Listening  to port ' + port + (isSecure ? ' secure' : ' insecure'));
});

if (isSecure === true) {
    // redirecting
    var httpApp = express();

    var httpRouter = express.Router();

    httpRouter.get('*', function (req, res) {
        var destination = "https://" + req.headers['host'] + ':' + securePort + req.url;
        console.log(destination);
        return res.redirect(destination);
    });

    httpApp.use('/', httpRouter);

    var proxyServer = http.createServer(httpApp)
        .listen(options.proxy || 80, function () {
            var port = proxyServer.address().port;
            console.log('Listening  to port ' + port + ' insecure');
        });
}
