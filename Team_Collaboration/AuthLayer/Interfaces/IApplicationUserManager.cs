﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace AuthLayer.Interfaces
{
    public interface IApplicationUserManager : IDisposable
    {
        Task<RefreshToken> FindRefreshTokenAsync(string refreshTokenId);
        Task<List<RefreshToken>> GetAllRefreshTokensAsync();
        Task<bool> RemoveRefreshTokenAsync(string refreshTokenId);
        Task<bool> RemoveRefreshTokenAsync(RefreshToken refreshToken);
        Task<bool> AddRefreshTokenAsync(RefreshToken token);
        Task<AppClient> FindClientAsync(string clientId);

        Task<IdentityResult> CreateAsync(AppUser user, string password);

        Task<AppUser> FindAsync(string userName, string password);
    }
}
