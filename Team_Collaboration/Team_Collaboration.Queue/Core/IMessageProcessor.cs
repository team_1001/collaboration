﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue.Core
{
    public class MessageInfo<TMessage>
    {
        public TMessage Message { get; set; }

        public ulong DeliveryTag { get; set; }

        public int Retry { get; set; }
    }

    public interface IMessageProcessor<TMessage>
    {
        Task<bool> Process(MessageInfo<TMessage> message);
    }
}
