﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Configuration;
using Common.Extensions;
using NLog;
using RabbitMQ.Client;

namespace Queue.Core
{
    public interface IMessageProducer
    {
        Task<bool> Produce<TMessage>(TMessage message, string routingKey, IDictionary<string, object> headers = null) where TMessage : class;
    }

    public class MessageProducer : IMessageProducer
    {
        private const int CMAX_RETRIES = 3;
        private const int CDELIVERY_TIMEOUT = 5;

        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        private readonly IConfigurationProvider _configuration;

        public MessageProducer(IConfigurationProvider configuration)
        {
            _configuration = configuration;
        }

        public async Task<bool> Produce<TMessage>(TMessage message, string routingKey, IDictionary<string, object> headers = null) where TMessage : class 
        {
            string host = _configuration.AppSettings["Host"];
            string exchange = _configuration.AppSettings["Exchange"];

            var factory = new ConnectionFactory()
            {
                HostName = host
            };

            int retry = 0;
            TaskCompletionSource<bool> completionSource = new TaskCompletionSource<bool>();

            while (retry < CMAX_RETRIES)
            {
                retry++;
                try
                {
                    using (var connection = factory.CreateConnection())
                    using (var channel = connection.CreateModel())
                    {
                        try
                        {
                            channel.ExchangeDeclare(
                                exchange: exchange,
                                type: "direct",
                                durable: true,
                                autoDelete: false,
                                arguments: null);

                            var props = channel.CreateBasicProperties();
                            props.Persistent = true;
                            if (headers != null)
                                props.Headers = headers;

                            channel.ConfirmSelect();

                            // make sure message was received by the broker
                            channel.BasicAcks += (o, e) => completionSource.TrySetResult(true);

                            _logger.Trace("Queue exchange {0} created on host {1}",
                                exchange,
                                host
                                );

                            _logger.Trace("Message topic: {0}", routingKey);

                            var body = message.ToByteArray();

                            channel.BasicPublish(exchange: exchange,
                                                 routingKey: routingKey,
                                                 basicProperties: props,
                                                 body: body);

                            channel.WaitForConfirmsOrDie();
                            _logger.Info(" [x] Sent {0}", message);
                            break;
                        }
                        catch (Exception e)
                        {
                            completionSource.TrySetResult(false);
                            _logger.Error(e);
                        }
                    }
                }
                catch (Exception e)
                {
                    completionSource.TrySetResult(false);
                    _logger.Fatal(e);
                }
            }

            return await completionSource.Task;
        }
    }
}
