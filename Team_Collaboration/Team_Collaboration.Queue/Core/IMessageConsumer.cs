﻿using System;
using System.Collections.Generic;
using System.Monads;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Configuration;
using Common.Extensions;
using NLog;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Queue.Core
{
    public interface IMessageConsumer
    {
        void StartConsuming();

        void StopConsuming();
    }

    public class MessageConsumer<TMessage> : IMessageConsumer where TMessage : class
    {
        private const int CMAX_RETRIES = 2;

        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        private readonly IConfigurationProvider _configuration;
        private readonly IMessageProcessor<TMessage> _messageProcessor;
        private readonly string _queueName;
        private readonly List<string> _topics;
        private readonly CancellationTokenSource _tokenSource;

        public MessageConsumer(
            IConfigurationProvider configuration,
            IMessageProcessor<TMessage> messageProcessor,
            string queueName, List<string> topics
            )
        {
            _tokenSource = new CancellationTokenSource();
            _configuration = configuration;
            _messageProcessor = messageProcessor;
            _queueName = queueName;
            _topics = topics;
        }

        public void StartConsuming()
        {
            Task.Run(() =>
            {
                StartConsumingInternal(_tokenSource.Token);
            }, _tokenSource.Token);
        }

        private void StartConsumingInternal(CancellationToken token)
        {
            _logger.Trace("Preparing to consume messages...");

            string exchange = _configuration.AppSettings["Exchange"];
            string host = _configuration.AppSettings["Host"];
            ushort maxMessages = (ushort)_configuration.AppSettings["PrefetchCount"].ToInt();
            int maxRetries = _configuration.AppSettings["MaxRequeRetries"].ToInt(CMAX_RETRIES);

            #region DEAD

            string exchangeDead = string.Format("{0}_dead", exchange);
            string queueDead = string.Format("{0}_dead", _queueName);
            string routingKeyDead = string.Format("{0}_dead", _queueName);

            #endregion

            if (_topics.Count == 0)
            {
                _logger.Trace("Host: {0}. Queue: {1}. Exchange: {2}", host, _queueName, exchange);
                _logger.Trace("No routing keys registered. Exiting...");
                return;
            }

            var factory = new ConnectionFactory()
            {
                HostName = host
            };

            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: exchange,
                                        type: "direct",
                                        durable: true,
                                        autoDelete: false,
                                        arguments: null);

                    _logger.Trace("Exchange {0} of type {1} created", exchange, "direct");

                    channel.ExchangeDeclare(exchange: exchangeDead,
                                        type: "direct",
                                        durable: true,
                                        autoDelete: false,
                                        arguments: null);

                    _logger.Trace("Exchange {0} of type {1} created", exchangeDead, "direct");

                    IDictionary<string, object> queueArgs = new Dictionary<string, object>
                    {
                        {"x-dead-letter-exchange", exchangeDead},
                        {"x-dead-letter-routing-key", routingKeyDead }
                    };

                    channel.QueueDeclare(queue: _queueName,
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: queueArgs);

                    _logger.Trace("Queue {0} declared", _queueName);

                    channel.QueueDeclare(queue: queueDead,
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    _logger.Trace("Queue {0} declared", queueDead);

                    channel.QueueBind(queue: queueDead,
                                      exchange: exchangeDead,
                                      routingKey: routingKeyDead,
                                      arguments: null);

                    _logger.Trace("Queue {0} bound to {1} exchange with {2} routing key", queueDead, exchangeDead, routingKeyDead);

                    channel.BasicQos(
                        prefetchSize: 0,
                        prefetchCount: maxMessages,
                        global: false);

                    _logger.Trace("Basic Qos prefetch set to {0}", maxMessages);

                    foreach (var key in _topics)
                    {
                        channel.QueueBind(queue: _queueName,
                                          exchange: exchange,
                                          routingKey: key,
                                          arguments: null);

                        _logger.Trace("Queue {0} for exchange {1} bind to {2} topic", _queueName, exchange, key);
                    }

                    _logger.Trace("Setup completed...");

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += async (obj, args) =>
                    {
                        try
                        {
                            var body = args.Body;
                            var message = body.FromByteArray<TMessage>();

                            int retry = 0;
                            if (args.BasicProperties != null
                                && args.BasicProperties.Headers != null
                                && args.BasicProperties.Headers.ContainsKey("retry"))
                            {
                                // if retry limit exceeded, reject message
                                if (int.TryParse(args.BasicProperties.Headers["retry"]
                                    .With(x => x.ToString())
                                    .Recover(() => string.Empty), out retry) && retry > maxRetries)
                                {
                                    _logger.Info("Message process retries exceeded. Nacking the message. {0}", message != null
                                        ? message.ToString()
                                        : string.Empty);

                                    consumer.Model.BasicNack(args.DeliveryTag, false, false);
                                    return;
                                }
                            }

                            if (message != null)
                            {
                                _logger.Info("Successfully retrieved message: {0}. Tag: {2}. Routing key: {1}", message,
                                    args.RoutingKey, args.DeliveryTag);

                                bool result = await _messageProcessor.Process(new MessageInfo<TMessage>()
                                {
                                    Message = message,
                                    DeliveryTag = args.DeliveryTag,
                                    Retry = retry
                                });

                                if (result)
                                    consumer.Model.BasicAck(args.DeliveryTag, false);
                            }
                            else
                            {
                                _logger.Error("Failed to retrieve message: {0}. Routing key: {1}", args.DeliveryTag,
                                    args.RoutingKey);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Fatal(e);
                        }
                    };

                    _logger.Trace("Starting consuming messages...");
                    consumer.Model.BasicConsume(queue: _queueName,
                                         noAck: false,
                                         consumer: consumer);

                    while (!token.IsCancellationRequested && channel.IsOpen)
                    {
                        Task.Delay(TimeSpan.FromSeconds(10)).Wait();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Fatal(e);
                throw;
            }
        }

        public void StopConsuming()
        {
            if (!_tokenSource.IsCancellationRequested)
                _tokenSource.Cancel();
        }
    }
}
