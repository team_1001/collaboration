﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace Terminal
{
    class Program
    {
        [DllImport("User32.Dll", EntryPoint = "PostMessageA")]
        private static extern bool PostMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

        const int VK_RETURN = 0x0D;
        const int WM_KEYDOWN = 0x100;

        static readonly Dictionary<string, Process> Processes = new Dictionary<string, Process>();
         
        static void PrintMenu()
        {
            Console.Clear();

            Console.WriteLine("Terminal");
            Console.WriteLine("Press Q to exit...");

            Console.WriteLine("1. Launch server;");
            Console.WriteLine("2. Launch hub;");
            Console.WriteLine("3. Launch all;");
            Console.WriteLine("4. Terminate all;");
        }

        static void Launch(string name, string cmd)
        {
            try
            {
                if (Processes.ContainsKey(name))
                {
                    Console.WriteLine("Process {0} already launched", name);
                    return;
                }

                var process = Process.Start(new ProcessStartInfo()
                {
                    FileName = "cmd.exe",
                    Arguments = cmd,
                    CreateNoWindow = true,
                });

                if (process != null)
                {
                    Processes.Add(name, process);
                }

                Console.WriteLine("{0} started successfully...", name);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error launching {0}...", name);
                Console.WriteLine(e.Message);
            }
        }

        static void Terminate(string name)
        {
            if (!Processes.ContainsKey(name))
            {
                Console.WriteLine("Process {0} not found", name);
                return;
            }

            try
            {
                var process = Processes[name];
                var hWnd = process.MainWindowHandle;
                PostMessage(hWnd, WM_KEYDOWN, VK_RETURN, 0);
                Processes.Remove(name);

                Console.WriteLine("Process {0} terminated successfully", name);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error terminating process {0}", name);
                Console.WriteLine(e.Message);
            }
        }

        static void TerminateAll()
        {
            foreach (var key in Processes.Keys)
            {
                try
                {
                    var processes = Process.GetProcessesByName(key);
                    var process = processes[processes.Length - 1];

                    var hWnd = process.MainWindowHandle;

                    PostMessage(hWnd, WM_KEYDOWN, VK_RETURN, 0);

                    Console.WriteLine("Process {0} terminated successfully", key);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error terminating process {0}", key);
                    Console.WriteLine(e.Message);
                }
            }

            Processes.Clear();
        }

        [STAThread]
        static void Main(string[] args)
        {
            while (true)
            {
                PrintMenu();

                ConsoleKey key = Console.ReadKey().Key;
                if (key == ConsoleKey.Q)
                    break;

                DirectoryInfo info = new DirectoryInfo(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
                string parentDir = info.Parent.Parent.Parent.FullName;

                switch (key)
                {
                    case ConsoleKey.D1:
                        Launch("Team_Collaboration.Server", $"/c start {Path.Combine(parentDir, "Team_Collaboration.Server", "bin", "Debug", "Team_Collaboration.Server.exe")}");
                        break;
                    case ConsoleKey.D2:
                        Launch("Team_Collaboration.HubServer", $"/c start{Path.Combine(parentDir, "Team_Collaboration.HubServer", "bin", "Debug", "Team_Collaboration.HubServer.exe")}");
                        break;
                    case ConsoleKey.D3:
                        Launch("Team_Collaboration.Server", $"/c start {Path.Combine(parentDir, "Team_Collaboration.Server", "bin", "Debug", "Team_Collaboration.Server.exe")}");
                        Launch("Team_Collaboration.HubServer", $"/c start {Path.Combine(parentDir, "Team_Collaboration.HubServer", "bin", "Debug", "Team_Collaboration.HubServer.exe")}");
                        break;
                    case ConsoleKey.D4:
                        TerminateAll();
                        break;
                    default:
                        Console.WriteLine("Unknown option...");
                        break;
                }

                Console.ReadLine();
            }
        }
    }
}
