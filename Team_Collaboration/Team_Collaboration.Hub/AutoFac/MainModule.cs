﻿using Autofac;
using Autofac.Integration.SignalR;
using BusinessLayer.Services.Interfaces;
using Microsoft.AspNet.SignalR;
using Models.Hub;
using Team_Collaboration.Hub.Services;
using Team_Collaboration.Hub.Utils;

namespace Team_Collaboration.Hub.AutoFac
{
    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HubUserIdProvider>().As<IUserIdProvider>();
            builder.RegisterType<PresenceMonitor>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterHubs(typeof(PresenceMonitor).Assembly).ExternallyOwned();
            builder.Register(c => new ConnectionPool<HubUser, string>(u => u.Id)).AsSelf().As<IConnectionPool<string>>().SingleInstance();
        }
    }
}
