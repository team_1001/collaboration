﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace Team_Collaboration.Hub.Services
{
    class HubUserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            return request.User.Identity.GetUserId<int>().ToString();
        }
    }
}
