﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.Messaging;
using BusinessLayer.Services.Interfaces;
using Common.Extensions;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Models.Hub;
using Team_Collaboration.Hub.Services;

namespace Team_Collaboration.Hub.Hubs
{
    [Authorize(Roles = "user")]
    [HubName("chathub")]
    public class ChatHub : Microsoft.AspNet.SignalR.Hub
    {
        private readonly ConnectionPool<HubUser, string> _connectionPool;
        private readonly IMessagesProvider _messagesProvider;
        private readonly HubNotificationService _notificationService;
        private readonly IUserIdProvider _userIdProvider;

        public ChatHub(
            ConnectionPool<HubUser, string> connectionPool, 
            IMessagesProvider messagesProvider,
            HubNotificationService notificationService, 
            IUserIdProvider userIdProvider)
        {
            _connectionPool = connectionPool;
            _messagesProvider = messagesProvider;
            _notificationService = notificationService;
            _userIdProvider = userIdProvider;
        }

        public async Task SendMessage(dynamic message)
        {
            string newId = Guid.NewGuid().ToString("n");
            HubNewMessage newMessage = new HubNewMessage()
            {
                Content = message.content,
                DialogId = message.dialogId,
                Id = newId,
                SenderName = Context.User.Identity.Name,
                SenderId = _userIdProvider.GetUserId(Context.Request).ToInt(),
                Type = message.type,
                TimeStamp = message.timeStamp
            };

            if (await _notificationService.SendMessageAsync(newMessage))
            {
                await Clients.Caller.messageSent(new
                {
                    id = message.id,
                    newId = newId,
                    dialogId = message.dialogId
                });

                return;
            }

            throw new HubException("Message {0} wasn't sent", message);
        }

        public async Task ReceiveMessage(dynamic message)
        {
            HubExistingMessage existingMessage = new HubExistingMessage()
            {
                ReceiverId = _userIdProvider.GetUserId(Context.Request).ToInt(),
                DialogId = message.dialogId,
                Id = message.id,
            };

            if (await _notificationService.ReceiveMessageAsync(existingMessage))
            {
                await Clients.Caller.receiveMessageAck(new
                {
                    id = message.id,
                    dialogId = message.dialogId
                });

                return;
            }

            throw new HubException("Message {0} wasn't received", message);
        }

        public async Task Ping()
        {
            await Clients.Caller.pingBack();
        }

        public override async Task OnConnected()
        {
            GetOrRegister();
            await NotifyRelatedUsers(_userIdProvider.GetUserId(Context.Request), UserState.Online);
            await base.OnConnected();
        }

        public override async Task OnReconnected()
        {
            GetOrRegister();
            await NotifyRelatedUsers(_userIdProvider.GetUserId(Context.Request), UserState.Online);
            await base.OnReconnected();
        }

        public override async Task OnDisconnected(bool stopCalled)
        {
            string userId = _userIdProvider.GetUserId(Context.Request);

            var mapping = _connectionPool.Remove(userId, Context.ConnectionId);

            //UpdateOnlineUsersCount();

            //SendUserStatisticsUpdated();

            if (mapping != null && mapping.Connections.Count == 0)
            {
                // user disconnected
                await NotifyRelatedUsers(mapping.User.Id, UserState.Offline);

                //UpdateOnlineUsersCount();
            }

            await base.OnDisconnected(stopCalled);
        }

        private HubUser GetOrRegister()
        {
            if (!Context.User.Identity.IsAuthenticated)
            {
                return null;
            }

            string userId = _userIdProvider.GetUserId(Context.Request);
            var existedUser = _connectionPool.TryGetUser(u => u.Id == userId);

            if (existedUser == null)
            {
                existedUser = new HubUser()
                {
                    Id = userId,
                    State = UserState.Online
                };
            }

            _connectionPool.TryAdd(existedUser, Context.ConnectionId);

            return existedUser;
        }

        private async Task NotifyRelatedUsers(string userId, UserState state)
        {
            var relatedUsers = await _messagesProvider.GetRelatedUsers(userId.ToInt());
            if (relatedUsers != null)
                await Clients.Users(relatedUsers.Select(x => x.ToString()).ToList()).userStateChanged(new
                {
                    Id = userId,
                    State = (int) state,
                    //isMobile = mapping.User.IsMobile
                });
        }
    }
}
