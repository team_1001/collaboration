set mypath=C:\App
sc query state= all | findstr /C:"SERVICE_NAME: EventsWebService" 
IF %ERRORLEVEL% EQU 0 (
	sc start EventsWebService
)
IF %ERRORLEVEL% EQU 1 (
	cd /d %mypath%\Web
	call cmd /k "npm install"
	call cmd /k "npm run-script install-windows-service"
	sc start EventsWebService
)
sc query state= all | findstr /C:"SERVICE_NAME: EventsServerService" 
IF %ERRORLEVEL% EQU 0 (
	sc start EventsServerService
)
IF %ERRORLEVEL% EQU 1 (
	cd /d %mypath%\Server
	sc create EventsServerService binpath= "%mypath%\Server\Team_Collaboration.Server.exe service" start= auto
	sc start EventsServerService
)
sc query state= all | findstr /C:"SERVICE_NAME: EventsHubService" 
IF %ERRORLEVEL% EQU 0 (
	sc start EventsHubService
)
IF %ERRORLEVEL% EQU 1 (
	cd /d %mypath%\Hub
	sc create EventsHubService binpath= "%mypath%\Hub\Team_Collaboration.HubServer.exe service" start= auto 
	sc start EventsHubService
)