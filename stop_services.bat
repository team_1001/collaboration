sc query EventsWebService | findstr RUNNING
IF %ERRORLEVEL% EQU 0 (
	sc stop EventsWebService
)
sc query EventsServerService | findstr RUNNING
IF %ERRORLEVEL% EQU 0 (
	sc stop EventsServerService
)
sc query EventsHubService | findstr RUNNING
IF %ERRORLEVEL% EQU 0 (
	sc stop EventsHubService
)
